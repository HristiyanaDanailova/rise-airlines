import { getAll, getById , create, update,  remove} from "./httpRequests.js";
import { hideTableRows, convertObjectKeysToLowercase } from "./utils.js";

// Row template
const tableRowTemplate  = airport =>{
    let convertedAirline = convertObjectKeysToLowercase(airport);
   return `<tr data-id=${airport['id']}>
   <td>${convertedAirline.name}</td>
   <td>${convertedAirline.city}</td>              
   <td>${convertedAirline.country}</td>       
   <td>${convertedAirline.code}</td>       
   <td>${convertedAirline.runwayscount}</td>       
   <td>${new Date(convertedAirline.founded).toLocaleDateString('en-GB')}</td>   
   <td class="main-table-action-buttons">
   <button class="main-table-edit-action-button" data-id="${convertedAirline.id}">Edit</button>
   <button class="main-table-delete-action-button" data-id="${convertedAirline.id}">Delete</button>
   </td>    
</tr>`;
}

// Adds table row and attaches edit and delete button event listeners
function addTableRow(tableBodyElement,airport){
    if(airport){
        tableBodyElement.insertAdjacentHTML('beforeend',tableRowTemplate(airport));
        const rowElement = tableBodyElement.querySelector(`tr[data-id="${airport.id}"]`);
        const editButton = rowElement.querySelector('.main-table-edit-action-button');
        const deleteButton = rowElement.querySelector('.main-table-delete-action-button');

        attachEditButtonEventListener(editButton);
        attachDeleteButtonEventListener(deleteButton);
    }
}

// Updates the content of a row
function updateTableRow(airport){
    const rowElement = document.querySelector(`tr[data-id="${airport.id}"]`);
    if(rowElement){
        rowElement.innerHTML = tableRowTemplate(airport);
        
        const editButton = rowElement.querySelector('.main-table-edit-action-button');
        const deleteButton = rowElement.querySelector('.main-table-delete-action-button');

        attachEditButtonEventListener(editButton);
        attachDeleteButtonEventListener(deleteButton);
    }
}

// Removes a table row
function removeTableRow(id){
    const rowElement = document.querySelector(`tr[data-id="${id}"]`);
    if(rowElement){
        rowElement.remove();
        hideTableRows();
    }
}

// Loads all airports in the table
async function loadAirportsTable(){
    const airports = await getAll('Airport');
    const tableBodyElement = document.getElementById('main-table-body');
    tableBodyElement.innerHTML = '';

    if(airports.length > 0){
        airports.forEach(airport => {
           addTableRow(tableBodyElement,airport);
        });
    }else{
        const noAirportsRowElement = '<tr><td colspan="6">No airports found</td></tr>'
        tableBodyElement.innerHTML = noAirportsRowElement;
    }
    hideTableRows();
}

function attachEditButtonEventListener(element) {
    element.addEventListener('click', async (event) => {
        const id = event.target.getAttribute('data-id');
        await populateForm(id);
        toggleViews();
    });
}

function attachDeleteButtonEventListener(element) {
    element.addEventListener('click', async (event) => {
        const id = event.target.getAttribute('data-id');
        removeAirport(id);
    });
}

// Fills in the input fields of the form with the given data
async function populateForm(id) {
    const airport = await getById('Airport', id);
    if (airport) {
        document.getElementById('airport-id').value = airport.id;
        document.getElementById('airport-name').value = airport.name;
        document.getElementById('airport-city').value = airport.city;
        document.getElementById('airport-country').value = airport.country;
        document.getElementById('airport-code').value = airport.code;
        document.getElementById('airport-founded').value = airport.founded;
        document.getElementById('airport-runways-count').value = airport.runwaysCount;
    }
}

// Creates or updates an airport based on the id value
// After the action, the form gets cleared and the table view gets shown
async function createOrUpdateAirport(event) {
    event.preventDefault();
    const formElement = document.getElementById('airport-form');
    const formData = new FormData(formElement);
    const airportsData = Object.fromEntries(formData.entries());
    const airportId = document.getElementById('airport-id').value;

    const {id, ...dataWithoutId} = airportsData;
    try {
        let response;
        if (airportId) {
            response = await update('Airport', airportsData);
            updateTableRow(airportsData)
        } else {
            response = await create('Airport', dataWithoutId);
            loadAirportsTable();
        }

        if (response) {
            toggleViews();
            formElement.reset();
            document.getElementById('airport-id').value = '';
        } else {
            alert('Something went wrong. Please try again. :(');
        }
    } catch (error) {
        console.error(error);
        alert('Something went wrong. Please try again. :(');
    }
}

// Removes an airport and deletes the table row corresponding to it
async function removeAirport(id){
    if(confirm("Are you sure you want to delete this airport?")){
        const response = await remove('Airport',id);
        if(response){
            alert('Airport successfully deleted.');
            removeTableRow(id);
        }else{
            alert('Something went wrong. Please, try again. :(');
        }
   }
}

document.getElementById('airport-form').addEventListener('submit', createOrUpdateAirport);


window.addEventListener('load',async ()=>{
    loadAirportsTable();
 });