﻿function fadeIn(element) {
    element.style.opacity = 0;
    let opacity = 0;
    let intervalId = setInterval(function () {
        opacity += 0.02;
        element.style.opacity = opacity;

        if (opacity >= 1) {
            clearInterval(intervalId);
        }
    }, 16);
}

// Toggles the display property ofthe form and table elements
function toggleViews() {
    const showFormButtonElement = document.getElementById("main-show-form-button");
    const tableElement = document.getElementById("table-container");
    const formElement = document.getElementById("form-container");

    if (formElement.style.display === "block") {
        tableElement.style.display = "block";
        formElement.style.display = "none";
        fadeIn(tableElement)
        showFormButtonElement.innerHTML = "Show form";
    } else {
        tableElement.style.display = "none";
        formElement.style.display = "block";
        fadeIn(formElement);
        showFormButtonElement.innerHTML = "Show table";
    }
}

// Toggles the display property of the rows with index >= 3
function toggleTableRows() {
    const tBodyRowsElements = document.getElementById("main-table-body").getElementsByTagName("tr");
    let isVisible = false;

    if (tBodyRowsElements.length > 3) {
        for (let i = 3; i < tBodyRowsElements.length; i++) {
            if (tBodyRowsElements[i].style.display === "none" || tBodyRowsElements[i].style.display === "") {
                tBodyRowsElements[i].style.display = "table-row";
                isVisible = true;
            } else {
                tBodyRowsElements[i].style.display = "none";
                isVisible = false;
            }
        }
    }

    const showAllTableButton = document.getElementById("main-show-all-table-button");
    showAllTableButton.innerHTML = isVisible ? "Show less" : "Show more";
}

