import {  getAll, getById, create, update, remove } from "./httpRequests.js";
import { convertObjectKeysToLowercase, hideTableRows } from "./utils.js";

// Row template
const tableRowTemplate = airline =>{
    let convertedAirline = convertObjectKeysToLowercase(airline);
   return `<tr data-id="${convertedAirline.id}">
        <td>${convertedAirline.name}</td>
        <td>${new Date(convertedAirline.founded).toLocaleDateString('en-GB')}</td>
        <td>${convertedAirline.fleetsize}</td>
        <td class="main-table-body-row">${convertedAirline.description}</td>
        <td class="main-table-action-buttons">
            <button class="main-table-edit-action-button" data-id="${convertedAirline.id}">Edit</button>
            <button class="main-table-delete-action-button" data-id="${convertedAirline.id}">Delete</button>
        </td>
    </tr>`;
}

// Adds table row and attaches edit and delete button event listeners
function addTableRow(tableBodyElement,airline){
    if(airline){
        tableBodyElement.insertAdjacentHTML('beforeend',tableRowTemplate(airline));
        const rowElement = tableBodyElement.querySelector(`tr[data-id="${airline.id}"]`);
        const editButton = rowElement.querySelector('.main-table-edit-action-button');
        const deleteButton = rowElement.querySelector('.main-table-delete-action-button');

        attachEditButtonEventListener(editButton);
        attachDeleteButtonEventListener(deleteButton);
    }
}

// Updates the content of a row
function updateTableRow(airline){
    const rowElement = document.querySelector(`tr[data-id="${airline.id}"]`);
    if(rowElement){
        rowElement.innerHTML = tableRowTemplate(airline);
        
        const editButton = rowElement.querySelector('.main-table-edit-action-button');
        const deleteButton = rowElement.querySelector('.main-table-delete-action-button');

        attachEditButtonEventListener(editButton);
        attachDeleteButtonEventListener(deleteButton);
    }
}

// Removes a table row
function removeTableRow(id){
    const rowElement = document.querySelector(`tr[data-id="${id}"]`);
    if(rowElement){
        rowElement.remove();
        hideTableRows();
    }
}

// Loads all airlines in the table
async function loadAirlinesTable() {
    const airlines = await getAll('Airline');
    const tableBodyElement = document.getElementById('main-table-body');
    tableBodyElement.innerHTML = '';

    if (airlines.length > 0) {
        airlines.forEach(airline => {
            addTableRow(tableBodyElement,airline);
        });
    } else {
        const noAirlinesRowElement = `<tr><td colspan="5">No airlines found</td></tr>`;
        tableBodyElement.innerHTML = noAirlinesRowElement;
    }
    hideTableRows();
   
}


function attachEditButtonEventListener(element) {
    element.addEventListener('click', async (event) => {
        const id = event.target.getAttribute('data-id');
        await populateForm(id);
        toggleViews();
    });
}

function attachDeleteButtonEventListener(element) {
    element.addEventListener('click', async (event) => {
        const id = event.target.getAttribute('data-id');
        removeAirline(id);
    });
}

// Fills in the input fields of the form with the given data
async function populateForm(id) {
    const airline = await getById('Airline', id);
    if (airline) {
        document.getElementById('airline-id').value = airline.id;
        document.getElementById('airline-name').value = airline.name;
        document.getElementById('airline-founded').value = airline.founded;
        document.getElementById('airline-fleet-size').value = airline.fleetSize;
        document.getElementById('airline-description').value = airline.description;
    }
}

// Creates or updates an airline based on the id value
// After the action, the form gets cleared and the table view gets shown
async function createOrUpdateAirline(event) {
    event.preventDefault();
    const formElement = document.getElementById('airline-form');
    const formData = new FormData(formElement);
    const airlinesData = Object.fromEntries(formData.entries());
    const airlineId = document.getElementById('airline-id').value;

    const { id, ...dataWithoutId } = airlinesData;

    try {
        let response;
        if (airlineId) {
            response = await update('Airline', airlinesData);
            updateTableRow(airlinesData);
            
        } else {
            response = await create('Airline', dataWithoutId);
            loadAirlinesTable();
        }
        if (response) {
            toggleViews();
            formElement.reset();
            document.getElementById('airline-id').value = '';
        } else {
            alert('Something went wrong. Please try again. :(');
        }
    } catch (error) {
        console.error(error);
        alert('Something went wrong. Please try again. :(');
    }
}

// Removes an airline and deletes the table row corresponding to it
async function removeAirline(id){
    if(confirm("Are you sure you want to delete this airline?")){
        const response = await remove('Airline',id);
        if(response){
            alert('Airline successfully deleted.');
            removeTableRow(id);
        }else{
            alert('Something went wrong. Please, try again. :(');
        }
   }
}

document.getElementById('airline-form').addEventListener('submit', createOrUpdateAirline);

window.addEventListener('load', async () => {
    loadAirlinesTable();
});
