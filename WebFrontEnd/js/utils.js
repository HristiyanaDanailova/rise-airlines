// Hides excess rows ( rows with index >= 3 )
export function hideTableRows(){
    const tBodyRowsElements = document.getElementById("main-table-body").getElementsByTagName("tr");
    if (tBodyRowsElements.length > 3) {
        for (let i = 3; i<tBodyRowsElements.length; i++) {
            tBodyRowsElements[i].style.display = "none";
        }
        
        const showAllTableButton = document.getElementById("main-show-all-table-button");
        showAllTableButton.innerHTML= 'Show more';
        showAllTableButton.style.display = "block";
    }else{
        const showAllTableButton = document.getElementById("main-show-all-table-button");
        showAllTableButton.style.display = "none";   
    }
}

// converts object keys to lowerCase
export function convertObjectKeysToLowercase(obj) {
    const lowercaseObj = {};
    for (const key in obj) {
        lowercaseObj[key.toLowerCase()] = obj[key];
    }
    return lowercaseObj;
}