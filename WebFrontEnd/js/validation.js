﻿// validates dates of format yyyy-mm-dd where year is between 1500 and 2199
function isValidDate(dateString) {
    const regex = /^(?:15\d{2}|16\d{2}|17\d{2}|18\d{2}|19\d{2}|20\d{2}|21\d{2})-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])$/;
    return regex.test(dateString);
}

// validates dates of format yyyy-mm-dd hh:mm, where the date can't be past
function isValidDateTime(dateString) {
    const regex = /^(?:15\d{2}|16\d{2}|17\d{2}|18\d{2}|19\d{2}|20\d{2}|21\d{2})-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\d|3[01])T(?:[01]\d|2[0-3]):(?:[0-5]\d)$/;
    return regex.test(dateString) && (new Date(dateString) > Date.now());
}

// checks if departure date and time is before arrival date and time
function isValidTimeFrame() {
    const departureDateTime = document.getElementById("flight-departure-date-time").value;
    const arrivalDateTime = document.getElementById("flight-arrival-date-time").value;
    return Date.parse(departureDateTime) < Date.parse(arrivalDateTime);
}

// Defines the validation functions and error messages for every input field
const validators = {
    // Airline validation
    'airline-name': {
        validate: value => /^[a-z0-9 ]{1,512}$/i.test(value),
        errorMessage: 'Airline name should contain only alphanumeric symbols.'
    },
    'airline-founded': {
        validate: isValidDate,
        errorMessage: 'Invalid date.'
    },
    'airline-fleet-size': {
        validate: value => !isNaN(value) && value >= 0 && value <= 1000000,
        errorMessage: 'Invalid fleet size. Number must be in the interval [0-1000000]'
    },
    'airline-description': {
        validate: value => value.length > 0 && value.length <= 1000000,
        errorMessage: 'Airline description can\'t be longer than 1000000 symbols.'
    },
    // Airport validation
    'airport-name': {
        validate: value => /^[a-z0-9 ]{1,512}$/i.test(value),
        errorMessage: 'Airport name should contain only alphanumeric symbols.'
    },
    'airport-city': {
        validate: value => /^[a-z0-9 ]{1,256}$/i.test(value),
        errorMessage: 'City name should contain only alphanumeric symbols.'
    },
    'airport-country': {
        validate: value => /^[a-z0-9 ]{1,256}$/i.test(value),
        errorMessage: 'Country name should contain only alphanumeric symbols.'
    },
    'airport-code': {
        validate: value => /^[a-z0-9]{3}$/i.test(value),
        errorMessage: 'Airport code should contain exactly 3 alphanumeric symbols.'
    },
    'airport-founded': {
        validate: isValidDate,
        errorMessage: 'Invalid date.'
    },
    'airport-runways-count': {
        validate: value => !isNaN(value) && value >= 0 && value <= 1000000,
        errorMessage: 'Invalid runways count. Number must be in the interval [0-1000000]'
    },
    // Flight validaion
    'flight-flight-number': {
        validate: value => /^[a-z0-9]{5}$/i.test(value),
        errorMessage: 'Flight number should contain exactly alphanumeric 5 symbols.'
    },
    'flight-airline-name': {
        validate: value => /^[a-z0-9 ]{1,512}$/i.test(value),
        errorMessage: 'Airline name should contain between 1 and 512 alphanumeric symbols.'
    },
    'flight-departure-airport-name': {
        validate: value => /^[a-z0-9 ]{1,512}$/i.test(value),
        errorMessage: 'Departure airport name should contain between 1 and 512 alphanumeric symbols.'
    },
    'flight-arrival-airport-name': {
        validate: value => /^[a-z0-9 ]{1,512}$/i.test(value),
        errorMessage: 'Arrival airport name should contain between 1 and 512 alphanumeric symbols.'
    },
    'flight-departure-date-time': {
        validate: value => { return (isValidDateTime(value) && isValidTimeFrame())},
        errorMessage: 'Deparure time should be in the future and of format dd/mm/yyyy hh:mm. Departure time can\'t be past the arrival time'
    },
    'flight-arrival-date-time': {
        validate: value => { return isValidDateTime(value) && isValidTimeFrame() },
        errorMessage: 'Arrival time should be in the future and of format dd/mm/yyyy hh:mm. Arrival time can\'t be before departure time.'
    },
};

// Checks for invalid values and displays/hides the error messages
function validateInput(inputElement, errorMessageElement) {
    const inputId = inputElement.id;
    const validator = validators[inputId];
    const value = inputElement.value.trim();

    if (value === '') {
        errorMessageElement.innerHTML = `Field is required.`;
        errorMessageElement.style.display = 'block';
    } else if (!validator.validate(value)) {
        errorMessageElement.innerHTML = validator.errorMessage;
        errorMessageElement.style.display = 'block';
    } else {
        errorMessageElement.style.display = 'none';
    }
}

// Attaches blur event listeners to input tags
document.addEventListener('blur', event => {
    const inputElement = event.target;
    if (inputElement.matches('input')) {
        const errorMessageElement = document.getElementById(`${inputElement.id}-error`);
        validateInput(inputElement, errorMessageElement);
        updateSubmitButton();
    }
}, true);


// Disables/enables the submit button based on the displayed error message divs
function updateSubmitButton() {
    const submitButton = document.getElementById('main-form-submit-button');
    submitButton.disabled = checkFormErrors();
    submitButton.classList.toggle('main-form-submit-button-disabled', submitButton.disabled);
}

// Checks if any error divs are displayed
function checkFormErrors() {
    const errorElements = document.querySelectorAll('.main-form-error-field');

    for (let i = 0; i < errorElements.length; i++) {
        if (errorElements[i].style.display === 'block') {
            return true;
        }
    }
    return false;
}

