import { getAll, getById , create, update, remove } from "./httpRequests.js";
import { hideTableRows, convertObjectKeysToLowercase } from "./utils.js";

// Row template
const tableRowTemplate = flight =>{
    let convertedFlight = convertObjectKeysToLowercase(flight);
   return  `<tr data-id=${convertedFlight.id}>
   <td>${convertedFlight.flightnumber}</td>
   <td>${convertedFlight.airlinename}</td>
   <td>${convertedFlight.departureairportname}</td>
   <td>${convertedFlight.arrivalairportname}</td>
   <td>${new Date(convertedFlight.departuredatetime).toLocaleDateString('en-GB')} ${new Date(convertedFlight.departuredatetime).toLocaleTimeString('en-GB',{ hour: '2-digit', minute: '2-digit' })}</td>
   <td>${new Date(convertedFlight.arrivaldatetime).toLocaleDateString('en-GB')} ${new Date(convertedFlight.arrivaldatetime).toLocaleTimeString('en-GB',{ hour: '2-digit', minute: '2-digit' })}</td>
   <td class="main-table-action-buttons">
   <button class="main-table-edit-action-button" data-id="${convertedFlight.id}">Edit</button>
   <button class="main-table-delete-action-button" data-id="${convertedFlight.id}">Delete</button></td>
</tr>`;
}

// Adds table row and attaches edit and delete button event listeners
function addTableRow(tableBodyElement,flight){
    if(flight){
        tableBodyElement.insertAdjacentHTML('beforeend',tableRowTemplate(flight));
        const rowElement = tableBodyElement.querySelector(`tr[data-id="${flight.id}"]`);
        const editButton = rowElement.querySelector('.main-table-edit-action-button');
        const deleteButton = rowElement.querySelector('.main-table-delete-action-button');

        attachEditButtonEventListener(editButton);
        attachDeleteButtonEventListener(deleteButton);
    }
}

// Updates the content of a row
function updateTableRow(flight){
    const rowElement = document.querySelector(`tr[data-id="${flight.id}"]`);
    if(rowElement){
        rowElement.innerHTML = tableRowTemplate(flight);
        
        const editButton = rowElement.querySelector('.main-table-edit-action-button');
        const deleteButton = rowElement.querySelector('.main-table-delete-action-button');

        attachEditButtonEventListener(editButton);
        attachDeleteButtonEventListener(deleteButton);
    }
}

// Removes a table row
function removeTableRow(id){
    const rowElement = document.querySelector(`tr[data-id="${id}"]`);
    if(rowElement){
        rowElement.remove();
        hideTableRows();
    }
}

// Loads all flights in the table
async function loadFlightsTable(){
    const flights = await getAll('Flight');
    const tableBodyElement = document.getElementById('main-table-body');
    tableBodyElement.innerHTML = '';

        if(flights.length > 0){
            flights.forEach(flight => {
                addTableRow(tableBodyElement,flight);
            });
        }else{
            const noFlightsRowElement = '<tr><td colspan="6">No flights found</td></tr>';
            tableBodyElement.innerHTML = noFlightsRowElement;
        }
        hideTableRows();
}

function attachEditButtonEventListener(element) {
    element.addEventListener('click', async (event) => {
        const id = event.target.getAttribute('data-id');
        await populateForm(id);
        toggleViews();
    });
}

function attachDeleteButtonEventListener(element) {
    element.addEventListener('click', async (event) => {
        const id = event.target.getAttribute('data-id');
        removeFlight(id);
    });
}

// Fills in the input fields of the form with the given data
async function populateForm(id) {
    const flight = await getById('Flight', id);
    if (flight) {
        document.getElementById('flight-id').value = flight.id;
        document.getElementById('flight-flight-number').value = flight.flightNumber
        document.getElementById('flight-airline-name').value = flight.airlineName;
        document.getElementById('flight-departure-airport-name').value = flight.departureAirportName;
        document.getElementById('flight-arrival-airport-name').value = flight.arrivalAirportName;
        document.getElementById('flight-departure-date-time').value = flight.departureDateTime;
        document.getElementById('flight-arrival-date-time').value = flight.arrivalDateTime;
    }
}

// Creates or updates a flight based on the id value
// After the action, the form gets cleared and the table view gets shown
async function createOrUpdateFlight(event) {
    event.preventDefault();
    const formElement = document.getElementById('flight-form');
    const formData = new FormData(formElement);
    const flightsData = Object.fromEntries(formData.entries());
    const flightId = document.getElementById('flight-id').value;

    const {id, ...dataWithoutId} = flightsData;
    try {
        let response;
        if (flightId) {
            response = await update('Flight', flightsData);
            updateTableRow(flightsData);
        } else {
        
            response = await create('Flight', dataWithoutId);
            loadFlightsTable();
        }

        if (response) {
            toggleViews();
            formElement.reset();
            document.getElementById('flight-id').value = '';
        } else {
            alert('Something went wrong. Please try again. :(');
        }
    } catch (error) {
        console.error(error);
        alert('Something went wrong. Please try again. :(');
    }
}

// Removes a flight and deletes the table row corresponding to it
async function removeFlight(id){
    if(confirm("Are you sure you want to delete this flight?")){
        const response = await remove('Flight',id);
        if(response){
            alert('Flight successfully deleted.');
            removeTableRow(id);
        }else{
            alert('Something went wrong. Please, try again. :(');
        }
   }
}


document.getElementById('flight-form').addEventListener('submit', createOrUpdateFlight);

window.addEventListener('load',async ()=>{
    loadFlightsTable();
});
