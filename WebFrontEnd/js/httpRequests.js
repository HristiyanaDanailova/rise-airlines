async function getAll(type) {
    try {
        const response = await fetch(`https://localhost:7042/api/${type}`);
        if (!response.ok) {
            return [];
        }
        return response.json();
    } catch (error) {
        console.error('An error occurred:', error);
        return [];
    }
}

async function getById(type,id){
    try {
        const response = await fetch(`https://localhost:7042/api/${type}/${id}`);
        if (!response.ok) {
            return null;
        }
        return response.json();
    } catch (error) {
        console.error('An error occurred:', error);
        return null;
    }
}

async function create(type, data) {
    try {
        const response = await fetch(`https://localhost:7042/api/${type}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (response.ok) {
           return true;
        } else {
            const errorData = await response.json();
            console.error(`Error: ${response.status} - ${errorData.message}`);
            return false;
        }
    } catch (error) {
        console.error(error);
        return false;
    }
}

async function update(type, data) {
    try {
        const response = await fetch(`https://localhost:7042/api/${type}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (response.ok) {
           return true;
        } else {
            const errorData = await response.json();
            console.error(`Error: ${response.status} - ${errorData.message}`);
            return false;
        }
    } catch (error) {
        console.error(error);
        return false;
    }
}

async function remove(type,id){
    try {
        const response = await fetch(`https://localhost:7042/api/${type}/${id}`, {
            method: 'DELETE'
        });
        if (response.ok) {
           return true;
        } else {
            const errorData = await response.json();
            console.error(`Error: ${response.status} - ${errorData.message}`);
            return false;
        }
    } catch (error) {
        console.error(error);
        return false;
    }
}

export{
    getAll,
    getById,
    create,
    update,
    remove
}