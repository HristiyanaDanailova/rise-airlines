import { getAll } from "./httpRequests.js";

// Loads the number of airports, airlines and flights and displays them in the cards
window.addEventListener('load',async ()=>{
    const airlines = await getAll('Airline');
    const airports = await getAll('Airport');
    const flights = await getAll('Flight');
    
    document.getElementById('main-info-card-content-airlines').innerHTML=`${airlines.length}`;
    document.getElementById('main-info-card-content-airports').innerHTML=`${airports.length}`;
    document.getElementById('main-info-card-content-flights').innerHTML=`${flights.length}`;

});