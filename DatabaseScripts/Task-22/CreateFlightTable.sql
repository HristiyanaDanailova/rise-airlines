CREATE TABLE Flight(
	Id INT PRIMARY KEY IDENTITY(1,1),
	FlightNumber CHAR(5) NOT NULL,
	AirlineId INT NOT NULL,
	DepartureAirportId INT NOT NULL,
	ArrivalAirportId INT NOT NULL,
	DepartureDateTime DATETIME NOT NULL,
	ArrivalDateTime DATETIME NOT NULL,
	CONSTRAINT FK_DepartureAirport FOREIGN KEY (DepartureAirportId) REFERENCES Airport(id),
	CONSTRAINT FK_ArrivalAirport FOREIGN KEY (ArrivalAirportId) REFERENCES Airport(id)
);