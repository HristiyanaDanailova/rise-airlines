﻿-- Insert data
INSERT INTO Airport(Name, Country, City, Code, RunwaysCount, Founded) 
VALUES ('Airport1','France','Paris','FRC',2,'2024-03-03');

INSERT INTO Airport(Name, Country, City, Code, RunwaysCount, Founded) 
VALUES ('Airport2','Turkey','Izmir','TRK',3,'2024-04-03');

INSERT INTO Airport(Name, Country, City, Code, RunwaysCount, Founded) 
VALUES ('Airport3','Germany','Berlin','GRM',4,'2024-01-03');

INSERT INTO Flight (FlightNumber, DepartureAirportId, ArrivalAirportId, DepartureDateTime, ArrivalDateTime)
VALUES ('FL001', 1, 2, '2023-03-03 08:00:00', '2023-03-03 14:00:00');

INSERT INTO Flight (FlightNumber, DepartureAirportId, ArrivalAirportId, DepartureDateTime, ArrivalDateTime)
VALUES ('FL002', 2, 1, '2024-01-02 09:00:00', '2022-01-02 11:30:00');

-- Select data
SELECT * FROM FLight WHERE FLightNumber='FL001';

SELECT * FROM FLight;

SELECT FlightNumber, DepartureAirportId, ArrivalAirportId, DepartureDateTime, ArrivalDateTime
FROM Flight;

SELECT * FROM Flight WHERE DepartureAirportId=1;

-- Update data
UPDATE Flight
SET ArrivalAirportId = 3
WHERE FlightNumber = 'FL001';

-- Delete data
DELETE FROM Flight
WHERE FlightNumber = 'FL002';