-- Adds constraints to ensure that the departure and arrival DateTimes are not past
ALTER TABLE Flight
ADD CONSTRAINT CHK_DepartuteDateTime_NotPast CHECK (DepartureDateTime >= GETDATE());

ALTER TABLE Flight
ADD CONSTRAINT CHK_ArrivalDateTime_NotPast CHECK (ArrivalDateTime >= GETDATE());

ALTER TABLE Flight
ADD CONSTRAINT CHK_Arrival_After_Departure CHECK (ArrivalDateTime > DepartureDateTime);
