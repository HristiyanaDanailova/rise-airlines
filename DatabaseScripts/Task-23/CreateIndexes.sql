-- Default clustered indexes already exist for the Id's (primary key) in the tables

-- Creates a nonclustered index for Airline name
CREATE NONCLUSTERED INDEX IX_Airline_Name ON Airline (Name);


-- Creates nonclustered indexes for Airline name and code
CREATE NONCLUSTERED INDEX IX_Airport_Name ON Airport (Name);

CREATE NONCLUSTERED INDEX IX_Airport_Code ON Airport (Code);


-- Creates nonclustured indexes for Flight number, deparuteAirportId and arrivalAirportId
CREATE NONCLUSTERED INDEX IX_Flight_FlightNumber ON Flight (FlightNumber);

CREATE NONCLUSTERED INDEX IX_Flight_DeparuteAirportId ON Flight (DepartureAirportId);

CREATE NONCLUSTERED INDEX IX_Flight_ArrivalAirportId ON Flight (ArrivalAirportId);