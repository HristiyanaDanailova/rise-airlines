-- Shows the information for all flights, scheduled for tomorrow and the airports, associated with them
SELECT 
    Flight.FlightNumber,
    Flight.DepartureDateTime AS DepartureTime,
    Flight.ArrivalDateTime AS ArrivalTime,
    DepartureAirport.Name AS DepartureAirportName,
    DepartureAirport.Country AS DepartureAirportCountry,
    DepartureAirport.City AS DepartureAirportCity,
    DepartureAirport.Code AS DepartureAirportCode,
    ArrivalAirport.Name AS ArrivalAirportName,
    ArrivalAirport.Country AS ArrivalAirportCountry,
    ArrivalAirport.City AS ArrivalAirportCity,
    ArrivalAirport.Code AS ArrivalAirportCode
FROM 
    Flight
JOIN 
    Airport AS DepartureAirport ON Flight.DepartureAirportId = DepartureAirport.Id
JOIN 
    Airport AS ArrivalAirport ON Flight.ArrivalAirportId = ArrivalAirport.Id
WHERE 
     DATEDIFF(DAY, Flight.DepartureDateTime, GETDATE() + 1) = 0;