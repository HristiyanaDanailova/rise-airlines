-- Selects the fartherest flight, based on the difference of the departure and arrival times ( in seconds )
-- Dispalys the deparute and arrival airport names
SELECT TOP 1
    Flight.FlightNumber,
	Flight.DepartureDateTime AS DepartureTime,
	Flight.ArrivalDateTime AS ArrivalTime,
	ArrivalAirport.Name AS ArrivalAirportName,
	DepartureAirport.Name AS DepartureAirportName
FROM 
    Flight
JOIN
	Airport AS ArrivalAirport ON Flight.ArrivalAirportId = ArrivalAirport.Id
JOIN
	Airport AS DepartureAirport ON Flight.DepartureAirportId = DepartureAirport.Id
ORDER BY
	DATEDIFF(SECOND,DepartureDateTime,ArrivalDateTime) DESC;
    