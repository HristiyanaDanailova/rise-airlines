-- Shows the number of departured flights
SELECT 
	COUNT(*) AS DeparturedFlightsCount
FROM 
	Flight
WHERE 
	Flight.DepartureDateTime < GETDATE(); 