﻿
using AirlinesBusinessWeb.DTO;

namespace AirlinesBusinessWeb.Validation;
public class AirportValidation
{
    private static bool IsValidAirportName(string airportName)
    {
        if (string.IsNullOrEmpty(airportName) || airportName.Length > 512)
        {

            return false;
        }
        return true;
    }

    private static bool IsValidAirportCity(string cityName)
    {
        if (string.IsNullOrEmpty(cityName) || cityName.Length > 256)
        {
            return false;
        }
        return true;
    }

    private static bool IsValidAirportCountry(string countryName)
    {
        if (string.IsNullOrEmpty(countryName) || countryName.Length > 256)
        {
            return false;
        }
        return true;
    }

    private static bool IsValidAirportCode(string code)
    {
        if (code.Length != 3 || HasUnicodeCharacter(code))
        {
            return false;
        }
        return true;

    }

    private static bool IsValidAirportRunwaysCount(int runwaysCount)
    {
        if (runwaysCount < 0 || runwaysCount > 1000000)
        {
            return false;
        }
        return true;
    }

    private static bool HasUnicodeCharacter(string input)
    {
        const int maxAnsiCode = 255;

        return input.Any(c => c > maxAnsiCode);
    }

    public static bool IsValidAirport(AirportDTO airport)
    {
        return (IsValidAirportName(airport.Name) && IsValidAirportCity(airport.City) && IsValidAirportCountry(airport.Country) && IsValidAirportCode(airport.Code) && IsValidAirportRunwaysCount(airport.RunwaysCount));
    }
}
