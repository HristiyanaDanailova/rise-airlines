﻿
using AirlinesBusinessWeb.DTO;

namespace AirlinesBusinessWeb.Validation;
public class FlightValidation
{
    private static bool IsValidFlightCode(string flightCode)
    {
        if (flightCode.Length != 5 || HasUnicodeCharacter(flightCode))
        {
            return false;
        }
        return true;
    }

    private static bool IsValidFlightDateTime(DateTime departureDateTime)
    {
        if (departureDateTime < DateTime.Now)
        {
            return false;
        }
        return true;
    }

    private static bool IsDepartureBeforeArrivalDate(DateTime departureDateTime, DateTime arrivalDateTime)
    {
        if (departureDateTime >= arrivalDateTime)
        {
            return false;
        }
        return true;
    }

    private static bool HasUnicodeCharacter(string input)
    {
        const int maxAnsiCode = 255;

        return input.Any(c => c > maxAnsiCode);
    }

    public static bool IsValidFlight(FlightDTO flight)
    {
        return (IsValidFlightCode(flight.FlightNumber) && IsValidFlightDateTime(flight.DepartureDateTime) && IsValidFlightDateTime(flight.ArrivalDateTime) && IsDepartureBeforeArrivalDate(flight.DepartureDateTime, flight.ArrivalDateTime));
    }
}
