﻿using AirlinesBusinessWeb.DTO;

namespace AirlinesBusinessWeb.Validation;
public class AirlineValidation
{
    private static bool IsValidAirlineName(string airlineName)
    {
        if (string.IsNullOrEmpty(airlineName) || airlineName.Length > 512)
        {
            return false;
        }
        return true;
    }

    private static bool IsValidAirlineFleetSize(int fleetSize)
    {
        if (fleetSize < 0 || fleetSize > 1000000)
        {
            return false;
        }
        return true;
    }

    private static bool IsValidAirlineDescription(string descritpion)
    {
        if (string.IsNullOrEmpty(descritpion))
        {
            return false;
        }
        return true;
    }

    public static bool IsValidAirline(AirlineDTO airline)
    {
        return (IsValidAirlineName(airline.Name) && IsValidAirlineFleetSize(airline.FleetSize) && IsValidAirlineDescription(airline.Description));
    }
}
