﻿using AirlinesBusinessWeb.DTO;
using AirlinesBusinessWeb.Profiles;
using AirlinesBusinessWeb.Validation;

using AirlinesPersistenceBasic.Repositories.Interfaces;
using AirportEntity = AirlinesPersistenceBasic.Entities.Airport;

namespace AirlinesBusinessWeb.Services;
public class AirportService : IAirportService
{
    private readonly IAirportRepository _airportRepository;
    private readonly AirportMapper _airportMapper;

    public AirportService(IAirportRepository airportRepository, AirportMapper airportMapper)
    {
        _airportRepository = airportRepository;
        _airportMapper = airportMapper;
    }
    // Get by column
    public AirportDTO? GetAirportById(int id)
    {
        AirportEntity? airport = _airportRepository.GetAirportById(id);
        if (airport != null)
        {
            return _airportMapper.MapToDTO(airport);
        }
        return null;

    }
    public AirportDTO? GetAirportByCode(string code)
    {
        AirportEntity? airport = _airportRepository.GetAirportByCode(code);
        if (airport != null)
        {
            return _airportMapper.MapToDTO(airport);
        }
        return null;
    }

    // Select airports based on a criteria
    public List<AirportDTO> GetAirportsByCity(string city)
    {
        List<AirportEntity> airports = _airportRepository.GetAirportsByCity(city);
        return airports.Select(_airportMapper.MapToDTO).OfType<AirportDTO>().ToList();
    }
    public List<AirportDTO> GetAirportsByCountry(string country)
    {
        List<AirportEntity> airports = _airportRepository.GetAirportsByCountry(country);
        return airports.Select(_airportMapper.MapToDTO).OfType<AirportDTO>().ToList();
    }
    public List<AirportDTO> GetAirportsByName(string name)
    {
        List<AirportEntity> airports = _airportRepository.GetAirportsByName(name);
        return airports.Select(_airportMapper.MapToDTO).OfType<AirportDTO>().ToList();
    }

    // Sort airports by runways count or founded date
    public List<AirportDTO> SortAirportsByRunwaysCountDesc()
    {
        List<AirportEntity> airports = _airportRepository.SortAirportsByRunwaysCountDesc();
        return airports.Select(_airportMapper.MapToDTO).OfType<AirportDTO>().ToList();
    }
    public List<AirportDTO> SortAirportsByRunwaysCountAsc()
    {
        List<AirportEntity> airports = _airportRepository.SortAirportsByRunwaysCountAsc();
        return airports.Select(_airportMapper.MapToDTO).OfType<AirportDTO>().ToList();
    }
    public List<AirportDTO> SortAirportsByFoundedDateDesc()
    {
        List<AirportEntity> airports = _airportRepository.SortAirportsByFoundedDateDesc();
        return airports.Select(_airportMapper.MapToDTO).OfType<AirportDTO>().ToList();
    }
    public List<AirportDTO> SortAirportsByFoundedDateAsc()
    {
        List<AirportEntity> airports = _airportRepository.SortAirportsByFoundedDateAsc();
        return airports.Select(_airportMapper.MapToDTO).OfType<AirportDTO>().ToList();
    }

    // Basic CRUD operations
    public List<AirportDTO> GetAirports()
    {
        List<AirportEntity> airports = _airportRepository.GetAirports();
        return airports.Select(_airportMapper.MapToDTO).OfType<AirportDTO>().ToList();
    }
    public bool AddAirport(AirportDTO airport)
    {
        if (AirportValidation.IsValidAirport(airport))
        {
            AirportEntity? airportEntity = _airportMapper.MapToEntity(airport);
            return airportEntity != null && _airportRepository.AddAirport(airportEntity);
        }
        return false;
    }
    public bool UpdateAirport(AirportDTO airport)
    {
        if (AirportValidation.IsValidAirport(airport))
        {
            AirportEntity? airportEntity = _airportMapper.MapToEntity(airport);
            return airportEntity != null && _airportRepository.UpdateAirport(airportEntity);
        }
        return false;
    }
    public bool DeleteAirport(int id)
    {
        return _airportRepository.DeleteAirport(id);
    }

    public List<AirportDTO> GetAirportsByFilter(string value, string objectProperty)
    {
        List<AirportEntity> airports = _airportRepository.GetAirportsByFilter(value, objectProperty);
        return airports.Select(_airportMapper.MapToDTO).OfType<AirportDTO>().ToList();
    }
}
