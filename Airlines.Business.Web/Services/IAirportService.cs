﻿using AirlinesBusinessWeb.DTO;

namespace AirlinesBusinessWeb.Services;
public interface IAirportService
{
    // Get by column
    AirportDTO? GetAirportById(int id);
    AirportDTO? GetAirportByCode(string code);

    // Select airports based on a criteria
    List<AirportDTO> GetAirportsByCity(string city);
    List<AirportDTO> GetAirportsByCountry(string country);
    List<AirportDTO> GetAirportsByName(string name);

    // Sort airports by runways count or founded date
    List<AirportDTO> SortAirportsByRunwaysCountDesc();
    List<AirportDTO> SortAirportsByRunwaysCountAsc();
    List<AirportDTO> SortAirportsByFoundedDateDesc();
    List<AirportDTO> SortAirportsByFoundedDateAsc();

    // Basic CRUD operations
    List<AirportDTO> GetAirports();
    bool AddAirport(AirportDTO airport);
    bool UpdateAirport(AirportDTO airport);
    bool DeleteAirport(int id);

    List<AirportDTO> GetAirportsByFilter(string value, string objectProperty);
}
