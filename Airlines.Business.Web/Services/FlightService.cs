﻿using AirlinesBusinessWeb.DTO;
using AirlinesBusinessWeb.Profiles;
using AirlinesBusinessWeb.Validation;

using AirlinesPersistenceBasic.Repositories.Interfaces;
using FlightEntity = AirlinesPersistenceBasic.Entities.Flight;

namespace AirlinesBusinessWeb.Services;
public class FlightService : IFlightService
{
    private readonly IFlightRepository _flightRepository;
    private readonly FlightMapper _flightMapper;
    public FlightService(IFlightRepository flightRepository, FlightMapper flightMapper)
    {
        _flightRepository = flightRepository;
        _flightMapper = flightMapper;
    }
    // Get by column
    public FlightDTO? GetFlightById(int id)
    {
        FlightEntity? flight = _flightRepository.GetFlightById(id);
        if (flight != null)
        {
            return _flightMapper.MapToDTO(flight);
        }
        return null;
    }
    public FlightDTO? GetFlightByFlightNumber(string flightNumber)
    {
        FlightEntity? flight = _flightRepository.GetFlightByFlightNumber(flightNumber);
        if (flight != null)
        {
            return _flightMapper.MapToDTO(flight);
        }
        return null;
    }
    // Farthest flight based on deparute and arrival time
    public FlightDTO? GetFarthestFlight()
    {
        FlightEntity? flight = _flightRepository.GetFarthestFlight();
        if (flight != null)
        {
            return _flightMapper.MapToDTO(flight);
        }
        return null;
    }

    // Select flights based on a criteria
    public List<FlightDTO> GetFlightsFromDepartureAirportById(int departureAirportId)
    {
        List<FlightEntity> flights = _flightRepository.GetFlightsFromDepartureAirportById(departureAirportId);
        return flights.Select(_flightMapper.MapToDTO).OfType<FlightDTO>().ToList();
    }
    public List<FlightDTO> GetFlightsToArrivalAirportById(int arrivalAirportId)
    {
        List<FlightEntity> flights = _flightRepository.GetFlightsToArrivalAirportById(arrivalAirportId);
        return flights.Select(_flightMapper.MapToDTO).OfType<FlightDTO>().ToList();
    }
    public List<FlightDTO> GetFlightsByAirlineById(int id)
    {
        List<FlightEntity> flights = _flightRepository.GetFlightsByAirlineById(id);
        return flights.Select(_flightMapper.MapToDTO).OfType<FlightDTO>().ToList();
    }
    public List<FlightDTO> GetFlightsFromDepartureAirportByName(string departureAirportName)
    {
        List<FlightEntity> flights = _flightRepository.GetFlightsFromDepartureAirportByName(departureAirportName);
        return flights.Select(_flightMapper.MapToDTO).OfType<FlightDTO>().ToList();
    }
    public List<FlightDTO> GetFlightsToArrivalAirportByName(string arrivalAirportName)
    {
        List<FlightEntity> flights = _flightRepository.GetFlightsToArrivalAirportByName(arrivalAirportName);
        return flights.Select(_flightMapper.MapToDTO).OfType<FlightDTO>().ToList();
    }

    public List<FlightDTO> GetFlightsByAirlineName(string airlineName)
    {
        List<FlightEntity> flights = _flightRepository.GetFlightsByAirlineByName(airlineName);
        return flights.Select(_flightMapper.MapToDTO).OfType<FlightDTO>().ToList();
    }
    public List<FlightDTO> GetFlightsByDepartureAirportCode(string airportCode)
    {
        List<FlightEntity> flights = _flightRepository.GetFlightsByDepartureAirportCode(airportCode);
        return flights.Select(_flightMapper.MapToDTO).OfType<FlightDTO>().ToList();
    }
    public List<FlightDTO> GetFlightsByArrivalAirportCode(string airportCode)
    {
        List<FlightEntity> flights = _flightRepository.GetFlightsByArrivalAirportCode(airportCode);
        return flights.Select(_flightMapper.MapToDTO).OfType<FlightDTO>().ToList();
    }


    // Sort flights by flight duration, based on departure and arrival time
    public List<FlightDTO> SortFlightsByFlightDurationDesc()
    {
        List<FlightEntity> flights = _flightRepository.SortFlightsByFlightDurationDesc();
        return flights.Select(_flightMapper.MapToDTO).OfType<FlightDTO>().ToList();
    }
    public List<FlightDTO> SortFlightsByFlightDurationAsc()
    {
        List<FlightEntity> flights = _flightRepository.SortFlightsByFlightDurationAsc();
        return flights.Select(_flightMapper.MapToDTO).OfType<FlightDTO>().ToList();
    }

    // Basic CRUD operations
    public List<FlightDTO> GetFlights()
    {
        List<FlightEntity> flights = _flightRepository.GetFlights();
        return flights.Select(_flightMapper.MapToDTO).OfType<FlightDTO>().ToList();
    }
    public bool AddFlight(FlightDTO flight)
    {
        if (FlightValidation.IsValidFlight(flight))
        {
            FlightEntity? flightEntity = _flightMapper.MapToEntity(flight);
            return flightEntity != null && _flightRepository.AddFlight(flightEntity);
        }
        return false;
    }
    public bool UpdateFlight(FlightDTO flight)
    {
        if (FlightValidation.IsValidFlight(flight))
        {
            FlightEntity? flightEntity = _flightMapper.MapToEntity(flight);
            return flightEntity != null && _flightRepository.UpdateFlight(flightEntity);
        }
        return false;
    }
    public bool DeleteFlight(int id)
    {
        return _flightRepository.DeleteFlight(id);
    }

    public List<FlightDTO> GetFlightsByFilter(string value, string objectProperty)
    {
        List<FlightEntity> flights = _flightRepository.GetFlightsByFilter(value, objectProperty);
        return flights.Select(_flightMapper.MapToDTO).OfType<FlightDTO>().ToList();
    }
}
