﻿using AirlinesBusinessWeb.DTO;
using AirlinesBusinessWeb.Profiles;
using AirlinesBusinessWeb.Validation;

using AirlinesPersistenceBasic.Repositories.Interfaces;
using AirlineEntity = AirlinesPersistenceBasic.Entities.Airline;

namespace AirlinesBusinessWeb.Services;
public class AirlineService : IAirlineService
{
    private readonly IAirlineRepository _airlineRepository;
    private readonly AirlineMapper _airlineMapper;

    public AirlineService(IAirlineRepository airlineRepository, AirlineMapper airlineMapper)
    {
        _airlineRepository = airlineRepository;
        _airlineMapper = airlineMapper;
    }

    // Select by column
    public AirlineDTO? GetAirlineById(int id)
    {
        AirlineEntity? airline = _airlineRepository.GetAirlineById(id);
        if (airline != null)
        {
            return _airlineMapper.MapToDTO(airline);
        }
        return null;
    }
    // Select by a certain criteria
    public List<AirlineDTO> GetAirlinesByName(string name)
    {
        List<AirlineEntity> airlines = _airlineRepository.GetAirlinesByName(name);
        return airlines.Select(_airlineMapper.MapToDTO).OfType<AirlineDTO>().ToList();
    }

    // Sort airlines by fleetSize and founded date
    public List<AirlineDTO> SortAirlinesByFleetSizeDesc()
    {
        List<AirlineEntity> airlines = _airlineRepository.SortAirlinesByFleetSizeDesc();
        return airlines.Select(_airlineMapper.MapToDTO).OfType<AirlineDTO>().ToList();
    }
    public List<AirlineDTO> SortAirlinesByFleetSizeAsc()
    {
        List<AirlineEntity> airlines = _airlineRepository.SortAirlinesByFleetSizeAsc();
        return airlines.Select(_airlineMapper.MapToDTO).OfType<AirlineDTO>().ToList();
    }
    public List<AirlineDTO> SortAirlinesByFoundedDateDesc()
    {
        List<AirlineEntity> airlines = _airlineRepository.SortAirlinesByFoundedDateDesc();
        return airlines.Select(_airlineMapper.MapToDTO).OfType<AirlineDTO>().ToList();

    }
    public List<AirlineDTO> SortAirlinesByFoundedDateAsc()
    {
        List<AirlineEntity> airlines = _airlineRepository.SortAirlinesByFoundedDateAsc();
        return airlines.Select(_airlineMapper.MapToDTO).OfType<AirlineDTO>().ToList();
    }

    // Basic CRUD operations
    public List<AirlineDTO> GetAirlines()
    {
        List<AirlineEntity> airlines = _airlineRepository.GetAirlines();
        return airlines.Select(_airlineMapper.MapToDTO).OfType<AirlineDTO>().ToList();
    }

    public bool AddAirline(AirlineDTO airline)
    {
        if (AirlineValidation.IsValidAirline(airline))
        {
            AirlineEntity? airlineEntity = _airlineMapper.MapToEntity(airline);
            return airlineEntity != null && _airlineRepository.AddAirline(airlineEntity);
        }
        {
            return false;
        }
    }
    public bool UpdateAirline(AirlineDTO airline)
    {
        if (AirlineValidation.IsValidAirline(airline))
        {
            AirlineEntity? airlineEntity = _airlineMapper.MapToEntity(airline);
            return airlineEntity != null && _airlineRepository.UpdateAirline(airlineEntity);
        }
        return false;
    }
    public bool DeleteAirline(int id)
    {
        return _airlineRepository.DeleteAirline(id);
    }

    public List<AirlineDTO> GetAirlinesByFilter(string value, string objectProperty)
    {

        List<AirlineEntity> airlines = _airlineRepository.GetAirlinesByFilter(value, objectProperty);
        return airlines.Select(_airlineMapper.MapToDTO).OfType<AirlineDTO>().ToList();
    }
}
