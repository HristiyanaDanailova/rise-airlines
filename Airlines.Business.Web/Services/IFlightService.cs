﻿using AirlinesBusinessWeb.DTO;

namespace AirlinesBusinessWeb.Services;
public interface IFlightService
{
    // Get by column
    FlightDTO? GetFlightById(int id);
    FlightDTO? GetFlightByFlightNumber(string flightNumber);

    // Farthest flight based on deparute and arrival time
    FlightDTO? GetFarthestFlight();

    // Select flights based on a criteria
    List<FlightDTO> GetFlightsFromDepartureAirportById(int departureAirportId);
    List<FlightDTO> GetFlightsToArrivalAirportById(int arrivalAirportId);
    List<FlightDTO> GetFlightsByAirlineById(int id);
    List<FlightDTO> GetFlightsFromDepartureAirportByName(string departureAirportName);
    List<FlightDTO> GetFlightsToArrivalAirportByName(string arrivalAirportName);
    List<FlightDTO> GetFlightsByAirlineName(string airlineName);
    List<FlightDTO> GetFlightsByDepartureAirportCode(string airportCode);
    List<FlightDTO> GetFlightsByArrivalAirportCode(string airportCode);

    // Sort flights by flight duration, based on departure and arrival time
    List<FlightDTO> SortFlightsByFlightDurationDesc();
    List<FlightDTO> SortFlightsByFlightDurationAsc();

    // Basic CRUD operations
    List<FlightDTO> GetFlights();
    bool AddFlight(FlightDTO flight);
    bool UpdateFlight(FlightDTO flight);
    bool DeleteFlight(int id);

    List<FlightDTO> GetFlightsByFilter(string value, string objectProperty);
}
