﻿using AirlinesBusinessWeb.DTO;

namespace AirlinesBusinessWeb.Services;
public interface IAirlineService
{
    // Select by column
    AirlineDTO? GetAirlineById(int id);

    // Select by a certain criteria
    List<AirlineDTO> GetAirlinesByName(string name);

    // Sort airlines by fleetSize and founded date
    List<AirlineDTO> SortAirlinesByFleetSizeDesc();
    List<AirlineDTO> SortAirlinesByFleetSizeAsc();
    List<AirlineDTO> SortAirlinesByFoundedDateDesc();
    List<AirlineDTO> SortAirlinesByFoundedDateAsc();

    // Basic CRUD operations
    List<AirlineDTO> GetAirlines();
    bool AddAirline(AirlineDTO airline);
    bool UpdateAirline(AirlineDTO airline);
    bool DeleteAirline(int id);

    List<AirlineDTO> GetAirlinesByFilter(string value, string objectProperty);
}
