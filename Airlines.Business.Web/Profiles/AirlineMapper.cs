﻿using AutoMapper;

using AirlinesBusinessWeb.DTO;
using AirlinesPersistenceBasic.Entities;

namespace AirlinesBusinessWeb.Profiles;
public class AirlineMapper
{
    private readonly IMapper _mapper;

    public AirlineMapper()
    {
        var mapperConfig = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Airline, AirlineDTO>().ReverseMap();

        });

        _mapper = mapperConfig.CreateMapper();
    }
    public Airline? MapToEntity(AirlineDTO airlineDTO)
    {
        try
        {
            Airline airline = _mapper.Map<Airline>(airlineDTO);
            return airline;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }

    }
    public AirlineDTO? MapToDTO(Airline airline)
    {
        try
        {
            AirlineDTO airlineDTO = _mapper.Map<AirlineDTO>(airline);
            return airlineDTO;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
    }
}

