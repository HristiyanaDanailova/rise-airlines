﻿using AutoMapper;

using AirlinesBusinessWeb.DTO;
using AirlinesPersistenceBasic.Entities;

namespace AirlinesBusinessWeb.Profiles;
public class FlightMapper
{
    private readonly IMapper _mapper;

    public FlightMapper()
    {
        var mapperConfig = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Flight, FlightDTO>().ReverseMap();
            cfg.CreateMap<Airport, AirportDTO>().ReverseMap();
        });

        _mapper = mapperConfig.CreateMapper();
    }
    public Flight? MapToEntity(FlightDTO flightDTO)
    {
        try
        {
            Flight flight = _mapper.Map<Flight>(flightDTO);
            return flight;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }

    }
    public FlightDTO? MapToDTO(Flight flight)
    {
        try
        {
            FlightDTO flightDTO = _mapper.Map<FlightDTO>(flight);
            return flightDTO;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
    }
}
