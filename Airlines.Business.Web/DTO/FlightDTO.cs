﻿namespace AirlinesBusinessWeb.DTO;

public class FlightDTO
{
    public int Id { get; set; }
    public string FlightNumber { get; set; } = string.Empty;
    public int AirlineId { get; set; }
    public int DepartureAirportId { get; set; }
    public int ArrivalAirportId { get; set; }
    public DateTime DepartureDateTime { get; set; }
    public DateTime ArrivalDateTime { get; set; }

    public FlightDTO(int id, string flightNumber, int airlineId, int departureAirportId, int arrivalAirportId, DateTime departureDateTime, DateTime arrivalDateTime)
    {
        Id = id;
        FlightNumber = flightNumber;
        AirlineId = airlineId;
        DepartureAirportId = departureAirportId;
        ArrivalAirportId = arrivalAirportId;
        DepartureDateTime = departureDateTime;
        ArrivalDateTime = arrivalDateTime;
    }
    public FlightDTO() { }
}
