﻿namespace AirlinesBusinessWeb.DTO;

public class AirportDTO
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Country { get; set; }
    public string City { get; set; }
    public string Code { get; set; }
    public int RunwaysCount { get; set; }
    public DateOnly Founded { get; set; }

    public AirportDTO(int id, string name, string country, string city, string code, int runwaysCount, DateOnly founded)
    {
        Id = id;
        Name = name;
        Country = country;
        City = city;
        Code = code;
        RunwaysCount = runwaysCount;
        Founded = founded;
    }
}
