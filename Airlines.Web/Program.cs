using AirlinesBusinessServices = AirlinesBusinessWeb.Services;
using AirlinesBusinessProfiles = AirlinesBusinessWeb.Profiles;

using AirlinesWebServices.Profiles;//AirlinesWebServicesProfiles = AirlinesWebServices.Profiles;
using AirlinesWebServices.Services;
//AirlinesWebServicesServices = AirlinesWebServices.Services;

using AirlinesPersistenceBasic.Repositories;
using AirlinesPersistenceBasic.Repositories.Interfaces;
using AirlinesPersistenceBasic.DBContext;


namespace Airlines.Web;
public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.
        builder.Services.AddControllersWithViews();

        // Repositories
        builder.Services.AddScoped<IAirportRepository, AirportRepository>();
        builder.Services.AddScoped<IAirlineRepository, AirlineRepository>();
        builder.Services.AddScoped<IFlightRepository, FlightRepository>();

        // Airlines.Business.Web mappers
        builder.Services.AddSingleton<AirlinesBusinessProfiles.AirportMapper>();
        builder.Services.AddSingleton<AirlinesBusinessProfiles.AirlineMapper>();
        builder.Services.AddSingleton<AirlinesBusinessProfiles.FlightMapper>();

        // Airlines.Business.Web services
        builder.Services.AddScoped<AirlinesBusinessServices.IAirportService, AirlinesBusinessServices.AirportService>();
        builder.Services.AddScoped<AirlinesBusinessServices.IAirlineService, AirlinesBusinessServices.AirlineService>();
        builder.Services.AddScoped<AirlinesBusinessServices.IFlightService, AirlinesBusinessServices.FlightService>();

        // Airlines.Web.Services mappers
        builder.Services.AddSingleton<AirlineMapper>();
        builder.Services.AddSingleton<AirportMapper>();
        builder.Services.AddSingleton<FlightMapper>();

        // Airlines.Web.Services services
        builder.Services.AddScoped<IAirlineService, AirlineService>();
        builder.Services.AddScoped<IAirportService, AirportService>();
        builder.Services.AddScoped<IFlightService, FlightService>();

        builder.Services.AddAutoMapper(typeof(Program).Assembly);
        builder.Services.AddDbContext<AirlinesDBContext>();
        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (!app.Environment.IsDevelopment())
        {
            app.UseExceptionHandler("/Home/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseAuthorization();

        app.MapControllerRoute(
            name: "default",
            pattern: "{controller=Home}/{action=Index}/{id?}");

        app.Run();
    }
}
