﻿using Microsoft.AspNetCore.Mvc;

using AirlinesWebServices.Models;
using AirlinesWebServices.Services;

namespace AirlinesWebServices.Controllers;
public class AirlineController : Controller
{
    private readonly IAirlineService _airlineService;
    public AirlineController(IAirlineService airlineService)
    {
        _airlineService = airlineService;
    }
    // GET: AirlineController
    public ActionResult Index(string searchTerm, string selectedOption)
    {
        try
        {
            if (!string.IsNullOrEmpty(searchTerm) && !string.IsNullOrEmpty(selectedOption))
            {
                List<Airline> airlines = _airlineService.GetAirlinesByFilter(searchTerm, selectedOption);
                return View(airlines);
            }
            else
            {
                List<Airline> airlines = _airlineService.GetAirlines();
                return View(airlines);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return View(new List<Airline>());
        }
    }

    [HttpPost]
    public ActionResult Index(Airline airline)
    {
        try
        {
            _airlineService.AddAirline(airline);
            return RedirectToAction(nameof(Index));
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return View(nameof(Index));
        }
    }

    public ActionResult Search()
    {
        return PartialView();
    }
}
