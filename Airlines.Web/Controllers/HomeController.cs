using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

using AirlinesPersistenceBasic.Repositories.Interfaces;
using Airlines.Web.Models;

namespace AirlinesWebServices.Controllers;
public class HomeController : Controller
{
    private readonly IAirportRepository _airportRepository;
    private readonly IAirlineRepository _airlineRepository;
    private readonly IFlightRepository _flightRepository;
#pragma warning disable IDE0052 // Remove unread private members
    private readonly ILogger<HomeController> _logger;
#pragma warning restore IDE0052 // Remove unread private members

    public HomeController(ILogger<HomeController> logger, IAirportRepository airportRepository, IAirlineRepository airlineRepository, IFlightRepository flightRepository)
    {
        _logger = logger;
        _airportRepository = airportRepository;
        _airlineRepository = airlineRepository;
        _flightRepository = flightRepository;
    }

    public IActionResult Index()
    {
        try
        {
            int airportsCount = _airportRepository.GetAirports().Count;
            int airlinesCount = _airlineRepository.GetAirlines().Count;
            int flightsCount = _flightRepository.GetFlights().Count;
            IndexViewModel indexViewModel = new IndexViewModel
            {
                AirlinesCount = airlinesCount,
                AirportsCount = airportsCount,
                FlightsCount = flightsCount
            };
            return View(indexViewModel);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            IndexViewModel indexViewModel = new IndexViewModel
            {
                AirlinesCount = 0,
                AirportsCount = 0,
                FlightsCount = 0
            };
            return View(indexViewModel);
        }

    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
