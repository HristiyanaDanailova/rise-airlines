﻿using Microsoft.AspNetCore.Mvc;

using AirlinesWebServices.Services;
using Airport = AirlinesWebServices.Models.Airport;

namespace AirlinesWebServices.Controllers;
public class AirportController : Controller
{
    private readonly IAirportService _airportService;
    public AirportController(IAirportService airportService)
    {
        _airportService = airportService;
    }

    // GET: AirportController
    public ActionResult Index(string searchTerm, string selectedOption)
    {
        try
        {
            if (!string.IsNullOrEmpty(searchTerm) && !string.IsNullOrEmpty(selectedOption))
            {
                List<Airport> airports = _airportService.GetAirportsByFilter(searchTerm, selectedOption);
                return View(airports);
            }
            else
            {
                List<Airport> airports = _airportService.GetAirports();
                return View(airports);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return View(new List<Airport>());
        }
    }

    [HttpPost]
    public ActionResult Index(Airport airport)
    {
        try
        {
            _airportService.AddAirport(airport);
            return RedirectToAction(nameof(Index));
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return View(nameof(Index));
        }
    }

    public ActionResult Search()
    {
        return PartialView();
    }
}
