﻿using Microsoft.AspNetCore.Mvc;

using AirlinesWebServices.Models;
using AirlinesWebServices.Services;

namespace AirlinesWebServices.Controllers;
public class FlightController : Controller
{
    private readonly IFlightService _flightService;
    public FlightController(IFlightService flightService)
    {
        _flightService = flightService;
    }
    // GET: FlightController
    public ActionResult Index(string searchTerm, string selectedOption)
    {
        try
        {
            if (!string.IsNullOrEmpty(searchTerm) && !string.IsNullOrEmpty(selectedOption))
            {
                List<Flight> flights = _flightService.GetFlightsByFilter(searchTerm, selectedOption);
                return View(flights);
            }
            else
            {
                List<Flight> flights = _flightService.GetFlights();
                return View(flights);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return View(new List<Flight>());
        }
    }

    [HttpPost]
    public ActionResult Index(Flight flight)
    {
        try
        {
            _flightService.AddFlight(flight);
            return RedirectToAction(nameof(Index));
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return View(nameof(Index));
        }
    }

    public ActionResult Search()
    {
        return PartialView();
    }
}
