﻿namespace Airlines.Web.Models;

public class IndexViewModel
{
    public int AirlinesCount { get; set; }
    public int AirportsCount { get; set; }
    public int FlightsCount { get; set; }

}
