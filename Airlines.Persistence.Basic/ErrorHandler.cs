﻿
namespace AirlinesPersistenceBasic;
public class ErrorHandler
{
#pragma warning disable CA1822 // Mark members as static
    public void HandleException(Exception ex)
#pragma warning restore CA1822 // Mark members as static
    {
        Console.WriteLine($"An error occurred: {ex.Message}");
    }
}
