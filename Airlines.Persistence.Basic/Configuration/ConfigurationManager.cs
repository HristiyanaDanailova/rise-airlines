﻿using Microsoft.Extensions.Configuration;

namespace AirlinesPersistenceBasic.Configuration;
public class ConfigurationManager
{
    private static IConfigurationRoot _configuration;

    static ConfigurationManager()
    {
        string basePath = Path.Combine(AppContext.BaseDirectory, "..", "..", "..", "..", "Airlines.Persistence.Basic");

        // Build configuration once
        _configuration = new ConfigurationBuilder()
            .SetBasePath(basePath)
            .AddJsonFile("appSettings.json")
            .Build();
    }

    public static string? GetConnectionString(string name)
    {
        // Get connection string from configuration
        return _configuration.GetConnectionString(name);
    }
}
