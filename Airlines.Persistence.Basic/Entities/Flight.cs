﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace AirlinesPersistenceBasic.Entities;

[Table("Flight")]
[Index("ArrivalAirportId", Name = "IX_Flight_ArrivalAirportId")]
[Index("DepartureAirportId", Name = "IX_Flight_DeparuteAirportId")]
[Index("FlightNumber", Name = "IX_Flight_FlightNumber")]
public partial class Flight
{
    [Key]
    public int Id { get; set; }

    [Required]
    [StringLength(5)]
    [Unicode(false)]
    public string FlightNumber { get; set; }

    public int AirlineId { get; set; }

    public int DepartureAirportId { get; set; }

    public int ArrivalAirportId { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime DepartureDateTime { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime ArrivalDateTime { get; set; }

    [ForeignKey("ArrivalAirportId")]
    [InverseProperty("FlightArrivalAirports")]
    public virtual Airport ArrivalAirport { get; set; }

    [ForeignKey("DepartureAirportId")]
    [InverseProperty("FlightDepartureAirports")]
    public virtual Airport DepartureAirport { get; set; }
}