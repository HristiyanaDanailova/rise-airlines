﻿using AirlinesPersistenceBasic.DBContext;
using AirlinesPersistenceBasic.Entities;
using AirlinesPersistenceBasic.Repositories.Interfaces;
using System.Runtime.CompilerServices;

namespace AirlinesPersistenceBasic.Repositories;
public class AirportRepository : IAirportRepository, IDisposable
{
    private readonly AirlinesDBContext _context;
    private readonly ErrorHandler _errorHandler;
    public AirportRepository(AirlinesDBContext context)
    {
        _errorHandler = new ErrorHandler();
        _context = context;
    }

    public void Dispose() { }

    // Select by column
    public Airport? GetAirportById(int id)
    {
        try
        {
            Airport? result = _context.Airports.FirstOrDefault(airport => airport.Id == id);
            return result;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
    }

    public Airport? GetAirportByCode(string code)
    {
        try
        {
            Airport? airport = _context.Airports.FirstOrDefault(airport => airport.Code == code);
            return airport;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
    }

    // Select airports based on a criteria
    public List<Airport> GetAirportsByCity(string city)
    {
        try
        {
            IQueryable<Airport> result = _context.Airports.Where(airport => airport.City == city);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airport>();
        }
    }

    public List<Airport> GetAirportsByCountry(string country)
    {
        try
        {
            IQueryable<Airport> result = _context.Airports.Where(airport => airport.Country == country);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airport>();
        }
    }

    // Name is not unique ( airports are differentiated by code and id )
    public List<Airport> GetAirportsByName(string name)
    {
        try
        {
            IQueryable<Airport> result = _context.Airports.Where(airport => airport.Name == name);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airport>();
        }

    }

    // Sort airports by runways count or founded date
    public List<Airport> SortAirportsByRunwaysCountDesc()
    {
        try
        {
            IQueryable<Airport> result = _context.Airports.OrderByDescending(airport => airport.RunwaysCount);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airport>();
        }
    }

    public List<Airport> SortAirportsByRunwaysCountAsc()
    {
        try
        {
            IQueryable<Airport> result = _context.Airports.OrderBy(airport => airport.RunwaysCount);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airport>();
        }

    }

    public List<Airport> SortAirportsByFoundedDateDesc()
    {
        try
        {
            IQueryable<Airport> result = _context.Airports.OrderByDescending(airport => airport.Founded);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airport>();
        }
    }

    public List<Airport> SortAirportsByFoundedDateAsc()
    {
        try
        {
            IQueryable<Airport> result = _context.Airports.OrderBy(airport => airport.Founded);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airport>();
        }
    }

    // Basic CRUD operations
    public List<Airport> GetAirports()
    {
        try
        {
            return _context.Airports.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airport>();
        }
    }

    public bool AddAirport(Airport airport)
    {
        try
        {
            _context.Airports.Add(airport);
            _context.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            _errorHandler.HandleException(ex);
            return false;
        }

    }

    public bool UpdateAirport(Airport airport)
    {
        try
        {
            _context.Airports.Update(airport);
            _context.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            _errorHandler.HandleException(ex);
            return false;
        }
    }

    public bool DeleteAirport(int id)
    {
        try
        {
            Airport? airportToDelete = _context.Airports.FirstOrDefault(airport => airport.Id == id);
            if (airportToDelete != null)
            {
                _context.Airports.Remove(airportToDelete);
                _context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            _errorHandler.HandleException(ex);
            return false;
        }
    }


    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1862:Use the 'StringComparison' method overloads to perform case-insensitive string comparisons", Justification = "<Pending>")]
    public List<Airport> GetAirportsByFilter(string value, string objectProperty)
    {
        if (objectProperty == "Name")
        {
            return _context.Airports.Where(airport => airport.Name.ToLower().Contains(value.ToLower())).ToList();
        }
        else if (objectProperty == "Country")
        {
            return _context.Airports.Where(airport => airport.Country.ToLower().Contains(value.ToLower())).ToList();
        }
        else if (objectProperty == "City")
        {
            return _context.Airports.Where(airport => airport.City.ToLower().Contains(value.ToLower())).ToList();
        }
        else if (objectProperty == "Code")
        {
            return _context.Airports.Where(airport => airport.Code.ToLower().Contains(value.ToLower())).ToList();
        }
        else if (objectProperty == "RunwaysCount")
        {
            return _context.Airports.Where(airport => airport.RunwaysCount.ToString() == value).ToList();
        }
        else if (objectProperty == "Founded")
        {
            if (DateOnly.TryParse(value, out DateOnly parsedDate))
            {

                return _context.Airports.Where(airport => airport.Founded == parsedDate).ToList();
            }
            return new List<Airport>();
        }
        else
        {
            return new List<Airport>();
        }
    }
}
