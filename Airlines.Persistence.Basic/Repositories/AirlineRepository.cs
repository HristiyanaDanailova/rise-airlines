﻿using AirlinesPersistenceBasic.DBContext;
using AirlinesPersistenceBasic.Entities;
using AirlinesPersistenceBasic.Repositories.Interfaces;

namespace AirlinesPersistenceBasic.Repositories;
public class AirlineRepository : IAirlineRepository, IDisposable
{
    private readonly AirlinesDBContext _context;
    private readonly ErrorHandler _errorHandler;
    public AirlineRepository(AirlinesDBContext context)
    {
        _errorHandler = new ErrorHandler();
        _context = context;

    }
    public void Dispose() { }

    // Select by column
    public Airline? GetAirlineById(int id)
    {
        try
        {
            Airline? result = _context.Airlines.FirstOrDefault(airline => airline.Id == id);
            return result;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
    }

    // Select by a certain criteria
    // Name is not unique ( airlines are differentiated by id )
    public List<Airline> GetAirlinesByName(string name)
    {
        try
        {
            IQueryable<Airline> result = _context.Airlines.Where(airline => airline.Name == name);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airline>();
        }
    }

    // Sort airlines by fleetSize and founded date
    public List<Airline> SortAirlinesByFleetSizeDesc()
    {
        try
        {
            IQueryable<Airline> result = _context.Airlines.OrderByDescending(airline => airline.FleetSize);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
            return new List<Airline>();
        }
    }

    public List<Airline> SortAirlinesByFleetSizeAsc()
    {
        try
        {
            IQueryable<Airline> result = _context.Airlines.OrderBy(airline => airline.FleetSize);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airline>();
        }
    }

    public List<Airline> SortAirlinesByFoundedDateDesc()
    {
        try
        {
            IQueryable<Airline> result = _context.Airlines.OrderByDescending(airline => airline.Founded);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airline>();
        }
    }

    public List<Airline> SortAirlinesByFoundedDateAsc()
    {
        try
        {
            IQueryable<Airline> result = _context.Airlines.OrderBy(airline => airline.Founded);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airline>();
        }
    }

    // Basic CRUD operations
    public List<Airline> GetAirlines()
    {
        try
        {
            return _context.Airlines.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Airline>();
        }
    }

    public bool AddAirline(Airline airline)
    {
        try
        {
            _context.Airlines.Add(airline);
            _context.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            _errorHandler.HandleException(ex);
            return false;
        }
    }

    public bool UpdateAirline(Airline airline)
    {
        try
        {
            _context.Airlines.Update(airline);
            _context.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            _errorHandler.HandleException(ex);
            return false;
        }
    }

    public bool DeleteAirline(int id)
    {
        try
        {
            Airline? airlineToDelete = _context.Airlines.FirstOrDefault(airline => airline.Id == id);
            if (airlineToDelete != null)
            {
                _context.Airlines.Remove(airlineToDelete);
                _context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            _errorHandler.HandleException(ex);
            return false;
        }

    }


    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1862:Use the 'StringComparison' method overloads to perform case-insensitive string comparisons", Justification = "<Pending>")]
    public List<Airline> GetAirlinesByFilter(string value, string objectProperty)
    {
        if (objectProperty == "Name")
        {
            return _context.Airlines.Where(airline => airline.Name.ToLower().Contains(value.ToLower())).ToList();
        }
        else if (objectProperty == "Founded")
        {
            if (DateOnly.TryParse(value, out DateOnly parsedDate))
            {
                return _context.Airlines.Where(airline => airline.Founded == parsedDate).ToList();
            }
            return new List<Airline>();
        }
        else if (objectProperty == "Description")
        {
            return _context.Airlines.Where(airline => airline.Description.ToLower().Contains(value.ToLower())).ToList();
        }
        else if (objectProperty == "FleetSize")
        {
            return _context.Airlines.Where(airline => airline.FleetSize.ToString() == value).ToList();
        }
        else
        {
            return new List<Airline>();
        }
    }
}
