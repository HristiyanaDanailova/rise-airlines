﻿using AirlinesPersistenceBasic.DBContext;
using AirlinesPersistenceBasic.Entities;
using AirlinesPersistenceBasic.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;

namespace AirlinesPersistenceBasic.Repositories;
public class FlightRepository : IFlightRepository, IDisposable
{
    private readonly AirlinesDBContext _context;
    private readonly ErrorHandler _errorHandler;
    public FlightRepository(AirlinesDBContext context)
    {
        _errorHandler = new ErrorHandler();
        _context = context;
    }
    public void Dispose() { }

    // Get by column
    public Flight? GetFlightById(int id)
    {
        try
        {
            Flight? result = _context.Flights.FirstOrDefault(flight => flight.Id == id);
            return result;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
    }

    public Flight? GetFlightByFlightNumber(string flightNumber)
    {
        try
        {
            Flight? result = _context.Flights.FirstOrDefault(flight => flight.FlightNumber == flightNumber);
            return result;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
    }

    // Farthest flight based on departure and arrival time
    public Flight? GetFarthestFlight()
    {
        try
        {
            IQueryable<Flight> result = _context.Flights.OrderByDescending(flight => (flight.ArrivalDateTime - flight.DepartureDateTime));
            return result.FirstOrDefault();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
    }

    // Select flights based on a criteria
    public List<Flight> GetFlightsFromDepartureAirportById(int departureAirportId)
    {
        try
        {
            IQueryable<Flight> result = _context.Flights.Where(flight => flight.DepartureAirportId == departureAirportId);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Flight>();
        }
    }

    public List<Flight> GetFlightsToArrivalAirportById(int arrivalAirportId)
    {
        try
        {
            IQueryable<Flight> result = _context.Flights.Where(flight => flight.ArrivalAirportId == arrivalAirportId);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Flight>();
        }
    }

    public List<Flight> GetFlightsByAirlineById(int id)
    {
        try
        {
            IQueryable<Flight> result = _context.Flights.Where(flight => flight.AirlineId == id);
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Flight>();
        }
    }
    public List<Flight> GetFlightsFromDepartureAirportByName(string departureAirportName)
    {
        try
        {
            int? departureAirportId = _context.Airports.Where(airport => airport.Name == departureAirportName).FirstOrDefault()?.Id;
            if (departureAirportId != null)
            {
                IQueryable<Flight> result = _context.Flights.Where(flight => flight.DepartureAirportId == departureAirportId);
                return result.ToList();
            }
            return new List<Flight>();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Flight>();
        }
    }

    public List<Flight> GetFlightsToArrivalAirportByName(string arrivalAirportName)
    {
        try
        {
            int? arrivalAirportId = _context.Airports.Where(airport => airport.Name == arrivalAirportName).FirstOrDefault()?.Id;
            if (arrivalAirportId != null)
            {

                IQueryable<Flight> result = _context.Flights.Where(flight => flight.ArrivalAirportId == arrivalAirportId);
                return result.ToList();
            }
            return new List<Flight>();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Flight>();
        }
    }

    public List<Flight> GetFlightsByAirlineByName(string airlineName)
    {
        try
        {
            int? airlineId = _context.Airlines.Where(airline => airline.Name == airlineName).FirstOrDefault()?.Id;
            if (airlineId != null)
            {
                IQueryable<Flight> result = _context.Flights.Where(flight => flight.AirlineId == airlineId);
                return result.ToList();
            }
            return new List<Flight>();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Flight>();
        }
    }

    public List<Flight> GetFlightsByDepartureAirportCode(string airportCode)
    {
        try
        {
            IQueryable<Flight> result = from flight in _context.Flights
                                        join airport in _context.Airports
                                        on flight.DepartureAirportId equals airport.Id
                                        where airport.Code == airportCode
                                        select flight;
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Flight>();
        }
    }

    public List<Flight> GetFlightsByArrivalAirportCode(string airportCode)
    {
        try
        {
            IQueryable<Flight> result = from flight in _context.Flights
                                        join airport in _context.Airports
                                        on flight.ArrivalAirportId equals airport.Id
                                        where airport.Code == airportCode
                                        select flight;
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Flight>();
        }
    }

    // Sort flights by flight duration, based on departure and arrival time
    public List<Flight> SortFlightsByFlightDurationDesc()
    {
        try
        {
            IQueryable<Flight> result = _context.Flights.OrderByDescending(flight => (flight.ArrivalDateTime - flight.DepartureDateTime));
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Flight>();
        }
    }

    public List<Flight> SortFlightsByFlightDurationAsc()
    {
        try
        {
            IQueryable<Flight> result = _context.Flights.OrderBy(flight => (flight.ArrivalDateTime - flight.DepartureDateTime));
            return result.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Flight>();
        }
    }

    // Basic CRUD operations
    public List<Flight> GetFlights()
    {
        try
        {
            return _context.Flights.ToList();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new List<Flight>();
        }
    }

    public bool AddFlight(Flight flight)
    {
        try
        {
            _context.Flights.Add(flight);
            _context.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            _errorHandler.HandleException(ex);
            return false;
        }
    }

    public bool UpdateFlight(Flight flight)
    {
        try
        {
            _context.Flights.Update(flight);
            _context.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            _errorHandler.HandleException(ex);
            return false;
        }
    }

    public bool DeleteFlight(int id)
    {
        try
        {
            Flight? flightToDelete = _context.Flights.FirstOrDefault(f => f.Id == id);
            if (flightToDelete != null)
            {
                _context.Flights.Remove(flightToDelete);
                _context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            _errorHandler.HandleException(ex);
            return false;
        }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1862:Use the 'StringComparison' method overloads to perform case-insensitive string comparisons", Justification = "<Pending>")]
    public List<Flight> GetFlightsByFilter(string value, string objectProperty)
    {
        if (objectProperty == "FlightNumber")
        {
            return _context.Flights.Where(flight => flight.FlightNumber.ToLower().Contains(value.ToLower())).ToList();
        }
        else if (objectProperty == "AirlineName")
        {
            List<int> airlineIds = _context.Airlines.Where(airline => airline.Name.ToLower().Contains(value.ToLower())).Select(airline => airline.Id).ToList();
            if (airlineIds.Count > 0)
            {
                return _context.Flights.Where(flight => airlineIds.Contains(flight.AirlineId)).ToList();
            }
            else
            {
                return new List<Flight>();
            }
        }
        else if (objectProperty == "DepartureAirportName")
        {
            List<int> airportIds = _context.Airports.Where(airport => airport.Name.ToLower().Contains(value.ToLower())).Select(airport => airport.Id).ToList();
            if (airportIds.Count > 0)
            {
                return _context.Flights.Where(flight => airportIds.Contains(flight.DepartureAirportId)).ToList();
            }
            else
            {
                return new List<Flight>();
            }
        }
        else if (objectProperty == "ArrivalAirportName")
        {
            List<int> airportIds = _context.Airports.Where(airport => airport.Name.ToLower().Contains(value.ToLower())).Select(airport => airport.Id).ToList();
            if (airportIds.Count > 0)
            {
                return _context.Flights.Where(flight => airportIds.Contains(flight.ArrivalAirportId)).ToList();
            }
            else
            {
                return new List<Flight>();
            }
        }
        else if (objectProperty == "DepartureDateTime")
        {
            if (DateTime.TryParse(value, out DateTime parsedDateTime))
            {
                return _context.Flights.Where(flight => flight.DepartureDateTime == parsedDateTime).ToList();
            }
            return new List<Flight>();
        }
        else if (objectProperty == "ArrivalDateTime")
        {
            if (DateTime.TryParse(value, out DateTime parsedDateTime))
            {
                return _context.Flights.Where(flight => flight.ArrivalDateTime == parsedDateTime).ToList();
            }
            return new List<Flight>();
        }
        else
        {
            return new List<Flight>();
        }
    }


}
