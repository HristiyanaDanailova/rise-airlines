﻿
using AirlinesPersistenceBasic.Entities;

namespace AirlinesPersistenceBasic.Repositories.Interfaces;
public interface IAirlineRepository
{
    // Select by column
    Airline? GetAirlineById(int id);

    // Select by a certain criteria
    List<Airline> GetAirlinesByName(string name);

    // Sort airlines by fleetSize and founded date
    List<Airline> SortAirlinesByFleetSizeDesc();
    List<Airline> SortAirlinesByFleetSizeAsc();
    List<Airline> SortAirlinesByFoundedDateDesc();
    List<Airline> SortAirlinesByFoundedDateAsc();

    // Basic CRUD operations
    List<Airline> GetAirlines();
    bool AddAirline(Airline airline);
    bool UpdateAirline(Airline airline);
    bool DeleteAirline(int id);

    List<Airline> GetAirlinesByFilter(string value, string objectProperty);
}
