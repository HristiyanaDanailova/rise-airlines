﻿
using AirlinesPersistenceBasic.Entities;

namespace AirlinesPersistenceBasic.Repositories.Interfaces;
public interface IAirportRepository
{
    // Get by column
    Airport? GetAirportById(int id);
    Airport? GetAirportByCode(string code);

    // Select airports based on a criteria
    List<Airport> GetAirportsByCity(string city);
    List<Airport> GetAirportsByCountry(string country);
    List<Airport> GetAirportsByName(string name);

    // Sort airports by runways count or founded date
    List<Airport> SortAirportsByRunwaysCountDesc();
    List<Airport> SortAirportsByRunwaysCountAsc();
    List<Airport> SortAirportsByFoundedDateDesc();
    List<Airport> SortAirportsByFoundedDateAsc();

    // Basic CRUD operations
    List<Airport> GetAirports();
    bool AddAirport(Airport airport);
    bool UpdateAirport(Airport airport);
    bool DeleteAirport(int id);

    List<Airport> GetAirportsByFilter(string value, string objectProperty);

}
