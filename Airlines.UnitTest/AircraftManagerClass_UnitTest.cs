﻿using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Managers;

namespace AirlinesUnitTest.AircraftManagerNS;
public class AircraftManagerClassUnitTest
{
    
    // Valid LoadAircrafts method test
    [Fact]
    public void AircraftsManagerClass_LoadAircrafts_Valid()
    {
        AircraftsManager aircraftManager = new AircraftsManager();
        aircraftManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        Assert.Equal(5, aircraftManager.Aircrafts.Count);
        Assert.True(aircraftManager.HasAircraft("Boeing 737-800"));
        Assert.True(aircraftManager.HasAircraft("Airbus A320"));
        Assert.True(aircraftManager.HasAircraft("Gulfstream G650"));
        Assert.True(aircraftManager.HasAircraft("Cessna Citation XLS+"));
        Assert.True(aircraftManager.HasAircraft("Embraer E190"));

        Assert.False(aircraftManager.HasAircraft("Gulfstream G"));
        Assert.False(aircraftManager.HasAircraft("Name"));
        Assert.False(aircraftManager.HasAircraft(""));
        Assert.False(aircraftManager.HasAircraft("-"));
    }
    
    // Invalid LoadAircrafts method test ( invalid file data )
    [Fact]
    public void AircraftsManagerClass_LoadAircrafts_Invalid_InvalidFileData()
    {
        string filename = Constants.Constants.INVALID_AIRCRAFTS_FILENAME;
        AircraftsManager aircraftsManager = new AircraftsManager();
        InvalidFileDataException exception = Assert.Throws<InvalidFileDataException>(() => aircraftsManager.LoadAircrafts(filename));
        Assert.Equal($"Invalid file data in {filename}! File fields should be of format: \"<aircraft model>,<load>,<volume>,<seats>\"", exception.Message);
    }

    // Invalid LoadAircrafts method test ( nonexistent file )
    [Fact]
    public void AircraftsManagerClass_LoadAircrafts_Invalid_NonexistentFile()
    {
        string fileName = Constants.Constants.NONEXISTENT_FILENAME;
        string filePath = $"{Constants.Constants.PATH_TO_FILE}/{fileName}";
        AircraftsManager aircraftsManager = new AircraftsManager();
        FileNotFoundException exception = Assert.Throws<FileNotFoundException>(() => aircraftsManager.LoadAircrafts(fileName));
        Assert.Equal("File not found: " + filePath, exception.Message);
    }

    // Valid AircraftType method test
    [Theory]
    [InlineData("Model1","23","11.1","3","passenger")]
    [InlineData("Model2","2","30.1","-","cargo")]
    [InlineData("Model1","-","-","3","private")]
    public void AircraftsManagerClass_AircratType_Valid(string model,string weight,string volume,string seats,string expectedType)
    {
        string type = AircraftsManager.GetAircraftType(model, weight, volume, seats);
        Assert.Equal(expectedType, type);
    }

    // Invalid AircraftType method test
    [Theory]
    [InlineData("-", "23", "11.1", "3")]
    [InlineData("Model2", "2", "-", "-")]
    [InlineData("Model1", "-", "-", "-")]
    [InlineData("", "", "", "")]
    [InlineData("", "", "", null)]
    [InlineData(null,null,null, null)]
    [InlineData("", "", "///", "")]
    [InlineData("", "dgs", "", "")]
    public void AircraftsManagerClass_AircratType_Invalid(string model, string weight, string volume, string seats)
    {
        string type = AircraftsManager.GetAircraftType(model, weight, volume, seats);
        Assert.Equal("invalid", type);
    }
}
