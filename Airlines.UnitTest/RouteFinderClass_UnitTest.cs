﻿using AirlinesBusiness.DataStructures;
using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Managers;
using AirlinesBusiness.Models;

using AirlinesUnitTest.Constants;
namespace Airlines.RouteFinderNS;
public class RouteFinderClassUnitTest
{
    // Valid LoadRoutes method test
    [Fact]
    public void RouteFinderClass_LoadRoutes_Valid()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        RouteFinder routeFinder = new RouteFinder();
        routeFinder.LoadRoutes(Constants.VALID_ROUTES_FILENAME, flightsManager, airportsManager);

        Assert.Equal("JFK", routeFinder.Routes.Root.Data!.Id);
        Assert.Equal(2, routeFinder.Routes.Root.Children!.Count);
    }

    // Invalid LoadRoutes method test ( invalid starting airport )
    [Fact]
    public void RouteFinderClass_LoadRoutes_Invalid_InvalidStartingAirport()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        RouteFinder routeFinder = new RouteFinder();
        EntityNotFoundException exception = Assert.Throws<EntityNotFoundException>(() => routeFinder.LoadRoutes(Constants.INVALID_STARTING_POINT_ROUTES_FILENAME, flightsManager, airportsManager));

        Assert.Equal("Starting airport not found!", exception.Message);

    }

    // Invalid LoadRoutes method test ( invalid flight id )
    [Fact]
    public void RouteFinderClass_LoadRoutes_Invalid_InvalidFlightId()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        RouteFinder routeFinder = new RouteFinder();
        EntityNotFoundException exception = Assert.Throws<EntityNotFoundException>(() => routeFinder.LoadRoutes(Constants.INVALID_FLIGHT_ID_ROUTES_FILENAME, flightsManager, airportsManager));

        Assert.Equal("No flight with such id was found!", exception.Message);

    }

    // Invalid LoadRoutes method test ( nonexistent file )
    [Fact]
    public void RouteFinderClass_LoadRoutes_Invalid_NonexistentFile()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        string fileName = Constants.NONEXISTENT_FILENAME;
        string filePath = $"{Constants.PATH_TO_FILE}/{fileName}";
        RouteFinder routeFinder = new RouteFinder();
        FileNotFoundException exception = Assert.Throws<FileNotFoundException>(() => routeFinder.LoadRoutes(fileName, flightsManager, airportsManager));

        Assert.Equal($"File not found: {filePath}", exception.Message);

    }

    // ConnectRoutes method test
    [Fact]
    public void RouteFinderClass_ConnectRoutes_Valid()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        RouteFinder routeFinder = new RouteFinder();
        routeFinder.LoadRoutes(Constants.VALID_ROUTES_FILENAME, flightsManager, airportsManager);
        routeFinder.ConnectRoutes(flightsManager,airportsManager,routeFinder.Routes.Root);

        Airport? airport1 = airportsManager.GetAirportById("LAX");
        Assert.NotNull(airport1);
        List<TreeNode<Airport>> foundRoute1 =  routeFinder.FindFlightRoute(airport1);
        Assert.Equal(foundRoute1.ConvertAll(node => node.Data!.Id), new List<string> { "JFK","LAX"});

        Airport? airport2 = airportsManager.GetAirportById("ORD");
        Assert.NotNull(airport2);
        List<TreeNode<Airport>> foundRoute2 = routeFinder.FindFlightRoute(airport2);
        Assert.Equal(foundRoute2.ConvertAll(node => node.Data!.Id), new List<string> { "JFK", "LAX","ORD" });

        Airport? airport3 = airportsManager.GetAirportById("MIA");
        Assert.NotNull(airport3);
        List<TreeNode<Airport>> foundRoute3 = routeFinder.FindFlightRoute(airport3);
        Assert.Equal(foundRoute3.ConvertAll(node => node.Data!.Id), new List<string> { "JFK", "SFO", "MIA" });

    }
}
