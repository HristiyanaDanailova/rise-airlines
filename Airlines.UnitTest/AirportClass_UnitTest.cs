﻿using AirlinesBusiness.Models;

namespace AirlinesUnitTest.AirportClassNS;
public class AirportClassUnitTest
{
    // Class constructor test
    [Theory]
    [InlineData("Id1","Name1","City1", "Country1")]
    [InlineData("Id2","Name2","City2", "Country2")]
    [InlineData("Id3","Name3", "City3", "Country3")]
    [InlineData("Id4","Name4", "City4", "Country4")]
    public void AirportClass_Constructor_Valid(string id,string name,string city,string country)
    {
        Airport airport = new Airport(id, name, city,country);
        Assert.Equal(airport.Id, id);
        Assert.Equal(airport.Name, name);
        Assert.Equal(airport.Country, country);
        Assert.Equal(airport.City, city);
    }

}
