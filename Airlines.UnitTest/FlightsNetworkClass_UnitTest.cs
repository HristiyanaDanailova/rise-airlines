﻿using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Managers;
using AirlinesBusiness.Models;

namespace AirlinesUnitTest.FlightsNetworkNS;
public class FlightsNetworkClassUnitTest
{

    // Valid LoadFlightNetwork method test
    [Fact]
    public void FlightsNetworkClass_LoadFlightNetwork_Valid()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        FlightsNetwork flightsNetwork = new FlightsNetwork();

        flightsNetwork.LoadFlightNetwork(Constants.Constants.VALID_ROUTES_FILENAME, flightsManager, airportsManager);

        Airport? airport = airportsManager.GetAirportById("JFK");
        Assert.True(flightsNetwork.FlightNetwork.ContainsNode(airport!));

        Assert.Equal(flightsNetwork.FlightNetwork.GetNode(airport!)!.Neighbours.Select(neightbour => neightbour.NeighbourData.Id), ["LAX", "SFO"]);

    }

    // Invalid LoadFlightNetwork method test ( invalid starting point )
    [Fact]
    public void FlightsNetworkClass_LoadFlightNetwork_Invalid_InvalidStartingPoint()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        FlightsNetwork flightsNetwork = new FlightsNetwork();

        EntityNotFoundException ex = Assert.Throws<EntityNotFoundException>(() => flightsNetwork.LoadFlightNetwork(Constants.Constants.INVALID_STARTING_POINT_ROUTES_FILENAME, flightsManager, airportsManager));
        Assert.Equal("Starting airport not found!", ex.Message);

    }

    //  Invalid LoadFlightNetwork method test ( invalid flight id )
    [Fact]
    public void FlightsNetworkClass_LoadFlightNetwork_Invalid_InvalidFlightId()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        FlightsNetwork flightsNetwork = new FlightsNetwork();

        EntityNotFoundException ex = Assert.Throws<EntityNotFoundException>(() => flightsNetwork.LoadFlightNetwork(Constants.Constants.INVALID_FLIGHT_ID_ROUTES_FILENAME, flightsManager, airportsManager));
        Assert.Equal("No flight with such id was found!", ex.Message);

    }

    // Invalid LoadFlightNetwork method test ( nonexistent file )
    [Fact]
    public void FlightsNetworkClass_LoadFlightNetwork_Invalid_NonExistentFile()
    {
        string fileName = Constants.Constants.NONEXISTENT_FILENAME;
        string filePath = $"{Constants.Constants.PATH_TO_FILE}/{fileName}";
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        FlightsNetwork flightsNetwork = new FlightsNetwork();

        FileNotFoundException ex = Assert.Throws<FileNotFoundException>(() => flightsNetwork.LoadFlightNetwork(fileName, flightsManager, airportsManager));
        Assert.Equal($"File not found: {filePath}", ex.Message);

    }

    // Valid AreAirportsConnected method test
    [Theory]
    [InlineData("JFK", "LAX")]
    [InlineData("JFK", "ORD")]
    [InlineData("JFK", "ATL")]
    [InlineData("JFK", "SFO")]
    [InlineData("JFK", "MIA")]
    public void FlightsNetworkClass_AreAirportsConnected_Valid(string startAirportId, string endAirportId)
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        FlightsNetwork flightsNetwork = new FlightsNetwork();
        flightsNetwork.LoadFlightNetwork(Constants.Constants.VALID_ROUTES_FILENAME, flightsManager, airportsManager);
        flightsNetwork.ConnectFlightsNetwork(flightsManager, airportsManager);

        Airport? startAirport = airportsManager.GetAirportById(startAirportId);
        Airport? endAirport = airportsManager.GetAirportById(endAirportId);

        Assert.True(flightsNetwork.AreAirportsConnected(startAirport!, endAirport!));

    }

    // Invalid AreAirportsConnected method test
    [Theory]
    [InlineData("JFK", "LA")]
    [InlineData("JFK", "")]
    [InlineData("JFK", "A")]
    [InlineData("JFK", "SF")]
    [InlineData("MIA", "JFK")]
    public void FlightsNetworkClass_AreAirportsConnected_Invalid(string startAirportId, string endAirportId)
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        FlightsNetwork flightsNetwork = new FlightsNetwork();
        flightsNetwork.LoadFlightNetwork(Constants.Constants.VALID_ROUTES_FILENAME, flightsManager, airportsManager);
        flightsNetwork.ConnectFlightsNetwork(flightsManager, airportsManager);

        Airport? startAirport = airportsManager.GetAirportById(startAirportId);
        Airport? endAirport = airportsManager.GetAirportById(endAirportId);

        Assert.False(flightsNetwork.AreAirportsConnected(startAirport!, endAirport!));

    }

    // Valid FindRoute with strategy "stops" method test
    [Theory]
    [InlineData("JFK", "LAX", new string[] { "JFK", "LAX" })]
    [InlineData("JFK", "ORD", new string[] { "JFK", "LAX", "ORD" })]
    [InlineData("JFK", "ATL", new string[] { "JFK", "LAX", "ORD", "ATL" })]
    [InlineData("LAX", "ATL", new string[] { "LAX", "ORD", "ATL" })]
    [InlineData("JFK", "MIA", new string[] { "JFK", "SFO", "MIA" })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void FlightsNetworkClass_FindRoute_StopsStrategy_Valid(string startAirportId, string endAirportId, string[] expectedPath)
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        FlightsNetwork flightsNetwork = new FlightsNetwork();
        flightsNetwork.LoadFlightNetwork(Constants.Constants.VALID_ROUTES_FILENAME, flightsManager, airportsManager);
        flightsNetwork.ConnectFlightsNetwork(flightsManager, airportsManager);

        Airport? startAirport = airportsManager.GetAirportById(startAirportId);
        Airport? endAirport = airportsManager.GetAirportById(endAirportId);

        List<Airport> path = flightsNetwork.FindRoute(startAirport!, endAirport!,"stops");
        Assert.Equal(path.Select(airport => airport.Id).ToArray(), expectedPath);
    }

    // Invalid FindRoute with strategy "stops" method test
    [Theory]
    [InlineData("LAX", "JFK", new string[] { })]
    [InlineData("SFO", "ORD", new string[] { })]
    [InlineData("ATL", "JFK", new string[] { })]
    [InlineData("MIA", "JFK", new string[] { })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void FlightsNetworkClass_FindRoute_StopsStrategy_Invalid(string startAirportId, string endAirportId, string[] expectedPath)
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        FlightsNetwork flightsNetwork = new FlightsNetwork();
        flightsNetwork.LoadFlightNetwork(Constants.Constants.VALID_ROUTES_FILENAME, flightsManager, airportsManager);
        flightsNetwork.ConnectFlightsNetwork(flightsManager, airportsManager);

        Airport? startAirport = airportsManager.GetAirportById(startAirportId);
        Airport? endAirport = airportsManager.GetAirportById(endAirportId);

        List<Airport> path = flightsNetwork.FindRoute(startAirport!, endAirport!,"stops");
        Assert.Equal(path.Select(airport => airport.Id).ToArray(), expectedPath);

    }

    // Valid FindRoute with strategy "short" method test
    [Theory]
    [InlineData("JFK", "LAX", new string[] { "JFK", "LAX" })]
    [InlineData("JFK", "ORD", new string[] { "JFK", "LAX", "ORD" })]
    [InlineData("JFK", "ATL", new string[] { "JFK", "LAX", "ORD", "ATL" })]
    [InlineData("LAX", "ATL", new string[] { "LAX", "ORD", "ATL" })]
    [InlineData("JFK", "MIA", new string[] { "JFK", "SFO", "MIA" })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void FlightsNetworkClass_FindRoute_ShortStrategy_Valid(string startAirportId, string endAirportId, string[] expectedPath)
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        FlightsNetwork flightsNetwork = new FlightsNetwork();
        flightsNetwork.LoadFlightNetwork(Constants.Constants.VALID_ROUTES_FILENAME, flightsManager, airportsManager);
        flightsNetwork.ConnectFlightsNetwork(flightsManager, airportsManager);

        Airport? startAirport = airportsManager.GetAirportById(startAirportId);
        Airport? endAirport = airportsManager.GetAirportById(endAirportId);

        List<Airport> path = flightsNetwork.FindRoute(startAirport!, endAirport!, "short");
        Assert.Equal(path.Select(airport => airport.Id).ToArray(), expectedPath);
    }

    // Invalid FindRoute with strategy "short" method test
    [Theory]
    [InlineData("LAX", "JFK", new string[] { })]
    [InlineData("SFO", "ORD", new string[] { })]
    [InlineData("ATL", "JFK", new string[] { })]
    [InlineData("MIA", "JFK", new string[] { })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void FlightsNetworkClass_FindRoute_ShortStrategy_Invalid(string startAirportId, string endAirportId, string[] expectedPath)
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        FlightsNetwork flightsNetwork = new FlightsNetwork();
        flightsNetwork.LoadFlightNetwork(Constants.Constants.VALID_ROUTES_FILENAME, flightsManager, airportsManager);
        flightsNetwork.ConnectFlightsNetwork(flightsManager, airportsManager);

        Airport? startAirport = airportsManager.GetAirportById(startAirportId);
        Airport? endAirport = airportsManager.GetAirportById(endAirportId);

        List<Airport> path = flightsNetwork.FindRoute(startAirport!, endAirport!, "short");
        Assert.Equal(path.Select(airport => airport.Id).ToArray(), expectedPath);

    }

    // Valid FindRoute with strategy "cheap" method test
    [Theory]
    [InlineData("JFK", "LAX", new string[] { "JFK", "LAX" })]
    [InlineData("JFK", "ORD", new string[] { "JFK", "LAX", "ORD" })]
    [InlineData("JFK", "ATL", new string[] { "JFK", "LAX", "ORD", "ATL" })]
    [InlineData("LAX", "ATL", new string[] { "LAX", "ORD", "ATL" })]
    [InlineData("JFK", "MIA", new string[] { "JFK", "SFO", "MIA" })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void FlightsNetworkClass_FindRoute_CheapStrategy_Valid(string startAirportId, string endAirportId, string[] expectedPath)
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        FlightsNetwork flightsNetwork = new FlightsNetwork();
        flightsNetwork.LoadFlightNetwork(Constants.Constants.VALID_ROUTES_FILENAME, flightsManager, airportsManager);
        flightsNetwork.ConnectFlightsNetwork(flightsManager, airportsManager);

        Airport? startAirport = airportsManager.GetAirportById(startAirportId);
        Airport? endAirport = airportsManager.GetAirportById(endAirportId);

        List<Airport> path = flightsNetwork.FindRoute(startAirport!, endAirport!, "cheap");
        Assert.Equal(path.Select(airport => airport.Id).ToArray(), expectedPath);
    }

    // Invalid FindRoute with strategy "cheap" method test
    [Theory]
    [InlineData("LAX", "JFK", new string[] { })]
    [InlineData("SFO", "ORD", new string[] { })]
    [InlineData("ATL", "JFK", new string[] { })]
    [InlineData("MIA", "JFK", new string[] { })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void FlightsNetworkClass_FindRoute_CheapStrategy_Invalid(string startAirportId, string endAirportId, string[] expectedPath)
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        FlightsNetwork flightsNetwork = new FlightsNetwork();
        flightsNetwork.LoadFlightNetwork(Constants.Constants.VALID_ROUTES_FILENAME, flightsManager, airportsManager);
        flightsNetwork.ConnectFlightsNetwork(flightsManager, airportsManager);

        Airport? startAirport = airportsManager.GetAirportById(startAirportId);
        Airport? endAirport = airportsManager.GetAirportById(endAirportId);

        List<Airport> path = flightsNetwork.FindRoute(startAirport!, endAirport!, "cheap");
        Assert.Equal(path.Select(airport => airport.Id).ToArray(), expectedPath);

    }
}
