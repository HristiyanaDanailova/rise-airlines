﻿using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Managers;

namespace AirlinesUnitTest.AirportsManagerNS;
public class AirportsManagerClassUnitTest
{
    // AddAirport method test
    [Theory]
    [InlineData(new string[] { "id1", "airport1", "Paris", "France" }, new string[] { "id2", "airport2", "Nice", "France" }, new string[] { "id3", "airport2", "Athens", "Greece" })]
    [InlineData(new string[] { "id1", "airport1", "Paris", "France" }, new string[] { "id2", "airport2", "Istanbul", "Turkey" }, new string[] { "id3", "airport2", "Barcelona", "Spain" })]
    [InlineData(new string[] { "id1", "airport1", "Paris", "France" }, new string[] { "id2", "airport2", "Paris", "France" }, new string[] { "id3", "airport2", "Paris", "France" })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void AirportsManagerClass_AddAirport_Valid(string[] airport1, string[] airport2, string[] airport3)
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.AddAirport(airport1[0], airport1[1], airport1[2], airport1[3]);
        airportsManager.AddAirport(airport2[0], airport2[1], airport2[2], airport2[3]);
        airportsManager.AddAirport(airport3[0], airport3[1], airport3[2], airport3[3]);

        string[] countryArray = { airport1[3], airport2[3], airport3[3] };
        string[] cityArray = { airport1[2], airport2[2], airport3[2] };
        Assert.Equal(airportsManager.AirportsByCountry.Keys.Count, countryArray.Distinct().ToArray().Length);
        Assert.Equal(airportsManager.AirportsByCity.Keys.Count, cityArray.Distinct().ToArray().Length);

    }

    // Sort method test
    [Fact]
    public void AirportsManagerClass_SortAiprorts_Valid()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.AddAirport("id1", "name1", "Paris", "France");
        airportsManager.AddAirport("id2", "name2", "Paris", "France");
        airportsManager.AddAirport("id3", "name3", "Nice", "France");
        airportsManager.AddAirport("id4", "name4", "Melbourne", "Australia");
        airportsManager.AddAirport("id5", "name5", "Berlin", "Germany");


        string[] countriesSortedAscending = { "Australia", "France", "Germany" };
        string[] citiesSortedAscending = { "Berlin", "Melbourne", "Nice", "Paris" };
        airportsManager.SortAirports("ascending");
        Assert.Equal(airportsManager.AirportsByCountry.Keys, countriesSortedAscending);
        Assert.Equal(airportsManager.AirportsByCity.Keys, citiesSortedAscending);

        airportsManager.SortAirports("descending");
        string[] countriesSortedDescending = { "Germany", "France", "Australia" };
        string[] citiesSortedDescending = { "Paris", "Nice", "Melbourne", "Berlin" };
        Assert.Equal(airportsManager.AirportsByCountry.Keys, countriesSortedDescending);
        Assert.Equal(airportsManager.AirportsByCity.Keys, citiesSortedDescending);

        Assert.Equal(3, airportsManager.AirportsByCountry["France"].Count);
        Assert.Equal(2, airportsManager.AirportsByCity["Paris"]!.Count);

    }

    // ListAirportsByCity method test
    [Fact]
    public void AirportsManagerClass_ListAirportsByCity_Valid()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.AddAirport("id1", "name1", "Paris", "France");
        airportsManager.AddAirport("id2", "name2", "Paris", "France");
        airportsManager.AddAirport("id3", "name3", "Nice", "France");
        airportsManager.AddAirport("id4", "name4", "Melbourne", "Australia");
        airportsManager.AddAirport("id5", "name5", "Berlin", "Germany");

        Assert.Equal(2,airportsManager.ListAirportsByCity("Paris").Count);
        Assert.Single(airportsManager.ListAirportsByCity("Melbourne"));
        Assert.Single(airportsManager.ListAirportsByCity("Nice"));
        Assert.Single(airportsManager.ListAirportsByCity("Berlin"));

        Assert.Empty(airportsManager.ListAirportsByCity(""));
        Assert.Empty(airportsManager.ListAirportsByCity("Sofia"));

    }

    // ListAirportsByCountry method test
    [Fact]
    public void AirportsManagerClass_ListAirportsByCountry_Valid()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.AddAirport("id1", "name1", "Paris", "France");
        airportsManager.AddAirport("id2", "name2", "Paris", "France");
        airportsManager.AddAirport("id3", "name3", "Nice", "France");
        airportsManager.AddAirport("id4", "name4", "Melbourne", "Australia");
        airportsManager.AddAirport("id5", "name5", "Berlin", "Germany");

        Assert.Equal(3, airportsManager.ListAirportsByCountry("France").Count);
        Assert.Single(airportsManager.ListAirportsByCountry("Australia"));
        Assert.Single(airportsManager.ListAirportsByCountry("Germany"));

        Assert.Empty(airportsManager.ListAirportsByCountry("China"));
        Assert.Empty(airportsManager.ListAirportsByCountry(""));

    }

    //Valid HasAirportByName method test
    [Fact]
    public void AirportsManagerClass_HasAirportByName_Valid()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.AddAirport("id1", "name1", "Paris", "France");
        airportsManager.AddAirport("id2", "name2", "Paris", "France");
        airportsManager.AddAirport("id3", "name3", "Nice", "France");
        airportsManager.AddAirport("id4", "name4", "Melbourne", "Australia");
        airportsManager.AddAirport("id5", "name5", "Berlin", "Germany");

        Assert.True(airportsManager.HasAirportByName("name1"));
        Assert.True(airportsManager.HasAirportByName("name2"));
        Assert.True(airportsManager.HasAirportByName("name3"));
        Assert.True(airportsManager.HasAirportByName("name4"));
        Assert.True(airportsManager.HasAirportByName("name5"));

    }

    // Valid HasAirportById method test
    [Fact]
    public void AirportsManagerClass_HasAirportById_Valid()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.AddAirport("id1", "name1", "Paris", "France");
        airportsManager.AddAirport("id2", "name2", "Paris", "France");
        airportsManager.AddAirport("id3", "name3", "Nice", "France");
        airportsManager.AddAirport("id4", "name4", "Melbourne", "Australia");
        airportsManager.AddAirport("id5", "name5", "Berlin", "Germany");

        Assert.True(airportsManager.HasAirportById("id1"));
        Assert.True(airportsManager.HasAirportById("id2"));
        Assert.True(airportsManager.HasAirportById("id3"));
        Assert.True(airportsManager.HasAirportById("id4"));
        Assert.True(airportsManager.HasAirportById("id5"));

    }

    // Valid LoadData method test
    [Fact]
    public void AirportsManagerClass_LoadAirports_Valid()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        Assert.Equal(airportsManager.AirportsByCountry?.Count, 6);
        Assert.Equal(airportsManager.AirportsByCity?.Count, 7);

        Assert.False(airportsManager.HasAirportByName("John F. Kennedy International Airport"));
        Assert.True(airportsManager.HasAirportByName("Orly Airport"));

        Assert.Equal(airportsManager.ListAirportsByCountry("France")?.Count, 2);
        Assert.Equal(airportsManager.ListAirportsByCountry("Turkey")?.Count, 2);
        Assert.Equal(airportsManager.ListAirportsByCountry("Cuba")?.Count, 1);
        Assert.Equal(airportsManager.ListAirportsByCountry("China")?.Count, 1);
        Assert.Equal(airportsManager.ListAirportsByCountry("Bulgaria")?.Count, 1);

        Assert.Equal(airportsManager.ListAirportsByCity("Paris")?.Count, 2);
        Assert.Equal(airportsManager.ListAirportsByCity("Istanbul")?.Count, 1);
        Assert.Equal(airportsManager.ListAirportsByCity("Sofia")?.Count, 1);
        Assert.Equal(airportsManager.ListAirportsByCity("Beijing")?.Count, 1);
    }
    // Invalid LoadAirports method test ( invalid file data )
    [Fact]
    public void AirportsManagerClass_LoadAirports_Invalid_InvalidFileData()
    {
        string filename = Constants.Constants.INVALID_AIRPORTS_FILENAME;
        AirportsManager airportsManager = new AirportsManager();
        InvalidFileDataException exception = Assert.Throws<InvalidFileDataException>(() => airportsManager.LoadAirports(filename));
        Assert.Equal($"Invalid file data in {filename}!\nAirport id should be of length 2-4 and contain only alphanumerical symbols!\nAirport name, city and country should be non-empty and contain only alphabetical symbols!", exception.Message);

    }

    // Invalid LoadAirports method test ( nonexistent file )
    [Fact]
    public void AirportsManagerClass_LoadAirports_Invalid_NonexistentFile()
    {
        string fileName = Constants.Constants.NONEXISTENT_FILENAME;
        string filePath = $"{Constants.Constants.PATH_TO_FILE}/{fileName}";
        AirportsManager airportsManager = new AirportsManager();
        FileNotFoundException exception = Assert.Throws<FileNotFoundException>(() => airportsManager.LoadAirports(fileName));
        Assert.Equal("File not found: " + filePath, exception.Message);

    }
}
