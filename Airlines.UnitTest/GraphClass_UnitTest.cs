﻿
using AirlinesBusiness.DataStructures;
using AirlinesBusiness.Managers;
using AirlinesBusiness.Models;

namespace AirlinesUnitTest.GraphClassNS;
public class GraphClassUnitTest
{
    // AddEdge method test
    [Theory]
    [InlineData(1, new int[] { 1, 4, 5, 3, 2, 3 }, new int[] { 1, 4, 5, 3, 2 })]
    [InlineData(4,new int[] {1, 4, 1, 1, 1, 1}, new int[] { 1, 4})]
    [InlineData(5,new int[] {5, 3, 3, 3, 2, 3},new int[] { 5, 3, 2})]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void GraphClass_AddEdge_Valid(int vertex,int[] edges,int[] expectedEdges)
    {
        Graph<int> graph = new Graph<int>();
        graph.AddVertex(vertex);
        foreach(int edge in edges)
        {
            graph.AddEdge(vertex, edge,0,0);
        }
        Assert.Equal(graph.GetNode(vertex)?.Neighbours.Select(neighbour=>neighbour.NeighbourData).ToArray(), expectedEdges);
    }

    // AddVertex method test
    [Theory]
    [InlineData(new int[] { 1, 4, 5, 3, 2, 3 }, new int[] { 1, 4, 5, 3, 2 })]
    [InlineData(new int[] { 1, 4, 1, 1, 1, 1 }, new int[] { 1, 4 })]
    [InlineData(new int[] { 5, 3, 3, 3, 2, 3 }, new int[] { 5, 3, 2 })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void GraphClass_AddVertex_Valid(int[] vertexes, int[] expectedVertexes)
    {
        Graph<int> graph = new Graph<int>();
        foreach (int vertex in vertexes)
        {
            graph.AddVertex(vertex);
        }
        Assert.Equal(graph.GraphNodes.Select(node=>node.Data).ToArray(), expectedVertexes);
    }

    [Theory]
    [InlineData(new int[] { 1,2,3,4})]
    [InlineData(new int[] { 1,2,3,4,5,2,3,2})]
    [InlineData(new int[] { 1, 2 })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void GraphClass_GetNode_Valid(int[] vertexes)
    {
        Graph<int> graph = new Graph<int>();
        foreach(int vertex in vertexes)
        {
            graph.AddVertex(vertex);
        }
        foreach(int vertex in vertexes)
        {
            Assert.NotNull(graph.GetNode(vertex));
            Assert.Equal(graph.GetNode(vertex)?.Data, vertex);
        }
    }

    [Theory]
    [InlineData(new int[] { 1, 2, 3, 4 })]
    [InlineData(new int[] { 1, 2, 3, 4, 5, 2, 3, 2 })]
    [InlineData(new int[] { 1, 2 })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void GraphClass_ContainsNode_Valid(int[] vertexes)
    {
        Graph<int> graph = new Graph<int>();
        foreach (int vertex in vertexes)
        {
            graph.AddVertex(vertex);
        }
        foreach (int vertex in vertexes)
        {
            Assert.True(graph.ContainsNode(vertex));
        }
    }

}
