﻿using AirlinesBusiness.Models;
using AirlinesBusiness.Managers;

namespace AirlinesUnitTest.RouteClassNS;
public class RouteClassUnitTest
{
    // Class constructor test
    [Fact]
    public void RouteClass_Constrictor_Valid()
    {
        Route route = new Route();
        Assert.True(route.IsRouteEmpty());
    }

    // AddFlight method test
    [Theory]
    [InlineData(new string[] { "FL123", "FL456", "FL789" }, new string[] { "FL123", "FL456", "FL789" })]
    [InlineData(new string[] { "FL123", "FL202", "FL456" }, new string[] { "FL123", "FL456" })]
    [InlineData(new string[] { "FL123", "FL202", "FL101" }, new string[] { "FL123" })]
    [InlineData(new string[] { "FL122", "FL122", "" }, new string[] { })]
    [InlineData(new string[] { }, new string[] { })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void RouteClass_AddFlight_Valid(string[] flightIds, string[] expectedRouteFlightIds)
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);
       
        Route route = new Route();
        foreach (string id in flightIds)
        {
            Flight? flight = flightsManager.FlightById(id);
            if (flight != null)
            {
                route.AddFlight(flight);
            }

        }
        Assert.Equal(expectedRouteFlightIds, route.FlightRoute.Select(flight => flight.Id));
    }

    // RemoveAtEnd method test
    [Theory]
    [InlineData(new string[] { "FL123", "FL456", "FL789" }, new string[] { "FL123", "FL456" })]
    [InlineData(new string[] { "FL123", "FL202", "FL456" }, new string[] { "FL123" })]
    [InlineData(new string[] { "FL123", "FL202", "FL101" }, new string[] { })]
    [InlineData(new string[] { }, new string[] { })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void RouteClass_RemoveFlightAtEnd_Valid(string[] flightIds, string[] expectedRouteFlightIds)
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME, airportsManager, aircraftsManager);

        Route route = new Route();
        foreach (string id in flightIds)
        {
            Flight? flight = flightsManager.FlightById(id);
            if (flight != null)
            {
                route.AddFlight(flight);
            }

        }
        route.RemoveFlightAtEnd();
        Assert.Equal(expectedRouteFlightIds, route.FlightRoute.Select(flight => flight.Id));
    }

    // EmptyRoute method test
    [Fact]
    public void RouteClass_EmptyRoute_Valid()
    {
        Flight flight1 = new Flight("FL123", "d1", "a1", "model1",1.2,1.2);
        Flight flight2 = new Flight("FL122", "d2", "a2", "model2",1.1,1);
        Flight flight3 = new Flight("FL1235", "d3", "a3", "model3",3,3);
        Flight flight4 = new Flight("FL125", "d4", "a4", "model4",0,-5);
        Route route = new Route();
        
        route.AddFlight(flight1);
        route.AddFlight(flight2);
        route.AddFlight(flight3);
        route.AddFlight(flight4);

        Assert.NotEmpty(route.FlightRoute);
        route.EmptyRoute();
        Assert.Empty(route.FlightRoute);
    }

    // LastFlight method test
    [Theory]
    [InlineData(new string[] { "FL123", "FL456", "FL789" }, "FL789")]
    [InlineData(new string[] { "FL123" }, "FL123")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void RouteClass_LastFlight_Valid(string[] flightIds,string expectedFlightId)
    {
       
        Route route = new Route();
        foreach(string flightId in flightIds)
        {
            Flight flight = new Flight(flightId, "A1", "A1", "model",1,1);
            route.AddFlight(flight);
        }

        Assert.Equal(route.LastFlight()!.Id, expectedFlightId);
    }
}
