﻿using AirlinesBusiness.Models.Aircraft;

namespace AirlinesUnitTest.PrivateAircraftClassNS;
public class PrivateAircraftClassUnitTest
{
    // Class constructor test
    [Theory]
    [InlineData("Model1",23)]
    [InlineData("Model2",30)]
    [InlineData("Model3",0)]
    [InlineData("",2)]
    public void PrivateAircraftClass_Constructor_Valid(string model,uint seats)
    {
        PrivateAircraft aircraft = new PrivateAircraft(model,seats);
        Assert.Equal(aircraft.Model,model);
        Assert.Equal(aircraft.Seats,seats);
    }

}

