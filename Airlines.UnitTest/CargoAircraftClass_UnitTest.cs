﻿using AirlinesBusiness.Models.Aircraft;

namespace AirlinesUnitTest.CargoAircraftNS;
public class CargoAircraftClassUnitTest
{
    // Valid CanBeReserved method test
    [Theory]
    [InlineData("AircraftModel", 32000, 23.1, 31999, 23)]
    [InlineData("AircraftModel", 30000, 0.1, 30, 0)]
    [InlineData("AircraftModel", 20000, 20, 20000, 20)]
    [InlineData("AircraftModel", 10, 2.1, 9, 2)]
    public void CargoAircraftClass_CanBeReserved_Valid(string model, uint load, double volume, uint testLoad, double testVolume)
    {
        CargoAircraft cargoAircraft = new CargoAircraft(model, load, volume);
        Assert.True(cargoAircraft.CanBeReserved(testLoad, testVolume));
    }

    // Invalid CanBeReserved method test
    [Theory]
    [InlineData("AircraftModel", 32000, 23.1, 32001, 23)]
    [InlineData("AircraftModel", 30000, 0.1, 30, 0.15)]
    [InlineData("AircraftModel", 20000, 20, 200, 21)]
    [InlineData("AircraftModel", 0, 0, 11, 0)]
    public void CargoAircraftClass_CanBeReserved_Invalid(string model, uint load, double volume, uint testLoad, double testVolume)
    {
        CargoAircraft cargoAircraft = new CargoAircraft(model, load, volume);
        Assert.False(cargoAircraft.CanBeReserved(testLoad, testVolume));
    }

}
