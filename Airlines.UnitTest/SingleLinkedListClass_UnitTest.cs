﻿using AirlinesBusiness.DataStructures;

namespace Airlines.SingleLinkedListClassNS;
public class SingleLinkedListClassUnitTest
{
    // AddLast method test
    [Theory]
    [InlineData(new int[] { 1, 2, 3, 4, 5, 6 }, new int[] {1,2,3,4,5,6})]
    [InlineData(new int[] { 1 }, new int[] {1})]
    [InlineData(new int[] { 23,20,20,20 }, new int[] {23,20,20,20})]
    [InlineData(new int[] {}, new int[] {})]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void SingleLinkedListClass_AddLast_Valid(int[] elements, int[] expectedElements)
    {
        SingleLinkedList<int> list = new SingleLinkedList<int>();
        foreach(int element in elements)
        {
            list.AddLast(element);
        }
        Assert.Equal(expectedElements, list.ToArray());
    }

    // GetLast method test
    [Theory]
    [InlineData(new int[] { 1, 2, 3, 4, 5, 6 }, 6)]
    [InlineData(new int[] { 1 }, 1)]
    [InlineData(new int[] { 23, 20, 20, 20 }, 20)]
    [InlineData(new int[] { }, 0)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void SingleLinkedListClass_RemoveLast_Valid(int[] elements, int expectedElement)
    {
        SingleLinkedList<int> list = new SingleLinkedList<int>();
        foreach (int element in elements)
        {
            list.AddLast(element);
        }
        int lastElement = list.GetLast();
        Assert.Equal(expectedElement,lastElement);
    }

    // Valid HasElement method test
    [Theory]
    [InlineData(new int[] { 1, 2, 3, 4, 5, 6 }, 6)]
    [InlineData(new int[] { 1 }, 1)]
    [InlineData(new int[] { 23, 20, 20, 20 }, 20)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void SingleLinkedListClass_HasElement_Valid(int[] elements, int validElement)
    {
        SingleLinkedList<int> list = new SingleLinkedList<int>();
        foreach (int element in elements)
        {
            list.AddLast(element);
        }
        Assert.True(list.HasElement(validElement));
        
    }

    // Invalid HasElement method test
    [Theory]
    [InlineData(new int[] { 1, 2, 3, 4, 5, 6 }, 7)]
    [InlineData(new int[] { 1 }, 2)]
    [InlineData(new int[] { 23, 20, 20, 20 } ,34)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void SingleLinkedListClass_HasElement_Invalid(int[] elements, int invalidElement)
    {
        SingleLinkedList<int> list = new SingleLinkedList<int>();
        foreach (int element in elements)
        {
            list.AddLast(element);
        }
        Assert.False(list.HasElement(invalidElement));

    }

    // Clear method test
    [Theory]
    [InlineData(new int[] { 1, 2, 3, 4, 5, 6 })]
    [InlineData(new int[] { 1 })]
    [InlineData(new int[] { 23, 20, 20, 20 })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void SingleLinkedListClass_ClearMethod_Valid(int[] elements)
    {
        SingleLinkedList<int> list = new SingleLinkedList<int>();
        foreach (int element in elements)
        {
            list.AddLast(element);
        }
        Assert.NotEmpty(list);
        list.Clear();
        Assert.Empty(list);
    }

    // IsEmpty method test
    [Theory]
    [InlineData(new int[] { 1, 2, 3, 4, 5, 6 },false)]
    [InlineData(new int[] { 1 },false)]
    [InlineData(new int[] { },true)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void SingleLinkedListClass_IsEmpty_Valid(int[] elements,bool isEmpty)
    {
        SingleLinkedList<int> list = new SingleLinkedList<int>();
        foreach (int element in elements)
        {
            list.AddLast(element);
        }
        Assert.Equal(list.IsEmpty(), isEmpty);
    }

}
