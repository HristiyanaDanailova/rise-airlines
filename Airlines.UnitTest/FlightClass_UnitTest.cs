﻿using AirlinesBusiness.Models;

namespace AirlinesUnitTest.FlightClassNS;
public class FlightClassUnitTest
{
    // Class constructor test
    [Theory]
    [InlineData("id1", "departureAirport1", "departureAirport2", "Boeing 147",2.3,2,2.3,2)]
    [InlineData("id1", "", "","",0,0,0,0)]
    [InlineData("", "", "","",-2,-3.3,0,0)]
    public void FlightClass_Constructor_Valid(string id, string departureAirport, string arrivalAirport, string aircraftModel,double price, double hours, double expectedPrice,double expectedHours)
    {
        Flight flight = new Flight(id,departureAirport,arrivalAirport,aircraftModel,price,hours);
        Assert.Equal(flight.Id, id);
        Assert.Equal(flight.DepartureAirport, departureAirport);
        Assert.Equal(flight.ArrivalAirport, arrivalAirport);
        Assert.Equal(flight.AircraftModel, aircraftModel);
        Assert.Equal(flight.Price, expectedPrice);
        Assert.Equal(flight.Hours, expectedHours);
    }
}
