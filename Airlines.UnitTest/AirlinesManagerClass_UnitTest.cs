﻿using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Managers;

namespace AirlinesUnitTest.AirlinesManagerNS;
public class AirlinesManagerClassUnitTest
{
    // AddAIirline method test
    [Theory]
    [InlineData("name1", "name2", "name3", "name4", new string[] { "name1", "name2", "name3", "name4" })]
    [InlineData("name1", "name2", "name3", "name3", new string[] { "name1", "name2", "name3" })]
    [InlineData("name1", "name1", "name1", "name1", new string[] { "name1" })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void AirlinesManagerClass_AddAirline_Valid(string airline1, string airline2, string airline3, string airline4, string[] airlineNames)
    {

        AirlinesManager airlinesManager = new AirlinesManager();
        airlinesManager.AddAirline(airline1);
        airlinesManager.AddAirline(airline2);
        airlinesManager.AddAirline(airline3);
        airlinesManager.AddAirline(airline4);

        Assert.Equal(airlinesManager.Airlines.Keys, airlineNames);
    }

    // Sort metehod test
    [Theory]
    [InlineData("name1", "name2", "name3", "name4", new string[] { "name1", "name2", "name3", "name4" }, new string[] { "name4", "name3", "name2", "name1" })]
    [InlineData("name1", "name2", "name3", "name3", new string[] { "name1", "name2", "name3" }, new string[] { "name3", "name2", "name1" })]
    [InlineData("name1", "name1", "name1", "name1", new string[] { "name1" }, new string[] { "name1" })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void AirlinesManagerClass_SortAirlines_Valid(string airline1, string airline2, string airline3, string airline4, string[] expectedSortedAscending, string[] expexctedSortedDescending)
    {
        AirlinesManager airlinesManager = new AirlinesManager();
        airlinesManager.AddAirline(airline1);
        airlinesManager.AddAirline(airline2);
        airlinesManager.AddAirline(airline3);
        airlinesManager.AddAirline(airline4);
        airlinesManager.SortAirlines("ascending");
        Assert.Equal(airlinesManager.Airlines.Keys, expectedSortedAscending);
        airlinesManager.SortAirlines("descdending");
        Assert.Equal(airlinesManager.Airlines.Keys, expexctedSortedDescending);
    }

    // Valid HasAirline method test
    [Theory]
    [InlineData(new string[] { "aaa", "abc", "111", "a12", "112" }, "111")]
    [InlineData(new string[] { "aaa", "abc", "111", "a12", "112" }, "112")]
    [InlineData(new string[] { "ddd" }, "ddd")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void AirlinesManagerClass_HasAirline_Valid(string[] airlines, string validSearchTerm)
    {
        AirlinesManager airlinesManager = new AirlinesManager();
        foreach (string airline in airlines)
        {
            airlinesManager.AddAirline(airline);
        }
        Assert.True(airlinesManager.HasAirline(validSearchTerm));
    }

    // Invalid HasAirline method test
    [Theory]
    [InlineData(new string[] { "aaa", "abc", "111", "a12", "112" }, "dds")]
    [InlineData(new string[] { "aaa", "abc", "111", "a12", "112" }, "hds")]
    [InlineData(new string[] { "ddd" }, "ccc")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void AirlinesManagerClass_HasAirline_Invalid(string[] airlines,string invalidSearchTerm)
    {
        AirlinesManager airlinesManager = new AirlinesManager();
        foreach (string airline in airlines)
        {
            airlinesManager.AddAirline(airline);
        }
        Assert.False(airlinesManager.HasAirline(invalidSearchTerm));
    }

    // Valid LoadAirlines method test
    [Fact]
    public void AirlinesManagerClass_LoadAirlines_Valid()
    {
        AirlinesManager airlinesManager = new AirlinesManager();
        airlinesManager.LoadAirlines(Constants.Constants.VALID_AIRLINES_FILENAME);
        Assert.Equal(airlinesManager.Airlines?.Count, 4);
        Assert.False(airlinesManager.HasAirline("XYZWZWZ"));
        Assert.False(airlinesManager.HasAirline("A BCDEFG"));
        Assert.True(airlinesManager.HasAirline("Jet2"));
        Assert.True(airlinesManager.HasAirline("Sky"));
        Assert.True(airlinesManager.HasAirline("Air1"));
        Assert.True(airlinesManager.HasAirline("BA"));

    }
    // Invalid LoadAirlines method test ( invalid file data )
    [Fact]
    public void AirlinesManagerClass_LoadAirlines_Inalid_InvalidFileData()
    {
        string filename = Constants.Constants.INVALID_AIRLINES_FILENAME;
        AirlinesManager airlinesManager = new AirlinesManager();
        InvalidFileDataException exception = Assert.Throws<InvalidFileDataException>(() => airlinesManager.LoadAirlines(filename));
        Assert.Equal($"Invalid file data in {filename}! Airline names should be non-empty and contain no more that 6 symbols", exception.Message);

    }

    // Invalid LoadAirlines method test ( nonexistent file )
    [Fact]
    public void AirlinesManagerClass_LoadAirlines_Invalid_NonexistentFile()
    {
        string fileName = Constants.Constants.NONEXISTENT_FILENAME;
        string filePath = $"{Constants.Constants.PATH_TO_FILE}/{fileName}";
        AirlinesManager airlinesManager = new AirlinesManager();
        FileNotFoundException exception = Assert.Throws<FileNotFoundException>(() => airlinesManager.LoadAirlines(fileName));
        Assert.Equal("File not found: " + filePath, exception.Message);

    }
}
