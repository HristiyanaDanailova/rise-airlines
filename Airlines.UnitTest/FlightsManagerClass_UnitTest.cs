﻿using AirlinesBusiness.Managers;
using AirlinesBusiness.CustomExceptions;
using System.Reflection;
namespace AirlinesUnitTest.FlightsManagerNS;
public class FlightsManagerClassUnitTest
{
    // AddFlght method test
    [Theory]
    [InlineData(new string[] { "id1", "departureAirport1", "arrivalAirport1", "aircraftModel1", "2", "4" },
            new string[] { "id2", "departureAirport2", "arrivalAirport2", "aircraftModel2", "3.3", "2.0" },
            new string[] { "id3", "departureAirport3", "arrivalAirport3", "aircraftModel3", "-1", "0.2" })]
    [InlineData(new string[] { "id4", "departureAirport4", "arrivalAirport4", "aircraftModel4", "12.2", "5" },
            new string[] { "id5", "departureAirport5", "arrivalAirport5", "aircraftModel5", "-5", "-5" },
            new string[] { "id6", "departureAirport6", "arrivalAirport6", "aircraftModel6", "300", "2" })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void FlightsManagerClass_AddFlight_Valid(string[] flight1,string[] flight2, string[] flight3)
    {
        FlightsManager flightsManager = new FlightsManager();
        flightsManager.AddFlight(flight1[0], flight1[1], flight1[2], flight1[3], double.Parse(flight1[4]), double.Parse(flight1[5]));
        flightsManager.AddFlight(flight2[0], flight2[1], flight2[2], flight2[3], double.Parse(flight2[4]), double.Parse(flight2[5]));
        flightsManager.AddFlight(flight3[0], flight3[1], flight3[2], flight3[3], double.Parse(flight3[4]), double.Parse(flight3[5]));

        Assert.Equal(3,flightsManager.Flights.Count);
        
    }

    // Sort method test
    [Fact]
    public void FlightsManagerClass_SortFlights_Valid()
    {
        FlightsManager flightsManager = new FlightsManager();

        flightsManager.AddFlight("abc", "departureAirport1", "arrivalAirport1","aircraftModel1",2.3,2.3);
        flightsManager.AddFlight("aaa", "departureAirport1", "arrivalAirport1", "aircraftModel1",2.3,2.3);
        flightsManager.AddFlight("a12", "departureAirport1", "arrivalAirport1", "aircraftModel1",2.3,2.3);
        flightsManager.AddFlight("aaa1", "departureAirport1", "arrivalAirport1", "aircraftModel1",2.3,2.3);

        string[] expectedSortedAscending = { "a12", "aaa", "aaa1", "abc" };
        string[] expectedSortedDescending = {"abc","aaa1", "aaa", "a12"};
        flightsManager.SortFlights("ascending");
        Assert.Equal(flightsManager.Flights.Select(flight => flight.Id).ToArray(), expectedSortedAscending); ;
        flightsManager.SortFlights("descending");
        Assert.Equal(flightsManager.Flights.Select(flight => flight.Id).ToArray(),expectedSortedDescending);
    }

    // Search method test
    [Theory]
    [InlineData(new string[] { "aaa", "abc", "111", "a12", "112" }, "111", "dds")]
    [InlineData(new string[] { "aaa", "abc", "111", "a12", "112" }, "112", "hds")]
    [InlineData(new string[] { "ddd" }, "ddd", "ccc")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void FlightsManagerClass_HasFlight_Valid(string[] flightIds, string validSearchTerm, string invalidSearchTerm)
    {
        FlightsManager flightsManager = new FlightsManager();
        foreach (string id in flightIds)
        {
            flightsManager.AddFlight(id,"departureAirport","arivalAirport", "aircraftModel",1,1);
        }
        Assert.True(flightsManager.HasFlight(validSearchTerm));
        Assert.False(flightsManager.HasFlight(invalidSearchTerm));
    }

    // Valid LoadFlights method test
    [Fact]
    public void FlightsManagerClass_LoadAirports_Valid()
    {
        FlightsManager flightsManager = new FlightsManager();
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);

        flightsManager.LoadFlights(Constants.Constants.VALID_FLIGHTS_FILENAME,airportsManager,aircraftsManager);
        Assert.Equal(5,flightsManager.Flights.Count);
        Assert.True(flightsManager.HasFlight("FL123"));
        Assert.True(flightsManager.HasFlight("FL456"));
        Assert.True(flightsManager.HasFlight("FL789"));
        Assert.True(flightsManager.HasFlight("FL101"));
        Assert.True(flightsManager.HasFlight("FL202"));
        Assert.False(flightsManager.HasFlight("FL1303"));
        Assert.False(flightsManager.HasFlight("FL404"));
    }

    // Invalid LoadFlights method test ( invalid file data )
    [Fact]
    public void FlightsManagerClass_LoadAirports_Invalid_InvalidFileData()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);

        string filename = Constants.Constants.INVALID_FLIGHTS_FILENAME;
        FlightsManager flightsManager = new FlightsManager();
        InvalidFileDataException exception =  Assert.Throws<InvalidFileDataException>(() => flightsManager.LoadFlights(filename,airportsManager,aircraftsManager));
        Assert.Equal($"Invalid file data in {filename}! File fields should be of format: \"<flight id>,<departure airport id>,<arrival airport id>,<aircraft model> <ticket price> <flight duration in hours>\"", exception.Message);

    }

    // Invalid LoadFlights method test ( invalid aircraft model )
    [Fact]
    public void FlightsManagerClass_LoadAirports_Invalid_InvalidAircraftModel()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);

        string fileName = Constants.Constants.INVALID_AIRCRAFT_MODEL_FLIGHTS_FILENAME;
        FlightsManager flightsManager = new FlightsManager();
        EntityNotFoundException exception = Assert.Throws<EntityNotFoundException>(() => flightsManager.LoadFlights(fileName, airportsManager, aircraftsManager));
        Assert.Equal($"Invalid file data in {fileName}! No aircraft of model Gulfstream found!", exception.Message);

    }

    // Invalid LoadFlights method test ( invalid aircraft model )
    [Fact]
    public void FlightsManagerClass_LoadAirports_Invalid_InvalidAirportId()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);

        string fileName = Constants.Constants.INVALID_AIRPORT_ID_FLIGHTS_FILENAME;
        FlightsManager flightsManager = new FlightsManager();
        EntityNotFoundException exception = Assert.Throws<EntityNotFoundException>(() => flightsManager.LoadFlights(fileName, airportsManager, aircraftsManager));
        Assert.Equal($"Invalid file data in {fileName}! No airport with id O found!", exception.Message);

    }

    // Invalid LoadFlights method test ( nonexistent file name )
    [Fact]
    public void FlightsManagerClass_LoadAirports_Invalid_NonexistentFile()
    {
        AirportsManager airportsManager = new AirportsManager();
        airportsManager.LoadAirports(Constants.Constants.VALID_AIRPORTS_FILENAME);
        AircraftsManager aircraftsManager = new AircraftsManager();
        aircraftsManager.LoadAircrafts(Constants.Constants.VALID_AIRCRAFTS_FILENAME);

        string fileName = Constants.Constants.NONEXISTENT_FILENAME;
        string filePath = $"{Constants.Constants.PATH_TO_FILE}/{fileName}";
        FlightsManager flightsManager = new FlightsManager();
        FileNotFoundException exception = Assert.Throws<FileNotFoundException>(() => flightsManager.LoadFlights(fileName,airportsManager,aircraftsManager));
        Assert.Equal($"File not found: {filePath}", exception.Message);

    }
}
