﻿using AirlinesBusiness.DataStructures;
using AirlinesBusiness.Models;

namespace Airlines.TreeClassNS;
public class TreeClassUnitTest
{
    // AddChild level 1 method test
    [Fact]
    public void TreeClass_AddChildren_Level_1_Valid()
    {
        Tree<int> tree = new Tree<int>(1);

        tree.Root.AddChild(new TreeNode<int>(2)); 
        tree.Root.AddChild(new TreeNode<int>(3));

        Assert.Equal(2, tree.Root.Children!.Count); 
        Assert.Equal(2, tree.Root.Children[0].Data);
        Assert.Equal(3, tree.Root.Children[1].Data);
    }

    // AddChild level 2 method test
    [Fact]
    public void TreeClass_AddChildren_Level_2_Valid()
    {
        Tree<int> tree = new Tree<int>(1);

        tree.Root.AddChild(new TreeNode<int>(2));
        tree.Root.AddChild(new TreeNode<int>(3));
        tree.Root.Children![0].AddChild(new TreeNode<int>(5));
        tree.Root.Children![1].AddChild(new TreeNode<int>(5));
        tree.Root.Children![1].AddChild(new TreeNode<int>(6));

        Assert.Single(tree.Root.Children![0].Children!);
        Assert.Equal(2, tree.Root.Children![1].Children!.Count);

        Assert.Equal(5, tree.Root!.Children[0].Children![0].Data);
        Assert.Equal(5, tree.Root!.Children[1].Children![0].Data);
        Assert.Equal(6, tree.Root!.Children[1].Children![1].Data);

    }


    // FindRoute method test
    [Fact]
    public void TreeClass_FindRoute_Valid()
    {
        Tree<int> tree = new Tree<int>(1);

        tree.Root.AddChild(new TreeNode<int>(2));
        tree.Root.AddChild(new TreeNode<int>(3));

        tree.Root.Children![0].AddChild(new TreeNode<int>(4));
        tree.Root.Children![0].AddChild(new TreeNode<int>(5));
        tree.Root.Children![1].AddChild(new TreeNode<int>(6));

        tree.Root.Children![0].Children![0].AddChild(new TreeNode<int>(7));
        tree.Root.Children![0].Children![1].AddChild(new TreeNode<int>(8));
        tree.Root.Children![1].Children![0].AddChild(new TreeNode<int>(9));

        List<TreeNode<int>> routeToNine = Tree<int>.FindRoute(9, tree.Root);
        List<TreeNode<int>> routeToFive = Tree<int>.FindRoute(5, tree.Root);
        List<TreeNode<int>> routeToTen = Tree<int>.FindRoute(10, tree.Root);

        Assert.Equal(new List<int> { 1, 3, 6, 9 }, routeToNine.Select(node => node.Data));
        Assert.Equal(new List<int> { 1, 2, 5 }, routeToFive.Select(node => node.Data));
        Assert.Empty(routeToTen);
    }

}
