using System.Diagnostics;
using Xunit.Abstractions;

using AirlinesBusiness.SortingNS;
using AirlinesBusiness.SearchingNS;

namespace AirlinesUnitTest.SortingAndSearchingNS;
public class SortingAndSearchingUnitTest(ITestOutputHelper output)
{

    private readonly ITestOutputHelper output = output;

    // random string[] array generator methods
    public static string GenerateRandomAlphaNumericString(int length, int seed)
    {
        string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        char[] charArray = new char[length];
        int randomIndex = seed % characters.Length;
        for (int i = 0; i < length; i++)
        {
            charArray[i] = characters[randomIndex];
            randomIndex = (randomIndex + seed) % characters.Length;
        }
        return new string(charArray);
    }

    public static string[] GenerateArrayOfStrings(int arrayLength, int seed)
    {
        string[] array = new string[arrayLength];
        for (int i = 0; i < arrayLength; i++)
        {
            array[i] = GenerateRandomAlphaNumericString(5, seed);
            seed = (seed + 1) % arrayLength;
        }
        return array;
    }

    // Valid BubbleSort method test
    [Theory]
    [InlineData(new string[] { "abc", "abc12", "aab21", "1abc", "aaa", "123", "111" }, new string[] { "111", "123", "1abc", "aaa", "aab21", "abc", "abc12" }, new string[] { "abc12", "abc", "aab21", "aaa", "1abc", "123", "111" })]
    [InlineData(new string[] { }, new string[] { }, new string[] { })]
    [InlineData(new string[] { "abc" }, new string[] { "abc" }, new string[] { "abc" })]
    [InlineData(new string[] { "abc", "abc", "abc", "1abc" }, new string[] { "1abc", "abc", "abc", "abc" }, new string[] { "abc", "abc", "abc", "1abc" })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void SorterClass_BubbleSort_Valid(string[] array, string[] expectedSortedArrayAscending, string[] expectedSortedArrayDescending)
    {
        array.BubbleSort(array.Length, true);
        Assert.Equal(array, expectedSortedArrayAscending);
        array.BubbleSort(array.Length, false);
        Assert.Equal(array, expectedSortedArrayDescending);

    }

    // InVvalid BubbleSort method test
    [Fact]
    public void SorterClass_BubbleSort_Invlaid()
    {
        string[] array = ["abc", "abc12", "aab21", "1abc", "aaa", "123", "111"];
        string[] sortedArray = ["123", "abc12", "1abc", "abc", "aab21", "aaa", "111"];
        array.BubbleSort(array.Length, true);
        Assert.NotEqual(array, sortedArray);
        array.BubbleSort(array.Length, false);
        Assert.NotEqual(array, sortedArray);
    }

    // Valid SelectionSort method test
    [Theory]
    [InlineData(new string[] { "abc", "abc12", "aab21", "1abc", "aaa", "123", "111" }, new string[] { "111", "123", "1abc", "aaa", "aab21", "abc", "abc12" }, new string[] { "abc12", "abc", "aab21", "aaa", "1abc", "123", "111" })]
    [InlineData(new string[] { }, new string[] { }, new string[] { })]
    [InlineData(new string[] { "abc" }, new string[] { "abc" }, new string[] { "abc" })]
    [InlineData(new string[] { "abc", "abc", "abc", "1abc" }, new string[] { "1abc", "abc", "abc", "abc" }, new string[] { "abc", "abc", "abc", "1abc" })]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1861:Avoid constant arrays as arguments", Justification = "<Pending>")]
    public void SorterClass_SelectionSort_Valid(string[] array, string[] expectedSortedArrayAscending, string[] expectedSortedArrayDescending)
    {
        array.SelectionSort(array.Length, true);
        Assert.Equal(array, expectedSortedArrayAscending);
        array.SelectionSort(array.Length, false);
        Assert.Equal(array, expectedSortedArrayDescending);


    }

    // Invalid SelectionSort method test
    [Fact]
    public void SorterClass_SelectionSort_Invalid()
    {
        string[] array = ["abc", "abc12", "aab21", "1abc", "aaa", "123", "111"];
        string[] sortedArray = ["123", "abc12", "1abc", "abc", "aab21", "aaa", "111"];
        array.SelectionSort(array.Length, true);
        Assert.NotEqual(array, sortedArray);
        array.SelectionSort(array.Length, false);
        Assert.NotEqual(array, sortedArray);
    }

    // Valid LinearSearch method test
    [Theory]
    [InlineData("abc")]
    [InlineData("111")]
    [InlineData("1abc")]
    public void SearcherClass_LinearSearch_Valid(string target)
    {
        string[] array = ["abc", "abc12", "aab21", "1abc", "aaa", "123", "111"];
        bool searchResult = Searcher.LinearSearch(target, array, array.Length);
        Assert.True(searchResult, $"{target} should be found");
    }

    // Invalid LinearSearch method test
    [Fact]
    public void SearcherClass_LinearSearch_Invalid()
    {
        string[] array = ["abc", "abc12", "aab21", "1abc", "aaa", "123", "111"];
        string target = "aab2";
        bool searchResult = Searcher.LinearSearch(target, array, array.Length);
        Assert.False(searchResult, $"{target} should't be found");
    }

    // Valid BinarySearch method test
    [Theory]
    [InlineData("111")]
    [InlineData("aaa")]
    [InlineData("abc12")]
    public void SearcherClass_BinarySearch_Valid(string target)
    {
        string[] sortedArray = ["111", "123", "1abc", "aaa", "aab21", "abc", "abc12"];
        bool searchResult = Searcher.BinarySearch(sortedArray, sortedArray.Length, target);
        Assert.True(searchResult, $"{target} should be found");
    }

    // Invalid BinarySearch method test ( unexistent element )
    [Fact]
    public void SearcherClass_BinarySearch_Invalid_InvalidElement()
    {
        string[] sortedArray = ["111", "123", "1abc", "aaa", "aab21", "abc", "abc12"];
        string target = "aab";
        bool searchResult = Searcher.BinarySearch(sortedArray, sortedArray.Length, target);
        Assert.False(searchResult, $"{target} shouldn't be found");
    }

    // Invalid BinarySearch method test ( unsorted array )
    [Fact]
    public void SearcherClass_BinarySearch_Invalid_UnsortedArray()
    {
        string[] unsortedArray = ["123", "abc12", "1abc", "abc", "aab21", "aaa", "111"];
        string target = "111";
        bool searchResult = Searcher.BinarySearch(unsortedArray, unsortedArray.Length, target);
        Assert.False(searchResult, $"{target} shouldn't be found");
    }

    // Sorting methods performance test
    // Outputs the number of executions of both algorithms over the same pseudo-randomized input, done for 1 sec
    [Fact]
    public void SorterCLass_SortingMethods_PerformanceTests()
    {
        string[] array;
        int executionCounter = 0;
        int seed = 20;
        Stopwatch stopwatch = new Stopwatch();
        while (stopwatch.ElapsedMilliseconds < 1000)
        {
            array = GenerateArrayOfStrings(100, seed + executionCounter);
            stopwatch.Start();
            array.BubbleSort(array.Length);
            stopwatch.Stop();
            executionCounter++;
        }
        output.WriteLine($"Number of BubbleSort executions for {stopwatch.ElapsedMilliseconds}ms: {executionCounter}");
        executionCounter = 0;
        stopwatch.Reset();
        while (stopwatch.ElapsedMilliseconds < 1000)
        {
            array = GenerateArrayOfStrings(100, seed + executionCounter);
            stopwatch.Start();
            array.SelectionSort(array.Length);
            stopwatch.Stop();
            executionCounter++;
        }
        output.WriteLine($"Number of SelectionSort executions for {stopwatch.ElapsedMilliseconds}ms: {executionCounter}");

    }

    // Search methods performance test
    // Outputs the number of searches done for 1 sec over an array of 100 elements (with randomly generated index for each execution)
    [Fact]
    public void SearcherClass_SearcingMethods_PerformanceTests()
    {
        int executionCounter = 0;
        int seed = 20;
        string[] array = GenerateArrayOfStrings(100, seed);
        array.BubbleSort(array.Length);
        Stopwatch stopwatch = new Stopwatch();
        while (stopwatch.ElapsedMilliseconds < 500)
        {
            int randomIndex = new Random().Next(0, array.Length - 1);
            stopwatch.Start();
            Searcher.BinarySearch(array, array.Length, array[randomIndex]);
            stopwatch.Stop();
            executionCounter++;
        }
        output.WriteLine($"Number of BinarySearch executions for {stopwatch.ElapsedMilliseconds}ms: {executionCounter}");
        stopwatch.Reset();
        array = GenerateArrayOfStrings(100, seed);
        while (stopwatch.ElapsedMilliseconds < 500)
        {
            Random random = new Random();
            int randomIndex = random.Next(0, array.Length - 1);
            stopwatch.Start();
            Searcher.LinearSearch(array[randomIndex], array, array.Length);
            stopwatch.Stop();
            executionCounter++;
        }
        output.WriteLine($"Number of LinearSearch executions for {stopwatch.ElapsedMilliseconds}ms: {executionCounter}");

    }

}
