﻿namespace AirlinesUnitTest.Constants;
public static class Constants
{
    public readonly static string VALID_AIRPORTS_FILENAME = "validAirportsUnitTest.csv";
    public readonly static string VALID_AIRLINES_FILENAME = "validAirlinesUnitTest.csv";
    public readonly static string VALID_AIRCRAFTS_FILENAME = "validAircraftsUnitTest.csv";
    public readonly static string VALID_FLIGHTS_FILENAME = "validFlightsUnitTest.csv";
    public readonly static string VALID_ROUTES_FILENAME = "validRoutesUnitTest.txt";

    public readonly static string INVALID_AIRPORTS_FILENAME = "invalidAirportsUnitTest.csv";
    public readonly static string INVALID_AIRLINES_FILENAME = "invalidAirlinesUnitTest.csv";
    public readonly static string INVALID_AIRCRAFTS_FILENAME = "invalidAircraftsUnitTest.csv";
    public readonly static string INVALID_FLIGHTS_FILENAME = "invalidFlightsUnitTest.csv";

    public readonly static string INVALID_STARTING_POINT_ROUTES_FILENAME = "invalidStartingPointRoutesUnitTest.txt";
    public readonly static string INVALID_FLIGHT_ID_ROUTES_FILENAME = "invalidFlightIdRoutesUnitTest.txt";

    public readonly static string INVALID_AIRCRAFT_MODEL_FLIGHTS_FILENAME = "invalidAircraftModelFlightsUnitTest.csv";
    public readonly static string INVALID_AIRPORT_ID_FLIGHTS_FILENAME = "invalidAirportIdFlightsUnitTest.csv";

    public readonly static string NONEXISTENT_FILENAME = "nonexistentfilename.csv";

    public readonly static string PATH_TO_FILE = "../../../files";

}
