﻿using AirlinesBusiness.Models.Aircraft;

namespace AirlinesUnitTest.PassengerAircraftNS;
public class PassengerAircraftClassUnitTest
{
    // Valid CanBeReserved method test
    [Theory]
    [InlineData("AircraftModel", 32000, 23.1, 30, 10, 5, 15)] 
    [InlineData("AircraftModel", 30000, 50.1, 100, 30, 0, 100)]
    [InlineData("AircraftModel", 20000, 20, 200, 200, 20, 5)] 
    [InlineData("AircraftModel", 100, 2.1, 1, 1, 1, 1)]
    [InlineData("AircraftModel", 100000, 100, 1000, 100, 100, 100)] 
    [InlineData("AircraftModel", 100, 2.1, 1, 0, 0, 0)]
    [InlineData("AircraftModel", 100, 2.1, 1, 1, 2, 2)]
    public void PassengerAircraftClass_CanBeReserved_Valid(string model, uint load, double volume,uint seats, uint testSeats, uint smallBaggageCount,uint largeBaggageCount)
    {
        PassengerAircraft cargoAircraft = new PassengerAircraft(model, load, volume,seats);
        Assert.True(cargoAircraft.CanBeReserved(testSeats,smallBaggageCount,largeBaggageCount));
    }

    // Invalid CanBeReserved method test
    [Theory]
    [InlineData("AircraftModel", 1000, 10, 10, 11, 2, 0)] 
    [InlineData("AircraftModel", 1000, 10, 10, 10, 1, 200)] 
    [InlineData("AircraftModel", 1000, 10, 10, 10, 100, 12)]
    [InlineData("AircraftModel", 200, 10, 10, 10, 1, 200)]
    [InlineData("AircraftModel", 10000, 10, 10, 30, 500, 12)]
    public void PassengerAircraftClass_CanBeReserved_Invalid(string model, uint load, double volume, uint seats, uint testSeats, uint smallBaggageCount, uint largeBaggageCount)
    {
        PassengerAircraft cargoAircraft = new PassengerAircraft(model, load, volume, seats);
        Assert.False(cargoAircraft.CanBeReserved(testSeats, smallBaggageCount, largeBaggageCount));
    }

}
