﻿using AirlinesBusiness.Models;

namespace AirlinesUnitTest.AirlineClassNS;
public class AirlineClassUnitTest
{
    // Class constructor test
    [Theory]
    [InlineData("AirlineName1")]
    [InlineData("AirlineName2")]
    [InlineData("AirlineName3")]
    public void AirlineClass_Constructor_Valid(string name)
    {
        Airline airline = new Airline(name);
        Assert.Equal(airline.Name, name);
    }
}
