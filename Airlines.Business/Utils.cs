namespace AirlinesBusiness.UtilsNS;
public class ArrayUtils
{

    internal static void PrintArrayValues(string[] array, int arraySize)
    {
        Console.Write("[ ");
        for (int i = 0; i < arraySize; i++)
        {
            if (i == arraySize - 1)
            {
                Console.Write($"{array[i]}");
            }
            else
            {
                Console.Write($"{array[i]}, ");

            }
        }
        Console.WriteLine(" ]");
    }

    // creates an array with "newCapacity" size and copies "size" number of values from "arr" to the new array
    internal static string[] ResizeArray(string[] arr, int newCapacity, int size)
    {
        string[] copyArr = new string[newCapacity];
        Array.Fill(copyArr, "");
        Array.Copy(arr, copyArr, size);
        return copyArr;
    }
}
