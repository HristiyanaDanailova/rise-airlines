using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Models;

namespace AirlinesBusiness.Managers;
public class FlightsManager
{
    private List<Flight> _flights = [];

    public FlightsManager()
    {
        _flights = new List<Flight>();
    }

    public List<Flight> Flights
    {
        get { return _flights; }
        set { _flights = value; }
    }

    internal void AddFlight(string id, string departureAirport, string arrivalAirport, string aircraftModel, double price, double hours)
    {
        _flights.Add(new Flight(id, departureAirport, arrivalAirport, aircraftModel, price, hours));
    }

    internal void SortFlights(string order)
    {
        bool isAscending = string.Equals(order, "ascending") ? true : false;
        if (isAscending)
        {
            _flights = _flights.OrderBy(flight => flight.Id).ToList();

        }
        else
        {
            _flights = _flights.OrderByDescending(flight => flight.Id).ToList();

        }
    }

    internal bool HasFlight(string flightId)
    {
        return _flights.Any(flightObj => flightObj.Id == flightId);
    }

    internal Flight? FlightById(string id)
    {
        if (Flights != null && HasFlight(id))
        {
            return Flights.Find(flight => flight.Id == id);
        }
        return null;
    }

    internal void PrintFlights()
    {
        Console.WriteLine("----------Flights list----------");
        foreach (Flight flight in _flights)
        {
            flight.PrintFlight();
            Console.WriteLine("----------");
        }
    }

    // checks if a flight name contains only alphanumeric characters
    private static bool IsValidFlight(string flight)
    {
        if (flight.Length < 1)
        {
            return false;

        }
        foreach (char ch in flight)
        {
            if (!char.IsLetterOrDigit(ch))
            {

                return false;
            }
        }
        return true;
    }
    internal static bool IsValidNumberRepresentation(string input)
    {
        foreach (char symbol in input)
        {
            if (!char.IsDigit(symbol) && !Equals(symbol, '.'))
            {
                return false;
            }
        }
        return true;
    }
    // Loads flights data from a given file
    // The construct "using" ensures the immediate release of file resources post data processing
    // Throws an exception if the file data is invalid
    internal void LoadFlights(string fileName, AirportsManager airportsManager, AircraftsManager aircraftsManager)//, AirportsManager airportsManager, AircraftsManager aircraftsManager)
    {
        string filePath = $"{Constants.Constants.PATH_TO_FILE}/{fileName}";
        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException("File not found: " + filePath);
        }
        using (StreamReader reader = new StreamReader(filePath))
            while (!reader.EndOfStream)
            {
                string? line = reader.ReadLine();
                if (line == null)
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}! File fields should be of format: \"<flight id>,<departure airport id>,<arrival airport id>,<aircraft model>\"");
                }
                string[] fields = line!.Split(",").Where(field => !string.IsNullOrEmpty(field)).ToArray(); ;
                if (fields.Length != 6)
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}! File fields should be of format: \"<flight id>,<departure airport id>,<arrival airport id>,<aircraft model> <ticket price> <flight duration in hours>\"");
                }
                string flightId = fields[0];
                string departureAirport = fields[1];
                string arrivalAirport = fields[2];
                string model = fields[3];
                string price = fields[4];
                string hours = fields[5];
                if (!IsValidFlight(flightId))
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}!Flight id should be non-empty and contain only alphanumerical symbols!");

                }
                if (!airportsManager.HasAirportById(departureAirport))
                {
                    throw new EntityNotFoundException($"Invalid file data in {fileName}! No airport with id {departureAirport} found!");
                }
                if (!airportsManager.HasAirportById(arrivalAirport))
                {
                    throw new EntityNotFoundException($"Invalid file data in {fileName}! No airport with id {arrivalAirport} found!");
                }
                if (!aircraftsManager.HasAircraft(model))
                {
                    throw new EntityNotFoundException($"Invalid file data in {fileName}! No aircraft of model {model} found!");
                }
                if (!IsValidNumberRepresentation(price))
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}! Ticket price should be a number!");
                }
                if (!IsValidNumberRepresentation(hours))
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}! Flight duration should be a number!");
                }
                AddFlight(flightId, departureAirport, arrivalAirport, model, double.Parse(price), double.Parse(hours));
            }
    }

}
