using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Models;

namespace AirlinesBusiness.Managers;
public class AirportsManager
{
    private Dictionary<string, List<Airport>> _airportsByCountry = [];
    private Dictionary<string, List<Airport>> _airportsByCity = [];

    public AirportsManager()
    {

        _airportsByCountry = new Dictionary<string, List<Airport>>();
        _airportsByCity = new Dictionary<string, List<Airport>>();

    }

    public Dictionary<string, List<Airport>> AirportsByCountry
    {
        get { return _airportsByCountry; }
        set { _airportsByCountry = value; }
    }

    public Dictionary<string, List<Airport>> AirportsByCity
    {
        get { return _airportsByCity; }
        set { _airportsByCity = value; }
    }

    internal void AddAirport(string id, string name, string city, string country)
    {
        Airport airport = new Airport(id, name, city, country);

        // Add to airports by country
        if (!_airportsByCountry.TryGetValue(country, out var airportsInCountry))
        {
            airportsInCountry = new List<Airport>();
            _airportsByCountry[country] = airportsInCountry;
        }
        airportsInCountry.Add(airport);

        // Add to airports by city
        if (!_airportsByCity.TryGetValue(city, out var airportsInCity))
        {
            airportsInCity = new List<Airport>();
            _airportsByCity[city] = airportsInCity;
        }
        airportsInCity.Add(airport);
    }

    // Sorts airports:
    // firstly by country name
    // secondly, for every country it sorts the airports in this coutntry by airport name
    internal void SortAirportsByCountry(string order)
    {
        bool isAcending = string.Equals(order, "ascending") ? true : false;
        if (isAcending)
        {
            _airportsByCountry = _airportsByCountry.OrderBy(keyValuePair => keyValuePair.Key).ToDictionary(keyValuePair => keyValuePair.Key, keyValuePair => keyValuePair.Value);
            foreach (string country in _airportsByCountry.Keys)
            {
                _airportsByCountry[country] = _airportsByCountry[country].OrderBy(airport => airport.Name).ToList();
            }
        }
        else
        {
            _airportsByCountry = _airportsByCountry.OrderByDescending(keyValuePair => keyValuePair.Key).ToDictionary(keyValuePair => keyValuePair.Key, keyValuePair => keyValuePair.Value);
            foreach (string country in _airportsByCountry.Keys)
            {
                _airportsByCountry[country] = _airportsByCountry[country].OrderByDescending(airport => airport.Name).ToList();
            }
        }
    }

    // Sorts airports:
    // firstly by city name
    // secondly, for every city it sorts the airports in this city by airport name
    internal void SortAirportsByCity(string order)
    {
        bool isAcending = string.Equals(order, "ascending") ? true : false;
        if (isAcending)
        {
            _airportsByCity = _airportsByCity.OrderBy(keyValuePair => keyValuePair.Key).ToDictionary(keyValuePair => keyValuePair.Key, keyValuePair => keyValuePair.Value);
            foreach (string city in _airportsByCity.Keys)
            {
                _airportsByCity[city] = _airportsByCity[city].OrderBy(airport => airport.Name).ToList();

            }
        }
        else
        {
            _airportsByCity = _airportsByCity.OrderByDescending(keyValuePair => keyValuePair.Key).ToDictionary(keyValuePair => keyValuePair.Key, keyValuePair => keyValuePair.Value);
            foreach (string city in _airportsByCity.Keys)
            {
                _airportsByCity[city] = _airportsByCity[city].OrderByDescending(airport => airport.Name).ToList();
            }
        }
    }

    // sorts airports both by country and city
    internal void SortAirports(string order)
    {
        SortAirportsByCountry(order);
        SortAirportsByCity(order);
    }

    // checks if an airport exists in any of the airportByCity or airportByCountry dictionaries
    internal bool HasAirportByName(string airport)
    {
        foreach (string country in _airportsByCountry!.Keys)
        {
            if (_airportsByCountry[country].Any(airportObject => airportObject.Name == airport))
            {

                return true;
            }
        }

        foreach (string city in _airportsByCity!.Keys)
        {
            if (_airportsByCity[city].Any(airportObject => airportObject.Name == airport))
            {

                return true;
            }
        }

        return false;
    }
    // checks if an airport exists in any of the airportByCity or airportByCountry dictionaries
    internal bool HasAirportById(string id)
    {
        foreach (string country in _airportsByCountry!.Keys)
        {
            if (_airportsByCountry[country].Any(airportObject => airportObject.Id == id))
            {

                return true;
            }
        }

        foreach (string city in _airportsByCity!.Keys)
        {
            if (_airportsByCity[city].Any(airportObject => airportObject.Id == id))
            {

                return true;
            }
        }

        return false;
    }
    internal Airport? GetAirportById(string id)
    {
        // Search in airports by country
        foreach (string country in _airportsByCountry.Keys)
        {
            Airport? airport = _airportsByCountry[country].FirstOrDefault(a => a.Id == id);
            if (airport != null)
            {
                return airport;
            }
        }

        // Search in airports by city
        foreach (string city in _airportsByCity.Keys)
        {
            Airport? airport = _airportsByCity[city].FirstOrDefault(a => a.Id == id);
            if (airport != null)
            {
                return airport;
            }

        }
        // Airport not found
        return null;
    }


    // returns the list of airports in a country
    internal List<Airport> ListAirportsByCountry(string country)
    {
        return _airportsByCountry.TryGetValue(country, out var value) ? value : new List<Airport>();
    }

    // returns the list of airports in a city
    internal List<Airport> ListAirportsByCity(string city)
    {
        return _airportsByCity.TryGetValue(city, out var value) ? value : new List<Airport>();
    }

    internal void PrintAirports()
    {
        List<Airport> airports = new List<Airport>();
        foreach (string country in _airportsByCountry!.Keys)
        {
            airports = airports.Concat(_airportsByCountry[country]).ToList();
        }

        foreach (string city in _airportsByCity!.Keys)
        {

            airports = airports.Concat(_airportsByCity[city]).ToList();
        }
        airports = airports.DistinctBy(airport => airport.Id).ToList();
        Console.WriteLine("----------Airports list----------");
        foreach (Airport airport in airports)
        {
            Console.WriteLine("-----------");
            airport.PrintAirport();
        }
    }

    private static bool IsValidId(string id)
    {
        if (id.Length < 2 || id.Length > 4)
        {
            return false;

        }
        foreach (char symbol in id)
        {
            if (!char.IsLetterOrDigit(symbol))
            {

                return false;
            }
        }

        return true;
    }

    private static bool IsValidAirportData(string data)
    {
        string[] splittedData = data.Split(" ");
        foreach (string word in splittedData)
        {
            foreach (char symbol in word)
            {
                if (!char.IsLetter(symbol))
                {

                    return false;
                }
            }

        }

        return true;
    }

    // Loads airports data from a given file
    // The construct "using" ensures the immediate release of file resources post data processing
    // Throws an exception if the file data is invalid
    internal void LoadAirports(string fileName)
    {
        string filePath = $"{Constants.Constants.PATH_TO_FILE}/{fileName}";
        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException("File not found: " + filePath);
        }
        using (StreamReader reader = new StreamReader(filePath))
            while (!reader.EndOfStream)
            {
                string? line = reader.ReadLine();
                if (line == null)
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}! File fields should be of format:  \"<airport id>,<airport name>,<city>,<country>\"");
                }
                string[] fields = line!.Split(",").Where(field => !string.IsNullOrEmpty(field)).ToArray();
                if (fields.Length != 4)
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}! File fields should be of format:  \"<airport id>,<airport name>,<city>,<country>\"");
                }
                string id = fields[0];
                string name = fields[1];
                string city = fields[2];
                string country = fields[3];
                if (!IsValidId(id) || !IsValidAirportData(name) || !IsValidAirportData(city) || !IsValidAirportData(country))
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}!\nAirport id should be of length 2-4 and contain only alphanumerical symbols!\nAirport name, city and country should be non-empty and contain only alphabetical symbols!");
                }
                AddAirport(id, name, city, country);
            }
    }

}

