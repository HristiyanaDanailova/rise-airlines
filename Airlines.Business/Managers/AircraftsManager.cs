﻿using AirlinesBusiness.Models.Aircraft;
using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Constants;

namespace AirlinesBusiness.Managers;
public class AircraftsManager
{
    private List<Aircraft> _aircrafts = [];

    public AircraftsManager()
    {
        Aircrafts = new List<Aircraft>();
    }

    public List<Aircraft> Aircrafts
    {
        get { return _aircrafts; }
        set { _aircrafts = value; }
    }

    internal void AddAircraft(Aircraft aircraft)
    {
        _aircrafts.Add(aircraft);
    }

    internal bool HasAircraft(string model)
    {
        return _aircrafts.Any(aircraft => string.Equals(aircraft.Model, model));
    }

    internal CargoAircraft? CargoAircraftByModel(string model)
    {
        return _aircrafts.FirstOrDefault(aircraft => aircraft.Model == model) as CargoAircraft;

    }
    internal PassengerAircraft? PassengerAircraftByModel(string model)
    {
        return _aircrafts.FirstOrDefault(aircraft => aircraft.Model == model) as PassengerAircraft;

    }

    internal static bool IsValidNumberRepresentation(string input)
    {
        foreach (char symbol in input)
        {
            if (!char.IsDigit(symbol) && !Equals(symbol, '.'))
            {
                return false;
            }
        }
        return true;
    }

    internal static string GetAircraftType(string model, string cargoWeight, string cargoVolume, string seats)
    {
        if (string.IsNullOrEmpty(model) || (string.IsNullOrEmpty(cargoWeight) && string.IsNullOrEmpty(cargoVolume) && string.IsNullOrEmpty(seats)))
        {
            return "invalid";
        }
        if ((cargoVolume != "-" && !IsValidNumberRepresentation(cargoVolume)) ||
            (cargoWeight != "-" && !IsValidNumberRepresentation(cargoWeight)) ||
            (seats != "-" && !IsValidNumberRepresentation(seats)))
        {
            return "invalid";
        }
        if (seats == "-" && model != "-" && cargoWeight != "-" && cargoVolume != "-")
        {
            return "cargo";
        }
        if (cargoWeight == "-" && cargoVolume == "-" && model != "-" && seats != "-")
        {
            return "private";

        }
        if (model != "-" && cargoWeight != "-" && cargoVolume != "-" && seats != "-")
        {
            return "passenger";

        }
        return "invalid";
    }

    // Loads aircrafts data from a given file
    // The construct "using" ensures the immediate release of file resources post data processing
    // Throws an exception if the file data is invalid
    internal void LoadAircrafts(string fileName)
    {
        string filePath = $"{Constants.Constants.PATH_TO_FILE}/{fileName}";
        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException("File not found: " + filePath);
        }
        using (StreamReader reader = new StreamReader(filePath))
            while (!reader.EndOfStream)
            {
                string? line = reader.ReadLine();
                if (line == null)
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}! File fields should be of format: \"<aircraft model>,<load>,<volume>,<seats>\"");
                }
                string[] fields = line!.Split(",").Where(field => !string.IsNullOrEmpty(field)).ToArray();
                if (fields.Length != 4)
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}! File fields should be of format: \"<aircraft model>,<load>,<volume>,<seats>\"");
                }
                string model = fields[0];
                string cargoWeight = fields[1];
                string cargoVolume = fields[2];
                string seats = fields[3];
                string type = GetAircraftType(model, cargoWeight, cargoVolume, seats);
                if (string.Equals(type, "cargo"))
                {
                    CargoAircraft cargoAircraft = new CargoAircraft(model, uint.Parse(cargoWeight), double.Parse(cargoVolume));
                    AddAircraft(cargoAircraft);
                }
                else if (string.Equals(type, "passenger"))
                {
                    PassengerAircraft passengerAircraft = new PassengerAircraft(model, uint.Parse(cargoWeight), double.Parse(cargoVolume), uint.Parse(seats));
                    AddAircraft(passengerAircraft);
                }
                else if (string.Equals(type, "private"))
                {
                    PrivateAircraft privateAircraft = new PrivateAircraft(model, uint.Parse(seats));
                    AddAircraft(privateAircraft);
                }
                else
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}! File fields should be of format: \"<aircraft model>,<load>,<volume>,<seats>\"");
                }
            }
    }
}
