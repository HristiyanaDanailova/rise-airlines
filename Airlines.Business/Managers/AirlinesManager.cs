using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Models;

namespace AirlinesBusiness.Managers;
public class AirlinesManager
{
    private Dictionary<string, Airline> _airlines = [];

    public AirlinesManager()
    {
        _airlines = new Dictionary<string, Airline>();
    }

    public Dictionary<string, Airline> Airlines
    {
        get { return _airlines; }
        set { _airlines = value; }
    }

    internal void AddAirline(string airline)
    {
        if (!_airlines.ContainsKey(airline))
        {

            _airlines.Add(airline, new Airline(airline));
        }
    }

    // Sorts airlines by airline names
    internal void SortAirlines(string order)
    {
        bool isAcending = string.Equals(order, "ascending") ? true : false;
        if (isAcending)
        {
            _airlines = _airlines.OrderBy(keyValuePair => keyValuePair.Key).ToDictionary(keyValuePair => keyValuePair.Key, keyValuePair => keyValuePair.Value);

        }
        else
        {
            _airlines = _airlines.OrderByDescending(keyValuePair => keyValuePair.Key).ToDictionary(keyValuePair => keyValuePair.Key, keyValuePair => keyValuePair.Value);

        }
    }

    internal bool HasAirline(string airline)
    {
        return _airlines.ContainsKey(airline);
    }

    internal void PrintAirlines()
    {
        Console.WriteLine("----------Airlines list---------- ");
        foreach (Airline airline in _airlines!.Values)
        {
            airline.PrintAirline();
            Console.WriteLine("-----------");
        }
    }
    private static bool IsValidAirlineName(string airline)
    {
        if (airline.Length >= 6 || airline.Length < 1)
        {
            return false;
        }
        return true;
    }

    // Loads airlines data from a given file
    // The construct "using" ensures the immediate release of file resources post data processing
    // Throws an exception if the file data is invalid
    internal void LoadAirlines(string fileName)
    {
        string filePath = $"{Constants.Constants.PATH_TO_FILE}/{fileName}";
        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException("File not found: " + filePath);
        }
        using (StreamReader reader = new StreamReader(filePath))
            while (!reader.EndOfStream)
            {
                string? line = reader.ReadLine();
                if (line == null)
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}! File fields should be of format: \"<airline name>\"");
                }
                if (!IsValidAirlineName(line!.Trim()))
                {
                    throw new InvalidFileDataException($"Invalid file data in {fileName}! Airline names should be non-empty and contain no more that 6 symbols");
                }
                AddAirline(line);
            }
    }
}
