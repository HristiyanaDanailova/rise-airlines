namespace AirlinesBusiness.SearchingNS;
public class Searcher
{
    internal static bool BinarySearch(string[] array, int arraySize, string target)
    {
        int left = 0;
        int right = arraySize - 1;
        while (left <= right)
        {
            int middle = left + (right - left) / 2;
            if (string.Equals(array[middle], target))
            {
                return true;
            }
            if (string.Compare(target, array[middle]) == 1)
            {
                left = middle + 1;
            }
            else
            {
                right = middle - 1;
            }
        }
        return false;
    }

    internal static bool LinearSearch(string input, string[] array, int arraySize)
    {
        for (int i = 0; i < arraySize; i++)
        {
            if (array[i].Equals(input))
            {
                return true;
            }
        }
        return false;
    }
}
