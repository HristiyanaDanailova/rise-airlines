﻿using System.Collections;

namespace AirlinesBusiness.DataStructures;
public class ListNode<T>
{
    private T? _data;
    private ListNode<T>? _next;

    public ListNode(T? data, ListNode<T>? next = null)
    {
        Data = data;
        Next = next;
    }

    public T? Data
    {
        get { return _data; }
        set { _data = value; }
    }
    public ListNode<T>? Next
    {
        get { return _next; }
        set { _next = value; }

    }
}
public class SingleLinkedList<T> : IEnumerable<T>
{
    private ListNode<T>? _head;

    public SingleLinkedList()
    {
        _head = null;
    }
    public ListNode<T>? Head => _head;
    internal void AddLast(T data)
    {
        if (_head == null)
        {
            _head = new ListNode<T>(data);
            return;
        }

        var current = _head;
        while (current.Next != null)
        {
            current = current.Next;
        }

        current.Next = new ListNode<T>(data);
    }

    internal void RemoveLast()
    {
        if (_head == null)
        {
            return;
        }

        if (_head.Next == null)
        {
            _head = null;
            return;
        }

        var current = _head;
        while (current.Next!.Next != null)
        {
            current = current.Next;
        }

        current.Next = null;
    }
    internal T? GetLast()
    {
        if (_head == null)
        {
            return default;
        }

        var current = _head;
        while (current.Next != null)
        {
            current = current.Next;
        }

        return current.Data;
    }

    internal void Clear()
    {
        _head = null;
    }

    internal bool IsEmpty()
    {
        return _head == null;
    }

    internal bool HasElement(T element)
    {
        var current = _head;
        while (current != null)
        {
            if (EqualityComparer<T>.Default.Equals(current.Data, element))
            {
                return true;
            }
            current = current.Next;
        }
        return false;
    }

    public IEnumerator<T> GetEnumerator()
    {
        var current = _head;
        while (current != null)
        {
            yield return current.Data!;
            current = current.Next;
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}
