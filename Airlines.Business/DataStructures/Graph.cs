﻿namespace AirlinesBusiness.DataStructures;

public class Neighbour<T>
{
    private T _neighbourData;
    private double _weightByPrice;
    private double _weightByHours;

    public Neighbour(T neighbourData, double weightByPrice, double weightByHours)
    {
        _neighbourData = neighbourData;
        _weightByPrice = weightByPrice;
        _weightByHours = weightByHours;
    }

    public T NeighbourData
    {
        get { return _neighbourData; }
        set { _neighbourData = value; }
    }
    public double WeightByPrice
    {
        get { return _weightByPrice; }
        set { _weightByPrice = value; }
    }
    public double WeightByHours
    {
        get { return _weightByHours; }
        set { _weightByHours = value; }
    }
}
public class GraphNode<T>
{
    private T _data;
    private List<Neighbour<T>> _neighbours;

    public GraphNode(T data)
    {
        _data = data;
        _neighbours = new List<Neighbour<T>>();
    }

    public T Data
    {
        get { return _data; }
        set { _data = value; }
    }

    public List<Neighbour<T>> Neighbours
    {
        get { return _neighbours; }
        set { _neighbours = value; }
    }

}
public class Graph<T>
{
    private List<GraphNode<T>> _graphNodes;

    public Graph()
    {
        _graphNodes = new List<GraphNode<T>>();
    }

    public List<GraphNode<T>> GraphNodes
    {
        get { return _graphNodes; }
        set { _graphNodes = value; }
    }

    internal void AddVertex(T data)
    {
        if (!ContainsNode(data))
        {
            GraphNode<T> node = new GraphNode<T>(data);
            _graphNodes.Add(node);
        }
    }
    internal void AddEdge(T source, T destination, double weightByPrice, double weightByHours)
    {
        GraphNode<T>? sourceNode = _graphNodes.FirstOrDefault(node => EqualityComparer<T>.Default.Equals(node.Data, source));
        if (sourceNode != null)
        {
            if (!sourceNode.Neighbours.Any(neighbour => EqualityComparer<T>.Default.Equals(neighbour.NeighbourData, destination)))
            {
                sourceNode.Neighbours.Add(new Neighbour<T>(destination, weightByPrice, weightByHours));
            }
        }
    }
    internal void PrintGraph(Func<T, string> printFunction)
    {
        foreach (GraphNode<T> node in _graphNodes)
        {
            Console.Write($"Node: {printFunction(node.Data)}");

            Console.Write(" -> ");
            string neighboursString = string.Join(", ", node.Neighbours.Select(neighbour => printFunction(neighbour.NeighbourData)));
            Console.WriteLine(neighboursString);
        }
    }

    internal GraphNode<T> GetNode(T data)
    {
        return _graphNodes.First(node => EqualityComparer<T>.Default.Equals(node.Data, data));
    }
    internal bool ContainsNode(T data)
    {
        return _graphNodes.Any(node => EqualityComparer<T>.Default.Equals(node.Data, data));
    }
}