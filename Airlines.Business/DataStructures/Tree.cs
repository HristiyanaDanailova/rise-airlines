﻿namespace AirlinesBusiness.DataStructures;
public class TreeNode<T>
{
    private T? _data;
    private List<TreeNode<T>>? _children;

    public TreeNode(T? data, List<TreeNode<T>>? children = null)
    {
        _data = data;
        _children = children ?? new List<TreeNode<T>>();
    }
    public TreeNode()
    {
        _children = new List<TreeNode<T>>();
    }

    public T? Data
    {
        get { return _data; }
        set { _data = value; }
    }
    public List<TreeNode<T>>? Children
    {
        get { return _children; }
        set { _children = value; }
    }
    internal void AddChild(TreeNode<T> child)
    {
        _children?.Add(child);
    }
}
public class Tree<T>
{
    private TreeNode<T> _root;

    public Tree(T? rootValue)
    {
        _root = new TreeNode<T>(rootValue);

    }
    public Tree()
    {
        _root = new TreeNode<T>();

    }
    public TreeNode<T> Root
    {
        get { return _root; }
        set { _root = value; }
    }

    internal void PrintTree(Func<T, string> printFunction)
    {
        PrintTree(_root, 0, printFunction);
    }

    // Prints the tree with identation at each level, where printFunction is recieved as an argument
    private static void PrintTree(TreeNode<T>? node, int depth, Func<T, string> printFunction)
    {
        if (node == null)
        {
            return;
        }

        Console.WriteLine(new string(' ', depth * 2) + printFunction(node.Data!));

        if (node.Children != null)
        {
            foreach (var child in node.Children)
            {
                PrintTree(child, depth + 1, printFunction);
            }
        }
    }

    internal static List<TreeNode<T>> FindRoute(T destination, TreeNode<T> currentNode)
    {
        // If currentNode is the destination, a list is returned
        if (currentNode.Data!.Equals(destination))
        {
            return new List<TreeNode<T>> { currentNode };
        }

        if (currentNode.Children != null)
        {
            foreach (var child in currentNode.Children)
            {
                // Searches for route in children
                var route = FindRoute(destination, child);
                // If a route is found, the successor of the node gets added to the list at the front
                if (route.Count > 0)
                {
                    route.Insert(0, currentNode);
                    return route;
                }
            }
        }
        return new List<TreeNode<T>>();
    }

}
