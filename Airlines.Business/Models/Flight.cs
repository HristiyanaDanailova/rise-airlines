﻿namespace AirlinesBusiness.Models;
public class Flight : IEquatable<Flight>
{
    private string _id = string.Empty;
    private string _departureAirport = string.Empty;
    private string _arrivalAirport = string.Empty;
    private string _aircraftModel = string.Empty;
    private double _price;
    private double _hours;

    public Flight(string id, string departureAirport, string arrivalAirport, string aircraftModel, double price, double hours)
    {
        _id = id;
        _departureAirport = departureAirport;
        _arrivalAirport = arrivalAirport;
        _aircraftModel = aircraftModel;
        Price = price;
        Hours = hours;
    }

    public string Id
    {
        get { return _id; }
        set { _id = value; }
    }

    public string DepartureAirport
    {
        get { return _departureAirport; }
        set { _departureAirport = value; }
    }

    public string ArrivalAirport
    {
        get { return _arrivalAirport; }
        set { _arrivalAirport = value; }
    }
    public string AircraftModel
    {
        get { return _aircraftModel; }
        set { _aircraftModel = value; }
    }
    public double Price
    {
        get { return _price; }
        set
        {
            if (value < 0)
            {
                _price = 0;
            }
            else
            {
                _price = value;
            }
        }
    }
    public double Hours
    {
        get { return _hours; }
        set
        {
            if (value < 0)
            {
                _hours = 0;
            }
            else
            {
                _hours = value;
            }
        }
    }

    public bool Equals(Flight? other)
    {
        if (other == null || GetType() != other.GetType())
        {
            return false;
        }
        return _id == other.Id;
    }

    internal void PrintFlight()
    {
        Console.WriteLine($"Flight with id {_id}: ");
        Console.WriteLine($"Departure airport: {_departureAirport}");
        Console.WriteLine($"Arrival airport: {_arrivalAirport}");
        Console.WriteLine($"Aircraft model: {_aircraftModel}");
        Console.WriteLine($"Ticket price: {_price}");
        Console.WriteLine($"Flight duration in hours: {_hours}");
    }
}
