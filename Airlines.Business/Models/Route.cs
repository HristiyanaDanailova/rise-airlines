﻿using AirlinesBusiness.DataStructures;

namespace AirlinesBusiness.Models;
public class Route
{
    private SingleLinkedList<Flight> _flightRoute;

    public Route()
    {
        _flightRoute = new SingleLinkedList<Flight>();
    }

    public SingleLinkedList<Flight> FlightRoute
    {
        get { return _flightRoute; }
        set { _flightRoute = value; }
    }

    internal bool AddFlight(Flight flight)
    {
        if (IsRouteEmpty())
        {
            _flightRoute.AddLast(flight);
            return true;
        }
        else
        {
            Flight? routeLastFlight = LastFlight();
            if (IsValidRouteConnection(routeLastFlight!, flight))
            {
                _flightRoute.AddLast(flight);
                return true;
            }
        }
        return false;
    }

    internal void RemoveFlightAtEnd()
    {
        _flightRoute.RemoveLast();
    }

    internal void PrintRoute()
    {
        Console.Write("Route: ");
        ListNode<Flight>? currentFlightNode = _flightRoute.Head;
        if (currentFlightNode == null)
        {
            Console.WriteLine("Route is empty.");
        }
        else
        {
            while (currentFlightNode != null)
            {
                Flight flight = currentFlightNode.Data!;
                Console.Write($"{flight.DepartureAirport}");
                if (currentFlightNode.Next != null)
                {

                    Console.Write(" -> ");
                }
                else
                {

                    // Fot the last flight, the arrival airport gets printed
                    Console.Write($" -> {flight.ArrivalAirport}");
                }
                currentFlightNode = currentFlightNode.Next;
            }
            Console.WriteLine();
        }

    }

    internal static bool IsValidRouteConnection(Flight flight1, Flight flight2)
    {
        return string.Equals(flight1.ArrivalAirport, flight2.DepartureAirport);
    }

    internal void EmptyRoute()
    {
        _flightRoute.Clear();
    }

    internal bool IsRouteEmpty()
    {
        return _flightRoute.IsEmpty();
    }

    internal Flight? LastFlight()
    {
        return _flightRoute.GetLast();
    }

    internal bool HasFlight(Flight flight)
    {
        return _flightRoute.HasElement(flight);
    }
}
