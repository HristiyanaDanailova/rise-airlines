﻿namespace AirlinesBusiness.Models;

public class Airport : IEquatable<Airport>
{
    private string _id = string.Empty;
    private string _name = string.Empty;
    private string _country = string.Empty;
    private string _city = string.Empty;

    public Airport(string id, string name, string city, string country)
    {
        _id = id;
        _name = name;
        _country = country;
        _city = city;
    }

    public string Id
    {
        get { return _id; }
        set { _id = value; }
    }

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public string Country
    {
        get { return _country; }
        set { _country = value; }
    }

    public string City
    {
        get { return _city; }
        set { _city = value; }
    }
    internal void PrintAirport()
    {
        Console.WriteLine($"Airport with identifier {_id}: ");
        Console.WriteLine($"Name: {_name}");
        Console.WriteLine($"Country: {_country}");
        Console.WriteLine($"City: {_city}");
    }
    public bool Equals(Airport? other)
    {
        if (other == null || GetType() != other.GetType())
        {
            return false;
        }
        return _id == other.Id && _name == other.Name && _city == other.City && _country == other.Country;
    }
}
