﻿namespace AirlinesBusiness.Models;

public class Airline
{
    private string _name = string.Empty;

    public Airline(string name)
    {
        _name = name;
    }
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }
    internal void PrintAirline()
    {
        Console.WriteLine($"Airline name: {_name}");
    }
}

