﻿namespace AirlinesBusiness.Models.Aircraft;
public class PrivateAircraft : Aircraft
{
    private uint _seats;
    public PrivateAircraft(string model, uint seats) : base(model)
    {
        _seats = seats;
    }

    public uint Seats
    {
        get { return _seats; }
        set
        {
            if (value < 0)
                _seats = 0;
            else
                _seats = value;
        }
    }
}
