﻿namespace AirlinesBusiness.Models.Aircraft;
public abstract class Aircraft
{
    protected string _model = string.Empty;
    public Aircraft(string model)
    {
        _model = model;
    }
    public string Model
    {
        get { return _model; }
        set { _model = value; }
    }
}
