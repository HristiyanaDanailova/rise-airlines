﻿namespace AirlinesBusiness.Models.Aircraft;
public class PassengerAircraft : Aircraft
{
    const uint MAXIMUM_WEIGHT_SMALL_BAGGAGE = 15;
    const double MAXIMUM_VOLUME_SMALL_BAGGAGE = 0.045;
    const uint MAXIMUM_WEIGHT_LARGE_BAGGAGE = 30;
    const double MAXIMUM_VOLUME_LARGE_BAGGAGE = 0.090;

    private uint _cargoLoad;
    private double _cargoVolume;
    private uint _seats;

    public PassengerAircraft(string model, uint cargoLoad, double cargoVolume, uint seats) : base(model)
    {
        _cargoLoad = cargoLoad;
        _cargoVolume = cargoVolume;
        _seats = seats;
    }

    public uint CargoLoad
    {
        get { return _cargoLoad; }
        set
        {
            if (value < 0)
                _cargoLoad = 0;
            else
                _cargoLoad = value;
        }
    }
    public double CargoVolume
    {
        get { return _cargoVolume; }
        set
        {
            if (value < 0)
                _cargoVolume = 0;
            else
                _cargoVolume = value;
        }
    }

    public uint Seats
    {
        get { return _seats; }
        set
        {
            if (value < 0)
                _seats = 0;
            else
                _seats = value;
        }
    }

    internal bool CanBeReserved(uint seats, uint smallBaggageCount, uint largeBaggageCount)
    {
        return seats <= _seats
                && smallBaggageCount * MAXIMUM_WEIGHT_SMALL_BAGGAGE + largeBaggageCount * MAXIMUM_WEIGHT_LARGE_BAGGAGE <= _cargoLoad
                && smallBaggageCount * MAXIMUM_VOLUME_SMALL_BAGGAGE + largeBaggageCount * MAXIMUM_VOLUME_LARGE_BAGGAGE <= _cargoVolume;

    }
}
