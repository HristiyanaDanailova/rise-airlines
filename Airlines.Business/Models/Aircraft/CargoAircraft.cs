﻿namespace AirlinesBusiness.Models.Aircraft;
public class CargoAircraft : Aircraft
{
    private uint _cargoLoad;
    private double _cargoVolume;

    public CargoAircraft(string model, uint cargoLoad, double cargoVolume) : base(model)
    {
        _cargoLoad = cargoLoad;
        _cargoVolume = cargoVolume;
    }

    public uint CargoLoad
    {
        get { return _cargoLoad; }
        set
        {
            if (value < 0)
                _cargoLoad = 0;
            else
                _cargoLoad = value;
        }
    }

    public double CargoVolume
    {
        get { return _cargoVolume; }
        set
        {
            if (value < 0)
                _cargoVolume = 0;
            else
                _cargoVolume = value;
        }
    }

    internal bool CanBeReserved(uint cargoWeight, double cargoVolume)
    {
        return cargoWeight <= _cargoLoad && cargoVolume <= _cargoVolume;
    }
}
