﻿using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.DataStructures;
using AirlinesBusiness.Managers;
using AirlinesBusiness.Models.SearchStrategy;
namespace AirlinesBusiness.Models;
public class FlightsNetwork
{
    Graph<Airport> _flightNetwork;
    private ISearchStrategy<Airport>? _searchStrategy;

    public FlightsNetwork()
    {
        _flightNetwork = new Graph<Airport>();
    }

    public Graph<Airport> FlightNetwork
    {
        get { return _flightNetwork; }
        set { _flightNetwork = value; }
    }

    public ISearchStrategy<Airport>? SearchStrategy
    {
        get { return _searchStrategy; }
        set { _searchStrategy = value; }
    }

    internal void PrintFlightNetwork()
    {
        _flightNetwork.PrintGraph(airport => airport.Id);
    }

    public void ConnectFlightsNetwork(FlightsManager flightsManager, AirportsManager airportsManager)
    {
        ConnectFlightsNetwork(flightsManager, airportsManager, _flightNetwork.GraphNodes[0], []);
    }

    private void ConnectFlightsNetwork(FlightsManager flightsManager, AirportsManager airportsManager, GraphNode<Airport>? node, HashSet<Airport> visited)
    {
        if (node == null)
        {
            return;
        }
        if (visited.Contains(node.Data))
        {
            return;
        }
        visited.Add(node.Data);
        foreach (Airport airport in node.Neighbours.Select(neighbour => neighbour.NeighbourData))
        {
            _flightNetwork.AddVertex(airport);

            // FLights departuring from this airport
            List<Flight> flights = flightsManager.Flights.FindAll(flight => flight.DepartureAirport == airport.Id);
            foreach (Flight flight in flights)
            {
                // The "edge" will be added between the airport and the arrival airport
                Airport? arrivalAirport = airportsManager.GetAirportById(flight.ArrivalAirport);
                if (arrivalAirport != null)
                {
                    _flightNetwork.AddEdge(airport, arrivalAirport, flight.Price, flight.Hours);
                }
                ConnectFlightsNetwork(flightsManager, airportsManager, _flightNetwork.GetNode(airport), visited);
            }
        }
    }

    internal bool AreAirportsConnected(Airport startAirport, Airport endAirport)
    {
        if (!_flightNetwork.ContainsNode(startAirport) || !_flightNetwork.ContainsNode(endAirport))
        {
            return false;
        }
        return AreAirportsConnectedRecursive(startAirport, endAirport, []);
    }

    private bool AreAirportsConnectedRecursive(Airport currentAirport, Airport endAirport, List<Airport> visited)
    {
        if (currentAirport == endAirport)
        {
            return true;
        }

        visited.Add(currentAirport);
        GraphNode<Airport>? node = _flightNetwork.GetNode(currentAirport);
        if (node == null)
        {
            return false;
        }
        foreach (Airport neighbour in node.Neighbours.Select(neighbour => neighbour.NeighbourData))
        {
            if (!visited.Contains(neighbour))
            {
                if (AreAirportsConnectedRecursive(neighbour, endAirport, visited))
                {
                    return true;
                }
            }
        }
        return false;
    }

    internal static void PrintPath(List<Airport> path)
    {
        if (path.Count == 0)
        {
            Console.WriteLine("No path found.");
            return;
        }

        Console.Write(path[0].Id);

        for (int i = 1; i < path.Count; i++)
        {
            Console.Write(" -> " + path[i].Id);
        }

        Console.WriteLine();
    }

    internal List<Airport> FindRoute(Airport startAirport, Airport endAirport, string strategy)
    {
        if (string.Equals(strategy, "cheap"))
        {
            _searchStrategy = new CheapRouteStrategy();
        }
        else if (string.Equals(strategy, "short"))
        {
            _searchStrategy = new ShortRouteStrategy();
        }
        else
        {
            _searchStrategy = new StopsSearchStrategy();
        }
        return _searchStrategy.FindRoute(_flightNetwork, startAirport, endAirport);
    }

    internal void LoadFlightNetwork(string fileName, FlightsManager flightsManager, AirportsManager airportsManager)
    {
        var filePath = $"{Constants.Constants.PATH_TO_FILE}/{fileName}";
        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException("File not found: " + filePath);
        }
        using (StreamReader reader = new StreamReader(filePath))
        {
            var line = reader.ReadLine();
            if (line == null)
            {
                throw new InvalidFileDataException("Invalid file data! Make sure the provided starting airport name is valid!");
            }
            var airport = airportsManager.GetAirportById(line);
            if (airport == null)
            {
                throw new EntityNotFoundException("Starting airport not found!");
            }
            _flightNetwork.AddVertex(airport);
            while (!reader.EndOfStream)
            {
                line = reader.ReadLine();
                if (line == null)
                {
                    throw new InvalidFileDataException("Invalid file data! Make sure the provided flight id is valid!");
                }
                var flight = flightsManager.FlightById(line);
                if (flight == null)
                {
                    throw new EntityNotFoundException("No flight with such id was found!");
                }
                var arrivalAirport = airportsManager.GetAirportById(flight.ArrivalAirport);
                if (arrivalAirport == null)
                {
                    throw new EntityNotFoundException("Invalid flight data");
                }
                _flightNetwork.AddEdge(airport, arrivalAirport, flight.Price, flight.Hours);

            }

        }
    }
}
