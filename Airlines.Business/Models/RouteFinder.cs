﻿using AirlinesBusiness.Managers;
using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.DataStructures;

namespace AirlinesBusiness.Models;
public class RouteFinder
{
    private Tree<Airport> _routes;

    public RouteFinder()
    {
        _routes = new Tree<Airport>();
    }

    public Tree<Airport> Routes
    {
        get { return _routes; }
    }

    // creates the tree route connections, starting from level 1 (already loaded from file)
#pragma warning disable CA1822 // Mark members as static
    public void ConnectRoutes(FlightsManager flightsManager, AirportsManager airportsManager, TreeNode<Airport> currentNode)
#pragma warning restore CA1822 // Mark members as static
    {
        foreach (TreeNode<Airport> child in currentNode.Children!)
        {
            var flight = flightsManager.Flights.FirstOrDefault(f => f.DepartureAirport == child.Data!.Id);
            if (flight != null)
            {
                var airport = airportsManager.GetAirportById(flight.ArrivalAirport);
                if (airport == null)
                {

                    throw new EntityNotFoundException("Invalid airport data detected! Check your file data!");
                }
                child.AddChild(new TreeNode<Airport>(airport));
                ConnectRoutes(flightsManager, airportsManager, child);
            }
        }
    }

    // Loads routes data from a given file
    // The construct "using" ensures the immediate release of file resources post data processing
    // Throws an exception if the airport or flight identifiers don't exist
    internal void LoadRoutes(string fileName, FlightsManager flightsManager, AirportsManager airportsManager)
    {
        string filePath = $"{Constants.Constants.PATH_TO_FILE}/{fileName}";
        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException("File not found: " + filePath);
        }
        using (StreamReader reader = new StreamReader(filePath))
        {
            string? line = reader.ReadLine();
            if (line == null)
            {
                throw new InvalidFileDataException("Invalid file data! Make sure the provided starting airport name is valid!");
            }
            Airport? airport = airportsManager.GetAirportById(line);
            if (airport == null)
            {
                throw new EntityNotFoundException("Starting airport not found!");
            }
            _routes.Root.Data = airport;
            while (!reader.EndOfStream)
            {
                line = reader.ReadLine();
                if (line == null)
                {
                    throw new InvalidFileDataException("Invalid file data! Make sure the provided flight id is valid!");
                }
                var flight = flightsManager.FlightById(line);
                if (flight == null)
                {
                    throw new EntityNotFoundException("No flight with such id was found!");
                }
                var arrivalAirport = airportsManager.GetAirportById(flight.ArrivalAirport);
                if (arrivalAirport == null)
                {
                    throw new EntityNotFoundException("Invalid flight data");
                }
                _routes.Root.AddChild(new TreeNode<Airport>(arrivalAirport));

            }
        }

    }

    internal void PrintRoute()
    {
        _routes.PrintTree(node => node.Id);
    }

    // returns a list of nodes of the found route
    internal List<TreeNode<Airport>> FindFlightRoute(Airport airport)
    {
        return Tree<Airport>.FindRoute(airport, _routes.Root);
    }

}
