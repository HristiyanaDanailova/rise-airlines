﻿using AirlinesBusiness.DataStructures;

namespace AirlinesBusiness.Models.SearchStrategy;
public class ShortRouteStrategy : ISearchStrategy<Airport>
{
    public List<Airport> FindRoute(Graph<Airport> flightNetwork, Airport startAirport, Airport endAirport)
    {
        Dictionary<Airport, double> distances = new Dictionary<Airport, double>();
        Dictionary<Airport, Airport> parents = new Dictionary<Airport, Airport>();
        List<Airport> visited = new List<Airport>();

        foreach (GraphNode<Airport> node in flightNetwork.GraphNodes)
        {
            distances[node.Data] = double.MaxValue;
        }

        distances[startAirport] = 0;

        while (true)
        {
            Airport? currentNode = null;
            double minDistance = double.MaxValue;

            foreach (GraphNode<Airport> node in flightNetwork.GraphNodes)
            {
                if (!visited.Contains(node.Data) && distances[node.Data] < minDistance)
                {
                    minDistance = distances[node.Data];
                    currentNode = node.Data;
                }
            }

            if (currentNode == null)
            {
                break; // No more reachable nodes
            }

            visited.Add(currentNode);

            if (currentNode.Equals(endAirport))
            {
                break; // Reached the destination node
            }

            double currentDistance = distances[currentNode];

            foreach (Neighbour<Airport> neighbor in flightNetwork.GetNode(currentNode)!.Neighbours)
            {
                Airport neighborNode = neighbor.NeighbourData;
                if (visited.Contains(neighborNode))
                {
                    continue;
                }

                double weightByHours = neighbor.WeightByHours;
                double distanceToNeighbor = currentDistance + weightByHours;

                if (distanceToNeighbor < distances[neighborNode])
                {
                    distances[neighborNode] = distanceToNeighbor;
                    parents[neighborNode] = currentNode;
                }
            }
        }

        // Reconstruct the shortest path
        List<Airport> shortestPath = new List<Airport>();
        Airport currentNodePath = endAirport;

        // Check if the current airport has a parent in the parents dictionary
        while (currentNodePath != null && !currentNodePath.Equals(startAirport))
        {
            shortestPath.Insert(0, currentNodePath);
            if (parents.TryGetValue(currentNodePath, out Airport? value))
            {
                currentNodePath = value;
            }
            else
            {
                shortestPath.Clear();
                break; // No path exists
            }
        }

        // If no path exists, return an empty list
        if (shortestPath.Count == 0)
        {
            return new List<Airport>();
        }

        shortestPath.Insert(0, startAirport);
        return shortestPath;
    }

}
