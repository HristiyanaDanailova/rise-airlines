﻿
using AirlinesBusiness.DataStructures;

namespace AirlinesBusiness.Models.SearchStrategy;
public class CheapRouteStrategy : ISearchStrategy<Airport>
{
    public List<Airport> FindRoute(Graph<Airport> flightNetwork, Airport startAirport, Airport endAirport)
    {
        Dictionary<Airport, double> prices = new Dictionary<Airport, double>();
        Dictionary<Airport, Airport> parents = new Dictionary<Airport, Airport>();
        List<Airport> visited = new List<Airport>();

        foreach (GraphNode<Airport> node in flightNetwork.GraphNodes)
        {
            prices[node.Data] = double.MaxValue;
        }

        prices[startAirport] = 0;

        while (true)
        {
            Airport? currentAirport = default(Airport);
            double minPrice = double.MaxValue;

            foreach (GraphNode<Airport> node in flightNetwork.GraphNodes)
            {
                if (!visited.Contains(node.Data) && prices[node.Data] < minPrice)
                {
                    minPrice = prices[node.Data];
                    currentAirport = node.Data;
                }
            }

            if (currentAirport == null)
            {
                break; // No more reachable nodes
            }

            visited.Add(currentAirport);

            if (currentAirport.Equals(endAirport))
            {
                break; // Reached the destination node
            }

            double currentPrice = prices[currentAirport];

            foreach (Neighbour<Airport> neighbor in flightNetwork.GetNode(currentAirport).Neighbours)
            {
                Airport neighborAirport = neighbor.NeighbourData;
                if (visited.Contains(neighborAirport))
                {
                    continue;
                }

                double weightByPrice = neighbor.WeightByPrice;
                double priceToNeighbor = currentPrice + weightByPrice;

                if (priceToNeighbor < prices[neighborAirport])
                {
                    prices[neighborAirport] = priceToNeighbor;
                    parents[neighborAirport] = currentAirport;
                }
            }
        }

        // Reconstruct the cheapest path
        List<Airport> cheapestPath = new List<Airport>();
        Airport currentAirportPath = endAirport;

        // Check if the current airport has a parent in the parents dictionary
        while (parents.ContainsKey(currentAirportPath))
        {
            cheapestPath.Insert(0, currentAirportPath);
            currentAirportPath = parents[currentAirportPath];
        }

        // If the start airport is not reached or the path is empty, return an empty list
        if (!currentAirportPath.Equals(startAirport) || cheapestPath.Count == 0)
        {
            return new List<Airport>();
        }

        // Add the start airport to the cheapest path
        cheapestPath.Insert(0, startAirport);

        return cheapestPath;
    }


}
