﻿using AirlinesBusiness.DataStructures;

namespace AirlinesBusiness.Models.SearchStrategy;
public interface ISearchStrategy<T>
{
    List<Airport> FindRoute(Graph<T> flightNetwork, T startAirport, T endAirport);
}
