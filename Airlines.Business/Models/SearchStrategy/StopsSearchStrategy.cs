﻿
using AirlinesBusiness.DataStructures;

namespace AirlinesBusiness.Models.SearchStrategy;
public class StopsSearchStrategy : ISearchStrategy<Airport>
{
    public List<Airport> FindRoute(Graph<Airport> flightNetwork, Airport startAirport, Airport endAirport)
    {
        Queue<Airport> queue = new Queue<Airport>();
        Dictionary<Airport, Airport> previous = new Dictionary<Airport, Airport>();
        List<Airport> visited = new List<Airport>();

        queue.Enqueue(startAirport);
        visited.Add(startAirport);

        while (queue.Count > 0)
        {
            Airport currentAirport = queue.Dequeue();

            // If we reach the end airport is reached, the loop breaks
            if (currentAirport == endAirport)
            {
                break;
            }

            GraphNode<Airport>? currentNode = flightNetwork.GetNode(currentAirport);
            if (currentNode == null)
            {
                continue;
            }

            foreach (Airport neighbor in currentNode.Neighbours.Select(neighbour => neighbour.NeighbourData))
            {
                if (!visited.Contains(neighbor))
                {
                    queue.Enqueue(neighbor);
                    visited.Add(neighbor);
                    previous[neighbor] = currentAirport;
                }
            }
        }

        // Checks if endAirport is reachable
        if (!previous.ContainsKey(endAirport))
        {
            // No path exists between start and end airports
            return new List<Airport>();
        }

        // Reconstruct the shortest path
        List<Airport> path = new List<Airport>();
        Airport current = endAirport;
        while (previous.ContainsKey(current))
        {
            path.Add(current);
            current = previous[current];
        }
        path.Add(startAirport);
        path.Reverse();
        return path;
    }
}
