﻿
namespace AirlinesBusiness.CustomExceptions;
public class InvalidCommandException : Exception
{
    public InvalidCommandException(string message) : base(message) { }
}
