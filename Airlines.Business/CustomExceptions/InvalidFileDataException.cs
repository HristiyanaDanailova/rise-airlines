﻿
namespace AirlinesBusiness.CustomExceptions;
public class InvalidFileDataException : Exception
{
    public InvalidFileDataException(string message) : base(message) { }
}
