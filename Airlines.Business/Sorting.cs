namespace AirlinesBusiness.SortingNS;
public static class Sorter
{
    internal static void BubbleSort(this string[] array, int arraySize, bool ascending = true)
    {
        bool swapped;
        for (int i = 0; i < arraySize - 1; i++)
        {
            swapped = false;
            for (int j = 0; j < arraySize - i - 1; j++)
            {
                int comparisonResult = ascending ? string.Compare(array[j], array[j + 1]) : string.Compare(array[j + 1], array[j]);
                if (comparisonResult == 1)
                {
                    (array[j], array[j + 1]) = (array[j + 1], array[j]);
                    swapped = true;
                }
            }
            if (swapped == false)
            {
                break;
            }

        }
    }

    internal static void SelectionSort(this string[] array, int arraySize, bool ascending = true)
    {

        for (int i = 0; i < arraySize - 1; i++)
        {
            int minIndex = i;
            for (int j = i + 1; j < arraySize; j++)
            {
                int comparisonResult = ascending ? string.Compare(array[minIndex], array[j]) : string.Compare(array[j], array[minIndex]);
                if (comparisonResult == 1)
                {
                    minIndex = j;
                }

            }
            if (minIndex != i)
            {
                (array[i], array[minIndex]) = (array[minIndex], array[i]);
            }
        }

    }
}
