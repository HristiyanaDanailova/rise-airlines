using AirlinesBusinessServices = AirlinesBusinessWeb.Services;
using AirlinesBusinessProfiles = AirlinesBusinessWeb.Profiles;

using AirlinesPersistenceBasic.Repositories;
using AirlinesPersistenceBasic.Repositories.Interfaces;
using AirlinesPersistenceBasic.DBContext;

using AirlinesWebServices.Profiles;
using AirlinesWebServices.Services;
using AutoMapper;
using AirlinesAPI.Middlewares;
namespace Airlines.API;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.

        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        // Repositories
        builder.Services.AddScoped<IAirportRepository, AirportRepository>();
        builder.Services.AddScoped<IAirlineRepository, AirlineRepository>();
        builder.Services.AddScoped<IFlightRepository, FlightRepository>();

        // Airlines.Business.Web mappers
        builder.Services.AddSingleton<AirlinesBusinessProfiles.AirportMapper>();
        builder.Services.AddSingleton<AirlinesBusinessProfiles.AirlineMapper>();
        builder.Services.AddSingleton<AirlinesBusinessProfiles.FlightMapper>();

        // Airlines.Business.Web services
        builder.Services.AddScoped<AirlinesBusinessServices.IAirportService, AirlinesBusinessServices.AirportService>();
        builder.Services.AddScoped<AirlinesBusinessServices.IAirlineService, AirlinesBusinessServices.AirlineService>();
        builder.Services.AddScoped<AirlinesBusinessServices.IFlightService, AirlinesBusinessServices.FlightService>();

        // Airlines.Web.Services mappers
        builder.Services.AddSingleton<AirlineMapper>();
        builder.Services.AddSingleton<AirportMapper>();
        builder.Services.AddSingleton<FlightMapper>();

        // Airlines.Web.Services services
        builder.Services.AddScoped<IAirlineService, AirlineService>();
        builder.Services.AddScoped<IAirportService, AirportService>();
        builder.Services.AddScoped<IFlightService, FlightService>();


        builder.Services.AddAutoMapper(typeof(Program).Assembly);
        builder.Services.AddDbContext<AirlinesDBContext>();

        builder.Services.AddCors(options =>
        {
            options.AddPolicy("AllowAll",
                builder => builder.AllowAnyOrigin()
                                  .AllowAnyMethod()
                                  .AllowAnyHeader());
        });

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseCors("AllowAll");

        app.UseAuthorization();

        app.UseMiddleware<ErrorHandlingMiddleware>();

        app.MapControllers();

        app.Run();
    }
}
