﻿using AirlinesWebServices.Models;
using AirlinesWebServices.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesAPI.Controllers;
[Route("api/[controller]")]
[ApiController]
public class AirportController : ControllerBase
{
    private readonly IAirportService _airportService;
    public AirportController(IAirportService airportService)
    {
        _airportService = airportService;
    }

    [HttpGet]
    public IActionResult GetAirports()
    {
        List<Airport> airports = _airportService.GetAirports();
        if (airports.Count == 0)
        {
            return NoContent();
        }
        return Ok(airports);

    }

    [HttpGet("{id}")]
    public IActionResult GetAirportById(int id)
    {
        Airport? airport = _airportService.GetAirportById(id);
        if (airport == null)
        {
            return NoContent();
        }
        return Ok(airport);
    }

    [HttpPost]
    public IActionResult CreateAirport([FromBody] Airport airport)
    {
        bool isCreated = _airportService.AddAirport(airport);
        if (!isCreated)
        {
            return StatusCode(500, "Internal server error.");
        }
        return StatusCode(201, "Airport was successfully created.");
    }

    [HttpPut]
    public IActionResult UpdateAirport([FromBody] Airport airport)
    {
        bool isUpdated = _airportService.UpdateAirport(airport);
        if (!isUpdated)
        {
            return StatusCode(500, "Internal server error.");
        }
        return Ok();
    }

    [HttpDelete("{id}")]
    public IActionResult DeleteAirport(int id)
    {
        bool isDeleted = _airportService.DeleteAirport(id);
        if (!isDeleted)
        {
            return StatusCode(500, "Internal server error.");
        }
        return Ok();
    }
}
