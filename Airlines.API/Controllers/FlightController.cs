﻿using AirlinesWebServices.Models;
using AirlinesWebServices.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesAPI.Controllers;
[Route("api/[controller]")]
[ApiController]
public class FlightController : ControllerBase
{
    private readonly IFlightService _flightService;
    public FlightController(IFlightService flightService)
    {
        _flightService = flightService;
    }
    [HttpGet]
    public IActionResult GetFlights()
    {
        List<Flight> flights = _flightService.GetFlights();
        if (flights.Count == 0)
        {
            return NoContent();
        }
        return Ok(flights);
    }

    [HttpGet("{id}")]
    public IActionResult GetFlightById(int id)
    {
        Flight? flight = _flightService.GetFlightById(id);
        if (flight == null)
        {
            return NoContent();
        }
        return Ok(flight);
    }

    [HttpPost]
    public IActionResult CreateFlight([FromBody] Flight flight)
    {
        bool isCreated = _flightService.AddFlight(flight);
        if (!isCreated)
        {
            return StatusCode(500, "Internal server error.");

        }
        return StatusCode(201, "Flight was successfully created.");
    }

    [HttpPut]
    public IActionResult UpdateFlight([FromBody] Flight flight)
    {
        bool isUpdated = _flightService.UpdateFlight(flight);
        if (!isUpdated)
        {
            return StatusCode(500, "Internal server error.");
        }
        return Ok();
    }

    [HttpDelete("{id}")]
    public IActionResult DeleteFlight(int id)
    {
        bool isDeleted = _flightService.DeleteFlight(id);
        if (!isDeleted)
        {
            return StatusCode(500, "Internal server error.");
        }
        return Ok();
    }
}
