﻿using AirlinesWebServices.Models;
using AirlinesWebServices.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesAPI.Controllers;
[Route("api/[controller]")]
[ApiController]
public class AirlineController : ControllerBase
{
    private readonly IAirlineService _airlinesService;
    public AirlineController(IAirlineService airlinesService)
    {
        _airlinesService = airlinesService;
    }

    [HttpGet]
    public IActionResult GetAirlines()
    {
        List<Airline> airlines = _airlinesService.GetAirlines();
        if (airlines.Count == 0)
        {
            return NoContent();
        }
        return Ok(airlines);
    }

    [HttpGet("{id}")]
    public IActionResult GetAirlineById(int id)
    {
        Airline? airline = _airlinesService.GetAirlineById(id);
        if (airline == null)
        {
            return NoContent();
        }
        return Ok(airline);
    }

    [HttpPost]
    public IActionResult CreateAirline([FromBody] Airline airline)
    {
        bool isCreated = _airlinesService.AddAirline(airline);
        if (!isCreated)
        {
            return StatusCode(500, "Internal server error.");

        }
        return StatusCode(201, "Airline was successfully created.");
    }

    [HttpPut]
    public IActionResult UpdateAirline([FromBody] Airline airline)
    {
        bool isUpdated = _airlinesService.UpdateAirline(airline);
        if (!isUpdated)
        {
            return StatusCode(500, "Internal server error.");
        }
        return Ok();
    }

    [HttpDelete("{id}")]
    public IActionResult DeleteAirline(int id)
    {
        bool isDeleted = _airlinesService.DeleteAirline(id);
        if (!isDeleted)
        {
            return StatusCode(500, "Internal server error.");

        }
        return Ok();
    }
}
