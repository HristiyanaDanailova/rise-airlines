﻿using AirlinesBusinessWeb.DTO;
using IAirportServiceBusiness = AirlinesBusinessWeb.Services.IAirportService;

using AirlinesWebServices.Models;
using AirlinesWebServices.Profiles;

namespace AirlinesWebServices.Services;
public class AirportService : IAirportService
{
    private readonly IAirportServiceBusiness _airportService;
    private readonly AirportMapper _airportMapper;
    public AirportService(IAirportServiceBusiness airportService, AirportMapper airportMapper)
    {
        _airportService = airportService;
        _airportMapper = airportMapper;
    }
    // Get by column
    public Airport? GetAirportById(int id)
    {
        AirportDTO? airport = _airportService.GetAirportById(id);
        if (airport != null)
        {
            return _airportMapper.MapToModel(airport);
        }
        return null;
    }
    public Airport? GetAirportByCode(string code)
    {
        AirportDTO? airport = _airportService.GetAirportByCode(code);
        if (airport != null)
        {
            return _airportMapper.MapToModel(airport);
        }
        return null;
    }

    // Select airports based on a criteria
    public List<Airport> GetAirportsByCity(string city)
    {
        List<AirportDTO> airports = _airportService.GetAirportsByCity(city);
        return airports.Select(_airportMapper.MapToModel).OfType<Airport>().ToList();
    }
    public List<Airport> GetAirportsByCountry(string country)
    {
        List<AirportDTO> airports = _airportService.GetAirportsByCountry(country);
        return airports.Select(_airportMapper.MapToModel).OfType<Airport>().ToList();

    }
    public List<Airport> GetAirportsByName(string name)
    {
        List<AirportDTO> airports = _airportService.GetAirportsByName(name);
        return airports.Select(_airportMapper.MapToModel).OfType<Airport>().ToList();
    }

    // Sort airports by runways count or founded date
    public List<Airport> SortAirportsByRunwaysCountDesc()
    {
        List<AirportDTO> airports = _airportService.SortAirportsByRunwaysCountDesc();
        return airports.Select(_airportMapper.MapToModel).OfType<Airport>().ToList();
    }
    public List<Airport> SortAirportsByRunwaysCountAsc()
    {
        List<AirportDTO> airports = _airportService.SortAirportsByRunwaysCountAsc();
        return airports.Select(_airportMapper.MapToModel).OfType<Airport>().ToList();
    }
    public List<Airport> SortAirportsByFoundedDateDesc()
    {
        List<AirportDTO> airports = _airportService.SortAirportsByFoundedDateDesc();
        return airports.Select(_airportMapper.MapToModel).OfType<Airport>().ToList();
    }
    public List<Airport> SortAirportsByFoundedDateAsc()
    {
        List<AirportDTO> airports = _airportService.SortAirportsByFoundedDateAsc();
        return airports.Select(_airportMapper.MapToModel).OfType<Airport>().ToList();
    }

    // Basic CRUD operations
    public List<Airport> GetAirports()
    {
        List<AirportDTO> airports = _airportService.GetAirports();
        return airports.Select(_airportMapper.MapToModel).OfType<Airport>().ToList();
    }

    public bool AddAirport(Airport airport)
    {
        AirportDTO? airportDTO = _airportMapper.MapToDTO(airport);
        return airportDTO != null && _airportService.AddAirport(airportDTO);
    }
    public bool UpdateAirport(Airport airport)
    {

        AirportDTO? airportDTO = _airportMapper.MapToDTO(airport);
        return airportDTO != null && _airportService.UpdateAirport(airportDTO);
    }

    public bool DeleteAirport(int id)
    {
        return _airportService.DeleteAirport(id);
    }

    public List<Airport> GetAirportsByFilter(string value, string objectProperty)
    {
        List<AirportDTO> airports = _airportService.GetAirportsByFilter(value, objectProperty);
        return airports.Select(_airportMapper.MapToModel).OfType<Airport>().ToList();
    }
}
