﻿using AirlinesBusinessWeb.DTO;
using IAirportServiceBusiness = AirlinesBusinessWeb.Services.IAirportService;
using IAirlineServiceBusiness = AirlinesBusinessWeb.Services.IAirlineService;
using IFlightServiceBusiness = AirlinesBusinessWeb.Services.IFlightService;

using AirlinesWebServices.Models;
using AirlinesWebServices.Profiles;

namespace AirlinesWebServices.Services;
public class FlightService : IFlightService
{

    private readonly IFlightServiceBusiness _flightService;
    private readonly IAirportServiceBusiness _airportService;
    private readonly IAirlineServiceBusiness _airlineService;
    private readonly FlightMapper _flightMapper;
    public FlightService(IFlightServiceBusiness flightServicePersistence, IAirportServiceBusiness airportServicePersistence, IAirlineServiceBusiness airlineServicePersistence)
    {
        _flightService = flightServicePersistence;
        _airportService = airportServicePersistence;
        _airlineService = airlineServicePersistence;
        _flightMapper = new FlightMapper();
    }

    // Get by column
    public Flight? GetFlightById(int id)
    {
        FlightDTO? flight = _flightService.GetFlightById(id);
        if (flight != null)
        {
            string? departureAirportName = GetAirportNameById(flight.DepartureAirportId);
            string? arrivalAirportName = GetAirportNameById(flight.ArrivalAirportId);
            string? airlineName = GetAirlineNameById(flight.AirlineId);
            if (departureAirportName != null && arrivalAirportName != null && airlineName != null)
            {
                return _flightMapper.MapToModel(flight, departureAirportName, arrivalAirportName, airlineName);
            }
        }
        return null;
    }
    public Flight? GetFlightByFlightNumber(string flightNumber)
    {
        FlightDTO? flight = _flightService.GetFlightByFlightNumber(flightNumber);
        if (flight != null)
        {
            string? departureAirportName = GetAirportNameById(flight.DepartureAirportId);
            string? arrivalAirportName = GetAirportNameById(flight.ArrivalAirportId);
            string? airlineName = GetAirlineNameById(flight.AirlineId);
            if (departureAirportName != null && arrivalAirportName != null && airlineName != null)
            {
                return _flightMapper.MapToModel(flight, departureAirportName, arrivalAirportName, airlineName);
            }
        }
        return null;
    }
    // Farthest flight based on deparute and arrival time
    public Flight? GetFarthestFlight()
    {
        FlightDTO? flight = _flightService.GetFarthestFlight();
        if (flight != null)
        {
            string? departureAirportName = GetAirportNameById(flight.DepartureAirportId);
            string? arrivalAirportName = GetAirportNameById(flight.ArrivalAirportId);
            string? airlineName = GetAirlineNameById(flight.AirlineId);
            if (departureAirportName != null && arrivalAirportName != null && airlineName != null)
            {
                return _flightMapper.MapToModel(flight, departureAirportName, arrivalAirportName, airlineName);
            }
        }
        return null;
    }
    // Select flights based on a criteria
    public List<Flight> GetFlightsFromDepartureAirportByName(string departureAirportName)
    {
        List<FlightDTO> flights = _flightService.GetFlightsFromDepartureAirportByName(departureAirportName);
        return flights.Select(flight =>
        {
            string? departureAirportName = GetAirportNameById(flight.DepartureAirportId);
            string? arrivalAirportName = GetAirportNameById(flight.ArrivalAirportId);
            string? airlineName = GetAirlineNameById(flight.AirlineId);
            if (departureAirportName != null && arrivalAirportName != null && airlineName != null)
            {
                return _flightMapper.MapToModel(flight, departureAirportName, arrivalAirportName, airlineName);
            }
            return null;
        }).OfType<Flight>().ToList();
    }
    public List<Flight> GetFlightsToArrivalAirportByName(string arrivalAirportName)
    {
        List<FlightDTO> flights = _flightService.GetFlightsToArrivalAirportByName(arrivalAirportName);
        return flights.Select(flight =>
        {
            string? departureAirportName = GetAirportNameById(flight.DepartureAirportId);
            string? arrivalAirportName = GetAirportNameById(flight.ArrivalAirportId);
            string? airlineName = GetAirlineNameById(flight.AirlineId);
            if (departureAirportName != null && arrivalAirportName != null && airlineName != null)
            {
                return _flightMapper.MapToModel(flight, departureAirportName, arrivalAirportName, airlineName);
            }
            return null;
        }).OfType<Flight>().ToList();
    }
    public List<Flight> GetFlightsByAirlineByName(string airlineName)
    {
        List<FlightDTO> flights = _flightService.GetFlightsByAirlineName(airlineName);
        return flights.Select(flight =>
        {
            string? departureAirportName = GetAirportNameById(flight.DepartureAirportId);
            string? arrivalAirportName = GetAirportNameById(flight.ArrivalAirportId);
            string? airlineName = GetAirlineNameById(flight.AirlineId);
            if (departureAirportName != null && arrivalAirportName != null && airlineName != null)
            {
                return _flightMapper.MapToModel(flight, departureAirportName, arrivalAirportName, airlineName);
            }
            return null;
        }).OfType<Flight>().ToList();
    }
    public List<Flight> GetFlightsByDepartureAirportCode(string airportCode)
    {
        List<FlightDTO> flights = _flightService.GetFlightsByDepartureAirportCode(airportCode);
        return flights.Select(flight =>
        {
            string? departureAirportName = GetAirportNameById(flight.DepartureAirportId);
            string? arrivalAirportName = GetAirportNameById(flight.ArrivalAirportId);
            string? airlineName = GetAirlineNameById(flight.AirlineId);
            if (departureAirportName != null && arrivalAirportName != null && airlineName != null)
            {
                return _flightMapper.MapToModel(flight, departureAirportName, arrivalAirportName, airlineName);
            }
            return null;
        }).OfType<Flight>().ToList();
    }
    public List<Flight> GetFlightsByArrivalAirportCode(string airportCode)
    {

        List<FlightDTO> flights = _flightService.GetFlightsByArrivalAirportCode(airportCode);
        return flights.Select(flight =>
        {
            string? departureAirportName = GetAirportNameById(flight.DepartureAirportId);
            string? arrivalAirportName = GetAirportNameById(flight.ArrivalAirportId);
            string? airlineName = GetAirlineNameById(flight.AirlineId);
            if (departureAirportName != null && arrivalAirportName != null && airlineName != null)
            {
                return _flightMapper.MapToModel(flight, departureAirportName, arrivalAirportName, airlineName);
            }
            return null;
        }).OfType<Flight>().ToList();
    }
    // Sort flights by flight duration, based on departure and arrival time
    public List<Flight> SortFlightsByFlightDurationDesc()
    {
        List<FlightDTO> flights = _flightService.SortFlightsByFlightDurationDesc();
        return flights.Select(flight =>
        {
            string? departureAirportName = GetAirportNameById(flight.DepartureAirportId);
            string? arrivalAirportName = GetAirportNameById(flight.ArrivalAirportId);
            string? airlineName = GetAirlineNameById(flight.AirlineId);
            if (departureAirportName != null && arrivalAirportName != null && airlineName != null)
            {
                return _flightMapper.MapToModel(flight, departureAirportName, arrivalAirportName, airlineName);
            }
            return null;
        }).OfType<Flight>().ToList();
    }
    public List<Flight> SortFlightsByFlightDurationAsc()
    {
        List<FlightDTO> flights = _flightService.SortFlightsByFlightDurationAsc();
        return flights.Select(flight =>
        {
            string? departureAirportName = GetAirportNameById(flight.DepartureAirportId);
            string? arrivalAirportName = GetAirportNameById(flight.ArrivalAirportId);
            string? airlineName = GetAirlineNameById(flight.AirlineId);
            if (departureAirportName != null && arrivalAirportName != null && airlineName != null)
            {

                return _flightMapper.MapToModel(flight, departureAirportName, arrivalAirportName, airlineName);
            }
            return null;
        }).OfType<Flight>().ToList();
    }

    // Basic CRUD operations
    public List<Flight> GetFlights()
    {

        List<FlightDTO> flights = _flightService.GetFlights();
        return flights.Select(flight =>
        {
            string? departureAirportName = GetAirportNameById(flight.DepartureAirportId);
            string? arrivalAirportName = GetAirportNameById(flight.ArrivalAirportId);
            string? airlineName = GetAirlineNameById(flight.AirlineId);
            if (departureAirportName != null && arrivalAirportName != null && airlineName != null)
            {
                return _flightMapper.MapToModel(flight, departureAirportName, arrivalAirportName, airlineName);
            }
            return null;
        }).OfType<Flight>().ToList();
    }
    public bool AddFlight(Flight flight)
    {
        int departureAirportId = GetAirportIdByName(flight.DepartureAirportName);
        int arrivalAirportId = GetAirportIdByName(flight.ArrivalAirportName);
        int airlineId = GetAirlineIdByName(flight.AirlineName);
        if (departureAirportId != -1 && arrivalAirportId != -1 && airlineId != -1)
        {
            FlightDTO? flightDTO = _flightMapper.MapToDTO(flight, departureAirportId, arrivalAirportId, airlineId);
            return flightDTO != null && _flightService.AddFlight(flightDTO);
        }
        return false;
    }
    public bool UpdateFlight(Flight flight)
    {
        int departureAirportId = GetAirportIdByName(flight.DepartureAirportName);
        int arrivalAirportId = GetAirportIdByName(flight.ArrivalAirportName);
        int airlineId = GetAirlineIdByName(flight.AirlineName);
        if (departureAirportId != -1 && arrivalAirportId != -1 && airlineId != -1)
        {
            FlightDTO? flightDTO = _flightMapper.MapToDTO(flight, departureAirportId, arrivalAirportId, airlineId);
            return flightDTO != null && _flightService.UpdateFlight(flightDTO);
        }
        return false;
    }
    public bool DeleteFlight(int id)
    {
        return _flightService.DeleteFlight(id);
    }

    public List<Flight> GetFlightsByFilter(string value, string objectProperty)
    {
        List<FlightDTO> flights = _flightService.GetFlightsByFilter(value, objectProperty);
        return flights.Select(flight =>
        {
            string? departureAirportName = GetAirportNameById(flight.DepartureAirportId);
            string? arrivalAirportName = GetAirportNameById(flight.ArrivalAirportId);
            string? airlineName = GetAirlineNameById(flight.AirlineId);
            if (departureAirportName != null && arrivalAirportName != null && airlineName != null)
            {
                return _flightMapper.MapToModel(flight, departureAirportName, arrivalAirportName, airlineName);
            }
            return null;
        }).OfType<Flight>().ToList();
    }

    private int GetAirportIdByName(string name)
    {
        AirportDTO? airport = _airportService.GetAirportsByName(name).FirstOrDefault();
        return (airport == null) ? -1 : airport.Id;
    }
    private int GetAirlineIdByName(string name)
    {
        AirlineDTO? airline = _airlineService.GetAirlinesByName(name).FirstOrDefault();
        return (airline == null) ? -1 : airline.Id;
    }
    private string? GetAirportNameById(int id)
    {
        return _airportService.GetAirportById(id)?.Name;
    }
    private string? GetAirlineNameById(int id)
    {
        return _airlineService.GetAirlineById(id)?.Name;
    }
}