﻿using AirlinesWebServices.Models;

namespace AirlinesWebServices.Services;
public interface IFlightService
{
    // Get by column
    Flight? GetFlightById(int id);
    Flight? GetFlightByFlightNumber(string flightNumber);

    // Farthest flight based on deparute and arrival time
    Flight? GetFarthestFlight();

    // Select flights based on a criteria
    List<Flight> GetFlightsFromDepartureAirportByName(string departureAirportName);
    List<Flight> GetFlightsToArrivalAirportByName(string arrivalAirportName);
    List<Flight> GetFlightsByAirlineByName(string airlineName);
    List<Flight> GetFlightsByDepartureAirportCode(string airportCode);
    List<Flight> GetFlightsByArrivalAirportCode(string airportCode);

    // Sort flights by flight duration, based on departure and arrival time
    List<Flight> SortFlightsByFlightDurationDesc();
    List<Flight> SortFlightsByFlightDurationAsc();

    // Basic CRUD operations
    List<Flight> GetFlights();
    bool AddFlight(Flight flight);
    bool UpdateFlight(Flight flight);
    bool DeleteFlight(int id);

    List<Flight> GetFlightsByFilter(string value, string objectProperty);
}
