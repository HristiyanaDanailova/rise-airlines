﻿using AirlinesBusinessWeb.DTO;
using IAirlineServiceBusiness = AirlinesBusinessWeb.Services.IAirlineService;

using AirlinesWebServices.Models;
using AirlinesWebServices.Profiles;

namespace AirlinesWebServices.Services;
public class AirlineService : IAirlineService
{
    private readonly IAirlineServiceBusiness _airlineService;
    private readonly AirlineMapper _airlineMapper;

    public AirlineService(IAirlineServiceBusiness airlineService, AirlineMapper airlineMapper)
    {
        _airlineService = airlineService;
        _airlineMapper = airlineMapper;
    }
    // Select by column
    public Airline? GetAirlineById(int id)
    {
        AirlineDTO? airline = _airlineService.GetAirlineById(id);
        if (airline != null)
        {
            return _airlineMapper.MapToModel(airline);
        }
        return null;
    }
    // Select by a certain criteria
    public List<Airline> GetAirlinesByName(string name)
    {
        List<AirlineDTO> airlines = _airlineService.GetAirlinesByName(name);
        return airlines.Select(_airlineMapper.MapToModel).OfType<Airline>().ToList();
    }
    // Sort airlines by fleetSize and founded date

    public List<Airline> SortAirlinesByFleetSizeDesc()
    {
        List<AirlineDTO> airlines = _airlineService.SortAirlinesByFleetSizeDesc();
        return airlines.Select(_airlineMapper.MapToModel).OfType<Airline>().ToList();
    }
    public List<Airline> SortAirlinesByFleetSizeAsc()
    {
        List<AirlineDTO> airlines = _airlineService.SortAirlinesByFleetSizeAsc();
        return airlines.Select(_airlineMapper.MapToModel).OfType<Airline>().ToList();
    }
    public List<Airline> SortAirlinesByFoundedDateDesc()
    {
        List<AirlineDTO> airlines = _airlineService.SortAirlinesByFoundedDateDesc();
        return airlines.Select(_airlineMapper.MapToModel).OfType<Airline>().ToList();
    }
    public List<Airline> SortAirlinesByFoundedDateAsc()
    {
        List<AirlineDTO> airlines = _airlineService.SortAirlinesByFoundedDateAsc();
        return airlines.Select(_airlineMapper.MapToModel).OfType<Airline>().ToList();
    }

    // Basic CRUD operations
    public List<Airline> GetAirlines()
    {
        List<AirlineDTO> airlines = _airlineService.GetAirlines();
        return airlines.Select(_airlineMapper.MapToModel).OfType<Airline>().ToList();
    }
    public bool AddAirline(Airline airline)
    {
        AirlineDTO? airlineDTO = _airlineMapper.MapToDTO(airline);
        return airlineDTO != null && _airlineService.AddAirline(airlineDTO);
    }
    public bool UpdateAirline(Airline airline)
    {

        AirlineDTO? airlineDTO = _airlineMapper.MapToDTO(airline);
        return airlineDTO != null && _airlineService.UpdateAirline(airlineDTO);
    }
    public bool DeleteAirline(int id)
    {
        return _airlineService.DeleteAirline(id);
    }

    public List<Airline> GetAirlinesByFilter(string value, string objectProperty)
    {
        List<AirlineDTO> airlines = _airlineService.GetAirlinesByFilter(value, objectProperty);
        return airlines.Select(_airlineMapper.MapToModel).OfType<Airline>().ToList();
    }
}
