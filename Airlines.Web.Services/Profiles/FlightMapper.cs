﻿using AutoMapper;

using AirlinesWebServices.Models;

using AirlinesBusinessWeb.DTO;

namespace AirlinesWebServices.Profiles;
public class FlightMapper
{
    private readonly IMapper _mapper;

    public FlightMapper()
    {
        var mapperConfig = new MapperConfiguration(cfg =>
        {

            cfg.CreateMap<Flight, FlightDTO>()
              .ForMember(dest => dest.DepartureAirportId, opt => opt.Ignore())
              .ForMember(dest => dest.ArrivalAirportId, opt => opt.Ignore())
              .ForMember(dest => dest.AirlineId, opt => opt.Ignore());

            cfg.CreateMap<FlightDTO, Flight>()
              .ForMember(dest => dest.DepartureAirportName, opt => opt.Ignore())
              .ForMember(dest => dest.ArrivalAirportName, opt => opt.Ignore())
              .ForMember(dest => dest.AirlineName, opt => opt.Ignore());
        });


        _mapper = mapperConfig.CreateMapper();
    }
    public Flight? MapToModel(FlightDTO flightDTO, string departureAirportName, string arrivalAirportName, string airlineName)
    {
        try
        {
            Flight flight = _mapper.Map<Flight>(flightDTO);
            flight.DepartureAirportName = departureAirportName;
            flight.ArrivalAirportName = arrivalAirportName;
            flight.AirlineName = airlineName;
            return flight;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
    }
    public FlightDTO? MapToDTO(Flight flight, int departureAirportId, int arrivalAirportId, int airportId)
    {
        try
        {
            FlightDTO flightDTO = _mapper.Map<FlightDTO>(flight);
            flightDTO.DepartureAirportId = departureAirportId;
            flightDTO.ArrivalAirportId = arrivalAirportId;
            flightDTO.AirlineId = airportId;
            return flightDTO;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }

    }

}
