﻿using AutoMapper;

using AirlinesWebServices.Models;

using AirlinesBusinessWeb.DTO;

namespace AirlinesWebServices.Profiles;
public class AirportMapper
{
    private readonly IMapper _mapper;

    public AirportMapper()
    {
        var mapperConfig = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Airport, AirportDTO>().ReverseMap();
        });

        _mapper = mapperConfig.CreateMapper();
    }

    public Airport? MapToModel(AirportDTO airportDTO)
    {
        try
        {
            Airport airport = _mapper.Map<Airport>(airportDTO);
            return airport;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
    }
    public AirportDTO? MapToDTO(Airport airport)
    {
        try
        {
            AirportDTO airportDTO = _mapper.Map<AirportDTO>(airport);
            return airportDTO;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
    }
}
