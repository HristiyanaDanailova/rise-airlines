﻿namespace AirlinesWebServices.Models;
public class Airline
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int FleetSize { get; set; }
    public DateOnly Founded { get; set; }
}
