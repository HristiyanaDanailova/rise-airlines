using AirlinesBusiness.SearchingNS;

namespace AirlinesConsole.ValidationsNS;
public class Validator
{
    // checks if a command is one of the available
    public static bool IsValidCommand(string command)
    {
        return (IsExitCommand(command) || IsHelpCommand(command) ||
                IsSearchCommand(command) || IsSortCommand(command) ||
                IsExistCommand(command) || IsListCommand(command) ||
                IsRouteNewCommand(command) || IsRouteAddCommand(command) ||
                IsRouteRemoveCommand(command) || IsRoutePrintCommand(command) ||
                IsReserveCargoCommand(command) || IsReserveTicketCommand(command) ||
                IsRouteFindCommand(command) || IsRouteCheckCommand(command) ||
                IsRouteSearchCommand(command));
    }

    public static bool IsExitCommand(string command)
    {
        return string.Equals(command, "Exit");
    }

    public static bool IsHelpCommand(string command)
    {
        return string.Equals(command, "Help");
    }

    public static bool IsSearchCommand(string command)
    {
        return command.StartsWith("Search ");
    }

    public static bool IsSortCommand(string command)
    {
        return command.StartsWith("Sort ");
    }

    public static bool IsExistCommand(string command)
    {
        return command.StartsWith("Exist ");
    }

    public static bool IsListCommand(string command)
    {
        return command.StartsWith("List");
    }

    public static bool IsRouteNewCommand(string command)
    {
        return string.Equals(command, "Route new");
    }

    public static bool IsRouteAddCommand(string command)
    {
        return command.StartsWith("Route add");
    }

    public static bool IsRouteRemoveCommand(string command)
    {
        return string.Equals(command, "Route remove");
    }

    public static bool IsRoutePrintCommand(string command)
    {
        return string.Equals(command, "Route print");
    }

    public static bool IsReserveCargoCommand(string command)
    {
        return command.StartsWith("Reserve cargo ");
    }

    public static bool IsReserveTicketCommand(string command)
    {
        return command.StartsWith("Reserve ticket ");
    }

    public static bool IsBatchStartCommand(string command)
    {
        return string.Equals(command, "Batch start");
    }

    public static bool IsBatchRunCommand(string command)
    {
        return string.Equals(command, "Batch run");
    }
    public static bool IsBatchCancelCommand(string command)
    {
        return string.Equals(command, "Batch cancel");
    }
    public static bool IsRouteFindCommand(string command)
    {
        return command.StartsWith("Route find");
    }
    public static bool IsRouteCheckCommand(string command)
    {
        return command.StartsWith("Route check");
    }
    public static bool IsRouteSearchCommand(string command)
    {
        return command.StartsWith("Route search");
    }

    // checks if a string is an element of an array
    public static bool IsUnique(string input, string[] array, int arraySize)
    {
        return !(Searcher.LinearSearch(input, array, arraySize));
    }

    // checks if the input is a valid data name
    public static bool IsValidDataArray(string input)
    {
        return (string.Equals(input, "airlines") || string.Equals(input, "airports") || string.Equals(input, "flights"));
    }

    // checks if the input is a valid order specifier
    public static bool IsValidOrderSpecifier(string input)
    {
        return (string.Equals(input, "ascending") || string.Equals(input, "descending"));
    }

    // checks if the input is a City or Country
    public static bool IsCityOrCountry(string input)
    {
        return (string.Equals(input, "City") || string.Equals(input, "Country"));
    }

    public static bool IsValidNumberRepresentation(string input)
    {
        foreach (char symbol in input)
        {
            if (!char.IsDigit(symbol) || !Equals(symbol, '.'))
            {
                return false;
            }
        }
        return true;
    }
}
