﻿using AirlinesBusiness.Models;
using AirlinesBusiness.Managers;

using AirlinesConsole.UtilsNS;
using AirlinesConsole.CommandManagerNS;

namespace AirlinesConsole;
internal class Program
{
    private static void Main(string[]? args)
    {

        AirportsManager airportsManager = new AirportsManager();
        AirlinesManager airlinesManager = new AirlinesManager();
        FlightsManager flightsManager = new FlightsManager();
        Route route = new Route();
        AircraftsManager aircraftManager = new AircraftsManager();
        RouteFinder routeFinder = new RouteFinder();
        FlightsNetwork flightsNetwork = new FlightsNetwork();

        try
        {
            // Loads the initial data from the given .csv files
            airportsManager.LoadAirports(Constants.Constants.AIRPORTS_FILENAME);
            airlinesManager.LoadAirlines(Constants.Constants.AIRLINES_FILENAME);
            aircraftManager.LoadAircrafts(Constants.Constants.AIRCRAFTS_FILENAME);
            flightsManager.LoadFlights(Constants.Constants.FLIGHTS_FILENAME, airportsManager, aircraftManager);
            routeFinder.LoadRoutes(Constants.Constants.ROUTES_FILENAME, flightsManager, airportsManager);
            flightsNetwork.LoadFlightNetwork(Constants.Constants.ROUTES_FILENAME, flightsManager, airportsManager);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error ocurred: {ex.Message}");
            Console.WriteLine("Program execution ended! Make sure you have provided valid data files!");
            return;
        }

        routeFinder.ConnectRoutes(flightsManager, airportsManager, routeFinder.Routes.Root);
        flightsNetwork.ConnectFlightsNetwork(flightsManager, airportsManager);

        CommandManager commandManager = new CommandManager(airportsManager, airlinesManager, flightsManager, route, aircraftManager, routeFinder, flightsNetwork);

        ConsoleUtils.PrintHelp();
        while (true)
        {
            Console.Write("Command > ");
            string command = ConsoleUtils.ReadLine().Trim();
            commandManager.ProcessCommand(command);
        }
    }
}

