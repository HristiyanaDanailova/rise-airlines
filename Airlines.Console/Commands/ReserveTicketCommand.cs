﻿using AirlinesBusiness.Models.Aircraft;
using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Managers;
using AirlinesBusiness.Models;

namespace AirlinesConsole.Commands;
public class ReserveTicketCommand : ICommand
{
    private readonly FlightsManager _flightsManager;
    private readonly AircraftsManager _aircraftManager;
    private readonly string _flightId;
    private readonly uint _seats;
    private readonly uint _smallBaggageCount;
    private readonly uint _largeBaggageCount;

    public ReserveTicketCommand(FlightsManager flightsManager, AircraftsManager aircraftManager, string flightId, uint seats, uint smallBaggageCount, uint largeBaggageCount)
    {
        _flightsManager = flightsManager;
        _aircraftManager = aircraftManager;
        _flightId = flightId;
        _seats = seats;
        _smallBaggageCount = smallBaggageCount;
        _largeBaggageCount = largeBaggageCount;
    }
    public void Execute()
    {
        Flight? flight = _flightsManager.FlightById(_flightId);
        if (flight == null)
        {
            throw new EntityNotFoundException($"No flight with id {_flightId} exists!");
        }
        string aircraftModel = flight.AircraftModel;
        PassengerAircraft? aircraft = _aircraftManager.PassengerAircraftByModel(aircraftModel);
        if (aircraft == null)
        {
            throw new InvalidInputException($"Couldn't reserve a ticket for flight with id {_flightId}! The flight doesn't have an aircraft of type passenger!");

        }
        if (aircraft.CanBeReserved(_seats, _smallBaggageCount, _largeBaggageCount))
        {
            Console.WriteLine($"Model {aircraft.Model} of aircraft with seats: {aircraft.Seats}, load: {aircraft.CargoLoad} and volume: {aircraft.CargoVolume} can be reserved!");
        }
        else
        {
            Console.WriteLine($"No aircraft with {_seats} seats that can take {_smallBaggageCount} small baggage and {_largeBaggageCount} large baggage is available!");
        }

    }

}
