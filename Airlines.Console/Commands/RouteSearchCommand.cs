﻿using AirlinesBusiness.Models;
using AirlinesBusiness.Models.SearchStrategy;

namespace AirlinesConsole.Commands;
public class RouteSearchCommand : ICommand
{
    private readonly FlightsNetwork _flightsNetwork;
    private readonly Airport _startAirport;
    private readonly Airport _endAirport;
    private readonly string _strategy;

    public RouteSearchCommand(FlightsNetwork flightNetwork, Airport startAirport, Airport endAirport, string strategy)
    {
        _flightsNetwork = flightNetwork;
        _startAirport = startAirport;
        _endAirport = endAirport;
        _strategy = strategy;
    }

    public void Execute()
    {
        List<Airport> path = _flightsNetwork.FindRoute(_startAirport, _endAirport, _strategy);
        FlightsNetwork.PrintPath(path);
    }
}
