﻿using AirlinesConsole.UtilsNS;

namespace AirlinesConsole.Commands;
public class HelpCommand : ICommand
{
    public void Execute()
    {
        ConsoleUtils.PrintHelp();
    }
}
