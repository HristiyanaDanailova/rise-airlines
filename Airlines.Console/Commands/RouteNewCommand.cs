﻿using AirlinesBusiness.Models;

namespace AirlinesConsole.Commands;
public class RouteNewCommand : ICommand
{
    private readonly Route _route;
    public RouteNewCommand(Route route)
    {
        _route = route;
    }

    public void Execute()
    {
        _route.EmptyRoute();
    }
}
