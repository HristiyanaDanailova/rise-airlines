﻿using AirlinesBusiness.DataStructures;
using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Managers;
using AirlinesBusiness.Models;

namespace AirlinesConsole.Commands;
public class RouteFindCommand : ICommand
{
    private readonly AirportsManager _airportsManager;
    private readonly RouteFinder _routeFinder;
    private readonly string _destinationAirport;

    public RouteFindCommand(AirportsManager airportsManager, RouteFinder routeFinder, string destinationAirport)
    {
        _airportsManager = airportsManager;
        _routeFinder = routeFinder;
        _destinationAirport = destinationAirport;
    }
    public void Execute()
    {
        Airport? airport = _airportsManager.GetAirportById(_destinationAirport);
        if (airport == null)
        {
            throw new EntityNotFoundException("No airport with such id was found!");
        }
        List<TreeNode<Airport>> flightRoute = _routeFinder.FindFlightRoute(airport);
        if (flightRoute.Count == 0)
        {
            Console.WriteLine("No route found to the destination airport.");
            return;
        }

        Console.WriteLine("Flight Route:");

        foreach (TreeNode<Airport> node in flightRoute)
        {
            Console.WriteLine(node.Data!.Id);
        }

    }
}
