﻿using AirlinesBusiness.Models;

namespace AirlinesConsole.Commands;
public class RoutePrintCommand : ICommand
{
    private readonly Route _route;
    public RoutePrintCommand(Route route)
    {
        _route = route;
    }
    public void Execute()
    {
        _route.PrintRoute();

    }
}
