﻿using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Managers;
using AirlinesBusiness.Models;

namespace AirlinesConsole.Commands;
public class ListCommand : ICommand
{
    private readonly AirportsManager _airportsManager;
    private readonly string _inputData;
    private readonly string _from;
    public ListCommand(AirportsManager airportsManager, string inputData, string from)
    {
        _airportsManager = airportsManager;
        _inputData = inputData;
        _from = from;
    }
    public void Execute()
    {
        if (string.Equals(_from, "City"))
        {
            List<Airport> airportsByCity = _airportsManager.ListAirportsByCity(_inputData);
            if (airportsByCity.Count == 0)
            {
                Console.WriteLine($"There were no airports found in {_inputData}");
            }
            else
            {
                Console.WriteLine($"----------Airports in {_inputData}----------");
                foreach (Airport airport in airportsByCity)
                {
                    airport.PrintAirport();
                    Console.WriteLine("----------");
                }
            }
        }
        else if (string.Equals(_from, "Country"))
        {
            List<Airport>? airportsByCountry = _airportsManager.ListAirportsByCountry(_inputData);
            if (airportsByCountry!.Count == 0)
            {
                Console.WriteLine($"There were no airports found in {_inputData}");
            }
            else
            {
                Console.WriteLine($"----------Airports in {_inputData}----------");
                foreach (Airport airport in airportsByCountry)
                {
                    airport.PrintAirport();
                    Console.WriteLine("----------");
                }
            }
        }
    }

}
