﻿using AirlinesBusiness.Models;

namespace AirlinesConsole.Commands;
public class RouteCheckCommand : ICommand
{

    private readonly FlightsNetwork _flightsNetwork;
    private readonly Airport _startAirport;
    private readonly Airport _endAirport;

    public RouteCheckCommand(FlightsNetwork flightNetwork, Airport startAirport, Airport endAirport)
    {
        _flightsNetwork = flightNetwork;
        _startAirport = startAirport;
        _endAirport = endAirport;
    }
    public void Execute()
    {
        if (_flightsNetwork.AreAirportsConnected(_startAirport, _endAirport))
        {
            Console.WriteLine("Airports are connected");
        }
        else
        {
            Console.WriteLine("Airports are not connected");
        }
    }
}
