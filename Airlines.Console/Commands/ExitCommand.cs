﻿namespace AirlinesConsole.Commands;
public class ExitCommand : ICommand
{
    public void Execute()
    {
        Console.WriteLine("Program ended.");
        Environment.Exit(0);
    }
}
