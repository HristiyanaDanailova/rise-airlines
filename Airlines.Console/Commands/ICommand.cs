﻿namespace AirlinesConsole.Commands;
public interface ICommand
{
    void Execute();
}
