﻿using AirlinesBusiness.Managers;

namespace AirlinesConsole.Commands;
public class SortCommand : ICommand
{
    private readonly AirportsManager _airportsManager;
    private readonly AirlinesManager _airlinesManager;
    private readonly FlightsManager _flightsManager;
    private readonly string _inputData;
    private readonly string _order;

    public SortCommand(AirportsManager airportsManager, AirlinesManager airlinesManager, FlightsManager flightsManager, string inputData, string order)
    {
        _airportsManager = airportsManager;
        _airlinesManager = airlinesManager;
        _flightsManager = flightsManager;
        _inputData = inputData;
        _order = order;
    }
    public void Execute()
    {
        if (string.Equals(_inputData, "airlines"))
        {
            _airlinesManager.SortAirlines(_order);
            _airlinesManager.PrintAirlines();
        }
        else if (string.Equals(_inputData, "airports"))
        {
            _airportsManager.SortAirports(_order);
            _airportsManager.PrintAirports();
        }
        else if (string.Equals(_inputData, "flights"))
        {
            _flightsManager.SortFlights(_order);
            _flightsManager.PrintFlights();
        }
    }
}
