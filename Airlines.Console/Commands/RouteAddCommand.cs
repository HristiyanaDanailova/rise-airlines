﻿using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Managers;
using AirlinesBusiness.Models;

namespace AirlinesConsole.Commands;
public class RouteAddCommand : ICommand
{
    private readonly FlightsManager _flightsManager;
    private readonly Route _route;
    private readonly string _flightId;

    public RouteAddCommand(FlightsManager flightsManager, Route route, string flightId)
    {
        _flightsManager = flightsManager;
        _route = route;
        _flightId = flightId;
    }
    public void Execute()
    {
        Flight? flight = _flightsManager.FlightById(_flightId);
        if (flight == null)
        {
            throw new EntityNotFoundException($"No flight with id {_flightId} exists!");
        }
        if (_route.AddFlight(flight))
        {
            Console.WriteLine("Flight added to route successfully!");
        }
        else
        {
            throw new InvalidInputException($"Couldn't add flight to route! The departute airport of flight with id {_flightId} doesn't match the arrival airport of the last flight in the route");
        }
    }

}
