﻿using AirlinesBusiness.Managers;

namespace AirlinesConsole.Commands;
public class SearchCommand : ICommand
{
    private readonly AirportsManager _airportsManager;
    private readonly AirlinesManager _airlinesManager;
    private readonly FlightsManager _flightsManager;
    private readonly string _searchTerm;
    public SearchCommand(AirportsManager airportsManager, AirlinesManager airlinesManager, FlightsManager flightsManager, string searchTerm)
    {
        _airportsManager = airportsManager;
        _airlinesManager = airlinesManager;
        _flightsManager = flightsManager;
        _searchTerm = searchTerm;
    }
    public void Execute()
    {
        bool foundInAirports = _airportsManager.HasAirportByName(_searchTerm);
        bool foundInAirlines = _airlinesManager.HasAirline(_searchTerm);
        bool foundInFlights = _flightsManager.HasFlight(_searchTerm);

        if (!(foundInAirports || foundInAirlines || foundInFlights))
        {
            Console.WriteLine($"\"{_searchTerm}\" was not found!");
        }
        else
        {
            if (foundInAirports)
            {
                Console.WriteLine($"\"{_searchTerm}\" was found in Airports");
            }
            if (foundInAirlines)
            {
                Console.WriteLine($"\"{_searchTerm}\" was found in Airlines");
            }
            if (foundInFlights)
            {
                Console.WriteLine($"\"{_searchTerm}\" was found in Flights");
            }
        }
    }
}
