﻿using AirlinesBusiness.Managers;

namespace AirlinesConsole.Commands;
public class ExistCommand : ICommand
{
    private readonly AirlinesManager _airlinesManager;
    private readonly string _searchTerm;
    public ExistCommand(AirlinesManager airlinesManager, string searchTerm)
    {
        _airlinesManager = airlinesManager;
        _searchTerm = searchTerm;
    }
    public void Execute()
    {
        if (_airlinesManager.HasAirline(_searchTerm))
        {
            Console.WriteLine("True");
        }
        else
        {
            Console.WriteLine("False");
        }
    }
}
