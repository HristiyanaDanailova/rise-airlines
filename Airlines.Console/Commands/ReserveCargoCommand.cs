﻿using AirlinesBusiness.Models.Aircraft;
using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Managers;
using AirlinesBusiness.Models;

namespace AirlinesConsole.Commands;
public class ReserveCargoCommand : ICommand
{
    private readonly FlightsManager _flightsManager;
    private readonly AircraftsManager _aircraftManager;
    private readonly string _flightId;
    private readonly uint _cargoWeight;
    private readonly double _cargoVolume;

    public ReserveCargoCommand(FlightsManager flightsManager, AircraftsManager aircraftManager, string flightId, uint cargoWeight, double cargoVolume)
    {
        _flightsManager = flightsManager;
        _aircraftManager = aircraftManager;
        _flightId = flightId;
        _cargoWeight = cargoWeight;
        _cargoVolume = cargoVolume;
    }

    public void Execute()
    {
        Flight? flight = _flightsManager.FlightById(_flightId);
        if (flight == null)
        {
            throw new EntityNotFoundException($"No flight with id {_flightId} exists!");
        }
        string aircraftModel = flight.AircraftModel;
        CargoAircraft? aircraft = _aircraftManager.CargoAircraftByModel(aircraftModel);
        if (aircraft == null)
        {
            throw new InvalidInputException($"Couldn't reserve cargo aircraft for flight with id {_flightId}! The flight doesn't have an aircraft of type cargo!");
        }
        if (aircraft.CanBeReserved(_cargoWeight, _cargoVolume))
        {
            Console.WriteLine($"Model {aircraft.Model} of cargo aircraft with load: {aircraft.CargoLoad} and volume: {aircraft.CargoVolume} can be reserved!");
        }
        else
        {
            Console.WriteLine($"No cargo aircraft with {_cargoWeight} kg load and {_cargoVolume} m^3 volume is available!");
        }
    }
}
