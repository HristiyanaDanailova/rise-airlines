﻿using AirlinesBusiness.Models;

namespace AirlinesConsole.Commands;
public class RouteRemoveCommand : ICommand
{
    private readonly Route _route;
    public RouteRemoveCommand(Route route)
    {
        _route = route;
    }

    public void Execute()
    {
        _route.RemoveFlightAtEnd();
        Console.WriteLine("Last flight removed successfully!");
    }
}
