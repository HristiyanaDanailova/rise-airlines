﻿using AirlinesBusiness.CustomExceptions;
using AirlinesBusiness.Managers;
using AirlinesBusiness.Models;
using AirlinesConsole.Commands;
using AirlinesConsole.UtilsNS;
using AirlinesConsole.ValidationsNS;

namespace AirlinesConsole.CommandFactoryNS;
public static class CommandFactory
{
    public static ICommand? CreateCommand(string command, AirportsManager airportsManager, AirlinesManager airlinesManager, FlightsManager flightsManager, Route route, AircraftsManager aircraftManager, RouteFinder routeFinder, FlightsNetwork flightsNetwork)
    {
        if (!Validator.IsValidCommand(command))
        {
            throw new InvalidCommandException("Invalid command! Please, try again!");
        }
        if (Validator.IsExitCommand(command))
        {
            return new ExitCommand();
        }
        else if (Validator.IsHelpCommand(command))
        {
            return new HelpCommand();
        }
        else if (Validator.IsSearchCommand(command))
        {
            string[] searchTerms = StringUtils.SplitString(command);
            if (searchTerms.Length != 2)
            {
                throw new InvalidCommandException("Invalid \"Search\" command!  Please ensure your command follows this format : \"\"Search <search term>\"");
            }
            string searchTerm = searchTerms[1];
            return new SearchCommand(airportsManager, airlinesManager, flightsManager, searchTerm);
        }
        else if (Validator.IsSortCommand(command))
        {
            string[] sortngArguments = StringUtils.SplitString(command);
            if (sortngArguments.Length != 3)
            {

                throw new InvalidCommandException("Invalid \"Sort\" command!  Please ensure your command follows this format : \"Sort \"airports/airlines/flights\" \"ascending/descending\"\"");

            }
            string inputData = sortngArguments[1];
            if (!Validator.IsValidDataArray(inputData))
            {
                throw new InvalidInputException("Invalid data name! Data name can be one of the following: \"airports/airlines/flights\"");
            }
            string order = sortngArguments[2];
            if (!Validator.IsValidOrderSpecifier(order))
            {
                throw new InvalidInputException("Invalid sorting order specifier! Sorting order speifier can be one of the following: \"ascending/descending\"");

            }
            return new SortCommand(airportsManager, airlinesManager, flightsManager, inputData, order);
        }
        else if (Validator.IsExistCommand(command))
        {
            string[] splitted = StringUtils.SplitString(command);
            if (splitted.Length != 2)
            {
                throw new InvalidCommandException("Invalid \"Exist\" command!  Please ensure your command follows this format : \"Exist <airline name>\"");

            }
            string searchTerm = splitted[1];
            return new ExistCommand(airlinesManager, searchTerm);
        }
        else if (Validator.IsListCommand(command))
        {
            string[] splitted = StringUtils.SplitString(command);
            int lastIndex = splitted.Length - 1;
            // a City/Country name can have space delimiter (ex: New York)
            string inputData = string.Join(" ", splitted.Skip(1).Take(lastIndex - 1));
            string from = splitted[lastIndex];
            if (!Validator.IsCityOrCountry(from))
            {
                throw new InvalidInputException("Invalid from specifier! From specifier can be one of the following: \"City/Country\"");

            }
            return new ListCommand(airportsManager, inputData, from);
        }
        else if (Validator.IsRouteNewCommand(command))
        {
            return new RouteNewCommand(route);
        }
        else if (Validator.IsRouteAddCommand(command))
        {
            string[] splitted = StringUtils.SplitString(command);
            if (splitted.Length != 3)
            {
                throw new InvalidCommandException("Invalid \"Route add\" command!  Please ensure your command follows this format : \"Route add <flight id>\"");
            }
            string flightId = splitted[2];
            return new RouteAddCommand(flightsManager, route, flightId);
        }
        else if (Validator.IsRouteRemoveCommand(command))
        {
            return new RouteRemoveCommand(route);
        }
        else if (Validator.IsRoutePrintCommand(command))
        {
            return new RoutePrintCommand(route);
        }
        else if (Validator.IsRouteFindCommand(command))
        {
            string[] splitted = StringUtils.SplitString(command);
            if (splitted.Length != 3)
            {
                throw new InvalidCommandException("Invalid \"Route find\" command!  Please ensure your command follows this format : \"Route find <airport id>\"");
            }
            string airportId = splitted[2];
            return new RouteFindCommand(airportsManager, routeFinder, airportId);
        }
        else if (Validator.IsRouteCheckCommand(command))
        {
            string[] splitted = StringUtils.SplitString(command);
            if (splitted.Length != 4)
            {
                throw new InvalidCommandException("Invalid \"Route check\" command!  Please ensure your command follows this format : \"Route check <star airport id> <end airport id>\"");
            }
            string startAirportId = splitted[2];
            string endAirportId = splitted[3];
            Airport? startAirport = airportsManager.GetAirportById(startAirportId);
            Airport? endAirport = airportsManager.GetAirportById(endAirportId);
            if (startAirport == null)
            {
                throw new InvalidInputException($"No airport with id {startAirportId} found!");
            }
            if (endAirport == null)
            {
                throw new InvalidInputException($"No airport with id {endAirportId} found!");
            }
            return new RouteCheckCommand(flightsNetwork, startAirport, endAirport);
        }
        else if (Validator.IsRouteSearchCommand(command))
        {
            string[] splitted = StringUtils.SplitString(command);
            if (splitted.Length != 5)
            {
                throw new InvalidCommandException("Invalid \"Route search\" command!  Please ensure your command follows this format : \"Route search <star airport id> <end airport id> <\"cheap\"/\"short\"/\"stops>\"\" ");
            }
            string startAirportId = splitted[2];
            string endAirportId = splitted[3];
            string strategy = splitted[4];
            Airport? startAirport = airportsManager.GetAirportById(startAirportId);
            Airport? endAirport = airportsManager.GetAirportById(endAirportId);
            if (startAirport == null)
            {
                throw new InvalidInputException($"No airport with id {startAirportId} found!");
            }
            if (endAirport == null)
            {
                throw new InvalidInputException($"No airport with id {endAirportId} found!");
            }
            if (!string.Equals(strategy, "cheap") && !string.Equals(strategy, "short") && !string.Equals(strategy, "stops"))
            {
                throw new InvalidInputException("Invalid strategy specifier! Strategy specifier can be one of the following: <\"cheap\"/\"short\"/\"stops>\"");
            }
            return new RouteSearchCommand(flightsNetwork, startAirport, endAirport, strategy);
        }
        else if (Validator.IsReserveCargoCommand(command))
        {
            string[] splitted = StringUtils.SplitString(command);
            if (splitted.Length != 5)
            {
                throw new InvalidCommandException("Invalid \"Reserve cargo\" command!  Please ensure your command follows this format : \"Reserve cargo <flight id> <cargo weight> <cargo volume>\"");
            }
            string flightId = splitted[2];
            if (!Validator.IsValidNumberRepresentation(splitted[3]) || !Validator.IsValidNumberRepresentation(splitted[4]))
            {
                throw new InvalidInputException("Cargo weight and cargo volume must be non-negative numbers!");
            }
            uint cargoWeight = uint.Parse(splitted[3]);
            double cargoVolume = double.Parse(splitted[4]);
            return new ReserveCargoCommand(flightsManager, aircraftManager, flightId, cargoWeight, cargoVolume);
        }
        else if (Validator.IsReserveTicketCommand(command))
        {
            string[] splitted = StringUtils.SplitString(command);
            if (splitted.Length != 6)
            {
                throw new InvalidCommandException("Invalid \"Reserve ticket\" command!  Please ensure your command follows this format : \"Reserve ticket <flight id> <seats> <small baggage count> <large baggage count>\"");
            }
            string flightId = splitted[2];
            if (!Validator.IsValidNumberRepresentation(splitted[3]) || !Validator.IsValidNumberRepresentation(splitted[4]) || !Validator.IsValidNumberRepresentation(splitted[5]))
            {
                throw new InvalidInputException("Seats and baggage count numbers should be non-negative whole numbers!");
            }
            uint seats = uint.Parse(splitted[3]);
            uint smallBaggageCount = uint.Parse(splitted[4]);
            uint largeBaggageCount = uint.Parse(splitted[5]);
            return new ReserveTicketCommand(flightsManager, aircraftManager, flightId, seats, smallBaggageCount, largeBaggageCount);

        }
        return null;
    }
}
