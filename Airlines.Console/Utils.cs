﻿namespace AirlinesConsole.UtilsNS;
public class ConsoleUtils
{
    // assures that an user input won't be null
    public static string ReadLine()
    {
        string? input = Console.ReadLine();
        if (input == null)
        {
            return "";
        }
        return input.Trim();

    }

    public static void PrintHelp()
    {
        Console.WriteLine("-----------HELP-----------");
        Console.WriteLine("Commands:");
        Console.WriteLine("\"Exit\" : Ends the program execution.");
        Console.WriteLine("\"Help\" : Prints a reference to all available commands.");
        Console.WriteLine("\"Search <search term>\" : Searches for <search-term> in all arrays.");
        Console.WriteLine("\"Sort \"airports/airlines/flights\" \"ascending/descending\"\" : Sorts the given input data in the given order.");
        Console.WriteLine("\"Exist <airline name> : Checks wether an airline with <airline name> exists.\"");
        Console.WriteLine("\"List <country/city name> <country/city> : Lists all airports in <city/country name>.\"");
        Console.WriteLine("\"Route new\" : Creates a new flight route.");
        Console.WriteLine("\"Route add <flight id>\" : Adds a flight with <dlight id> to the existing route.");
        Console.WriteLine("\"Route remove\" : Removes the last flight form the route."); ;
        Console.WriteLine("\"Route print\" : Prints the current route.");
        Console.WriteLine("\"Route find <airport id>\" : Prints the flight route to the destination airport."); ;
        Console.WriteLine("\"Route check <start airport id> <end airport id>\" : Checks if there are flight connecting the start airpor to the end airport."); ;
        Console.WriteLine("\"Route search <start airport id> <end airport id> \"short/cheap/stops\"\" : Finds a path between the start and end airport following a given strategy."); ;
        Console.WriteLine("\"Reserve cargo <flight id> <cargo weight> <cargo volume>\" : Checks if a cargo aircraft can be reserved."); ;
        Console.WriteLine("\"Reserve ticket <flight id> <seats> <small baggage count> <large baggage count>\" : Checks if a ticket for a passenger aircraft can be reserved."); ;
        Console.WriteLine("\"Batch start\": Turns batch execution mode ON.");
        Console.WriteLine("\"Batch cancel\": Turns batch execution mode OFF.");
        Console.WriteLine("\"Batch run\": Executes all commands in the batch.");
        Console.WriteLine("-------------------------");

    }

}

public class StringUtils
{
    public static string[] SplitString(string input)
    {
        string[] parts = input.Split(" ", StringSplitOptions.RemoveEmptyEntries);
        return parts;
    }
}
