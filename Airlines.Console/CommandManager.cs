﻿
using AirlinesBusiness.Managers;
using AirlinesBusiness.Models;
using AirlinesConsole.CommandFactoryNS;
using AirlinesConsole.ValidationsNS;
using AirlinesConsole.Commands;

namespace AirlinesConsole.CommandManagerNS;
public class CommandManager
{
    private readonly AirportsManager _airportsManager;
    private readonly AirlinesManager _airlinesManager;
    private readonly FlightsManager _flightsManager;
    private readonly Route _route;
    private readonly AircraftsManager _aircraftManager;
    private readonly RouteFinder _routeFinder;
    private readonly FlightsNetwork _flightsNetwork;
    private readonly Queue<ICommand> _batch;
    private bool _batchMode;

    public CommandManager(
        AirportsManager airportsManager,
        AirlinesManager airlinesManager,
        FlightsManager flightsManager,
        Route route,
        AircraftsManager aircraftManager,
        RouteFinder routeFinder
        , FlightsNetwork flightsNetwork)
    {
        _airportsManager = airportsManager;
        _airlinesManager = airlinesManager;
        _flightsManager = flightsManager;
        _route = route;
        _aircraftManager = aircraftManager;
        _routeFinder = routeFinder;
        _flightsNetwork = flightsNetwork;
        _batch = new Queue<ICommand>();
        _batchMode = false;

    }

    public void ProcessCommand(string command)
    {
        if (Validator.IsBatchStartCommand(command))
        {
            _batchMode = true;
            Console.WriteLine("Batch mode is ON.");
        }
        else if (Validator.IsBatchRunCommand(command))
        {
            ICommand? commandObject;
            while (_batch.TryDequeue(out commandObject))
            {
                try
                {

                    commandObject.Execute();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"En error ocurred: {ex.Message}");
                }
            }
        }
        else if (Validator.IsBatchCancelCommand(command))
        {
            ICommand? commandObject;
            while (_batch.TryDequeue(out commandObject))
            {
            }
            _batchMode = false;
            Console.WriteLine("Batch mode is OFF.");
        }
        else
        {
            try
            {
                ICommand? commandObject = CommandFactory.CreateCommand(command, _airportsManager, _airlinesManager, _flightsManager, _route, _aircraftManager, _routeFinder, _flightsNetwork);

                if (commandObject != null)
                {
                    if (_batchMode)
                    {
                        _batch.Enqueue(commandObject);
                    }
                    else
                    {
                        try
                        {
                            commandObject.Execute();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"En error ocurred: {ex.Message}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"En error ocurred: {ex.Message}");
            }
        }
    }

}
