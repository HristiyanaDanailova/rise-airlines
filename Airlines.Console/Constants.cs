﻿namespace AirlinesConsole.Constants;
public static class Constants
{
    public readonly static string AIRPORTS_FILENAME = "airports.csv";
    public readonly static string AIRLINES_FILENAME = "airlines.csv";
    public readonly static string AIRCRAFTS_FILENAME = "aircrafts.csv";
    public readonly static string FLIGHTS_FILENAME = "flights.csv";
    public readonly static string ROUTES_FILENAME = "routeFinder.txt";

}
